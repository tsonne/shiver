#! /usr/bin/env python
# -*- coding: utf-8 -*-
"""
Shiver - Collection of seismic analysis, plotting, mapping code

Purpose:
    get response-corrected SIL waveform data
    station-event map/depth plots
    seismic array data processing
    SIL waveform data overviews per station
    utilities for seismic analysis

:copyright:
    Tim Sonnemann
:license:
    GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html)
"""
# C extension lib not moved correctly if only using numpy.distutils,
# importing setuptools fixes it.
try:
    import setuptools  # @UnusedImport # NOQA
except ImportError:
    pass

import glob
import inspect
import os
import sys

from numpy.distutils.core import setup
from numpy.distutils.misc_util import Configuration

DOCSTRING = __doc__.split("\n")
# Directory of the current file in the (hopefully) most reliable way
# possible, according to krischer (from Obspy)
SETUP_DIRECTORY = os.path.dirname(os.path.abspath(inspect.getfile(
    inspect.currentframe())))
MAIN_NAME = 'shiver'

# Any .py files that are used at install time must be registered now
UTIL_PATH = os.path.join(SETUP_DIRECTORY, MAIN_NAME, "util")
sys.path.insert(0, UTIL_PATH)
from libnames import _get_lib_name  # @UnresolvedImport
sys.path.pop(0)

INSTALL_REQUIRES = [
    'future>=0.12.4',
    'numpy>=1.6.1',
    'obspy>=1.2.0',
    'fastparquet',
    'zstandard']

def find_packages():
    """
    Simple function to find all modules under the current folder.
    """
    modules = []
    for dirpath, _, filenames in os.walk(SETUP_DIRECTORY):
        if "__init__.py" in filenames:
            modules.append(os.path.relpath(dirpath, SETUP_DIRECTORY))
    return [_i.replace(os.sep, ".") for _i in modules]


def configuration(parent_package="", top_path=None):
    """
    Config function used to compile C code.
    """
    config = Configuration("", parent_package, top_path)
    # SIGNAL
    path = os.path.join(MAIN_NAME, "c_fct", "src")
    files = glob.glob(os.path.join(path, "*.c"))
    # compiler specific options
    kwargs = {}
    config.add_extension(_get_lib_name("c_fct", add_extension_suffix=False),
                         files, **kwargs)
    return config


def setupPackage():
    # setup package
    setup(
        name=MAIN_NAME,
        version='0.1.0',
        description=DOCSTRING[1],
        long_description="\n".join(DOCSTRING[3:]),
        author='Tim Sonnemann',
        license='Icelandic Met Office',
        classifiers=[
            'Development Status :: 4 - Beta',
            'Environment :: Console',
            'Intended Audience :: Science/Research',
            'Intended Audience :: Developers',
            'License :: Other/Proprietary License :: Icelandic Met Office',
            'Operating System :: POSIX',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.5',
            'Programming Language :: Python :: 3.6',
            'Programming Language :: Python :: 3.7',
            'Programming Language :: Python :: 3.8',
            'Topic :: Scientific/Engineering',
            'Topic :: Scientific/Engineering :: Physics'],
        keywords=['earthquakes', 'signal', 'trigger'],
        packages=find_packages(),
        namespace_packages=[],
        zip_safe=False,
        install_requires=INSTALL_REQUIRES,
        ext_package=(MAIN_NAME + '.lib'),
        configuration=configuration)


if __name__ == '__main__':
    # clean --all does not remove extensions automatically
    if 'clean' in sys.argv and '--all' in sys.argv:
        import shutil
        # delete complete build directory
        path = os.path.join(SETUP_DIRECTORY, 'build')
        try:
            shutil.rmtree(path)
        except Exception:
            pass
        # delete all shared libs from lib directory
        path = os.path.join(SETUP_DIRECTORY, MAIN_NAME, 'lib')
        for filename in glob.glob(path + os.sep + '*.pyd'):
            try:
                os.remove(filename)
            except Exception:
                pass
        for filename in glob.glob(path + os.sep + '*.so'):
            try:
                os.remove(filename)
            except Exception:
                pass
    else:
        setupPackage()
