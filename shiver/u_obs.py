#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 18:31:46 2020

@author: ts
"""


def draw_line_vrt(ax, x, dct={}):
    """
    Draw a vertical line from top to bottom at position x.

    Parameters
    ----------
    ax : axes._axes.Axes
        Plot axis object to draw line in.
    x : float, int
        Scalar line position x-value, its y-axis extent will be Ylim.
    dct : dict, optional
        Dict of plot method options to use. The default is {}.

    Returns
    -------
    None.
    """
    ylim = ax.get_ylim()
    attr = {'linewidth': 1.,
            'linestyle': '-',
            'color': 'k'}
    attr = {**attr, **dct}  # overwrite defaults if input given
    ax.plot((x, x), ylim, **attr)
    return None


def get_rect_1(hmrg, vmrg, gap, rsz):
    pmargl, pmargr = hmrg  # margin left, right
    pmargb, pmargt = vmrg  # margin bottom, top
    # gap = gap size horizontal, vertical
    # rsz = relative size main to secondary plots (X times)
    phrz = 1. - pmargl - pmargr
    pvrt = 1. - pmargb - pmargt - gap
    pw1 = phrz  # width main plot and bottom plot
    ph1 = pvrt * rsz/(rsz+1.)  # height main plot
    ph2 = pvrt * 1./(rsz+1.)  # height bottom secondary plot
    pbox_Main = [pmargl, pmargb+2.*gap+ph2, pw1, ph1]
    pbox_Bttm = [pmargl, pmargb, pw1, ph2]
    return pbox_Main, pbox_Bttm


def response_plot(paz, t_samp=0.01, nfft=32768, title=None,
                  outfile=None):
    """
    Plot amplitude and phase of instrument response based on poles and zeros.

    Parameters
    ----------
    paz : dict
        Dict with gain, poles and zeros.
    t_samp : float, optional
        Sampling interval [s]. The default is 0.01.
    nfft : int, optional
        Number of FFT points used. The default is 32768.
    title : str, optional
        Plot title string. The default is None.
    outfile : str, optional
        Create image at this filepath if given. The default is None.

    Returns
    -------
    fig : TYPE
        DESCRIPTION.
    """
    import numpy as np
    from obspy.signal.invsim import paz_to_freq_resp
    import matplotlib.pyplot as plt

    # get frequency transform, remove f=0 value (cannot be show in log plots)
    h, f = paz_to_freq_resp(
        paz['poles'], paz['zeros'], paz['gain'],
        t_samp, nfft, freq=True)
    h = h[1:]
    f = f[1:]
    # create plots
    fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex=True,
                                   gridspec_kw={'hspace': 0.01},
                                   figsize=(8, 6), dpi=200)
    # Set plot positions
    hmrg = [0.1, 0.01]  # margin left, right
    if not title:
        vmrg = [0.1, 0.01]  # margin bottom, top
    else:
        vmrg = [0.1, 0.05]  # margin bottom, top
    gap = 0.005  # gap size horizontal, vertical
    rsz = 2.  # relative size main to secondary plots (X times)
    # derive other position parameters
    pbox_Main, pbox_Bttm = get_rect_1(hmrg, vmrg, gap, rsz)
    # apply new positions
    ax1.set_position(pbox_Main)
    ax2.set_position(pbox_Bttm)

    lines = ax1.loglog(f, abs(h))
    ax1.set_ylabel('Amplitude')
    if title:
        ax1.set_title(title)

    phase = np.angle(h, deg=True)
    ax2.semilogx(f, phase)
    ax2.set_xlabel('Frequency [Hz]')
    ax2.set_ylabel('Phase [deg]')

    plt.setp(ax1.get_xticklabels(), visible=False)
    YLim = ax1.get_ylim()
    YR = np.log10(YLim[1]/YLim[0])
    if YR < 2.1:
        fct = 10**((2.1-YR)/2)
        ax1.set_ylim([YLim[0]/fct, YLim[1]*fct])
    else:
        ax1.set_ylim(YLim)
    YLim = ax2.get_ylim()
    ax2.set_ylim(YLim)
    ax1.grid(b=True, which='major')
    ax1.grid(b=True, which='minor', linestyle='--')
    ax2.grid(b=True)

    fny = 0.5/t_samp
    col = lines[0].get_color()
    attr = {'color': col, 'linestyle': '--'}
    draw_line_vrt(ax1, fny, attr)
    draw_line_vrt(ax2, fny, attr)

    if outfile:
        fig.savefig(outfile, bbox_inches="tight")
        plt.close()
        return None
    else:
        plt.show()
    return fig


def remove_resp_xml(st, pre_filt, finv, format='SC3ML'):
    """
    Remove instrument response from stream using inventory XML.

    Output will be VEL in m/s.

    Parameters
    ----------
    st : obspy.stream
        Input obspy stream of traces.
    pre_filt : list
        List of 4 corner frequencies of spectral cosine filter.
    finv : str
        Path to station inventory XML file.
    format : str, optional
        Inventory XML format. The default is 'SC3ML'.

    Returns
    -------
    stcor : obspy.stream
        Output stream with instrument-corrected traces.

    """
    from obspy import read_inventory
    inv = read_inventory(finv, format=format)
    stcor = st.copy()
    stcor.detrend()
    for tr in stcor:
        try:
            tr.remove_response(inventory=inv, pre_filt=pre_filt)
        except ValueError as e:
            print('ValueError: {} {}'.format(e, tr.id))
            stcor.remove(tr)
            print('# Removed trace from further processing.')
    return stcor, inv


def station_stream_list(st):
    """
    Get list of stations from obspy.stream.

    Parameters
    ----------
    st : obspy.stream
        Stream to get station names from.

    Returns
    -------
    list
        list of station code strings.
    """
    cod = [t.stats.station for t in st]
    return sorted_unique(cod)


def check_clipped_old(st, silent=False):
    """
    Check if traces are clipped.

    This is a very simple calculation. It assumes an earthquake signal and not
    too much noise before or after (at least 10% strong signal).
    If more than 0.25 percent of the data are closer than (max-min)/100 to
    the maximum or minimum, there's a high chance the trace is clipped.
    Has not been tested extensively and could probably be improved,
    but good enough for simple cases.

    Parameters
    ----------
    st : obspy.Stream
        All traces of stream will be checked individually.
    silent : bool
        If True, write message to terminal if clipped trace found.

    Returns
    -------
    clip_list : list
        List with trace IDs of clipped traces.
    """
    import numpy as np
    clip_list = []
    for tr in st:
        n = tr.stats.npts
        xmin = tr.data.min()
        xmax = tr.data.max()
        dx = xmax - xmin
        xlim_up = xmax - dx/100.
        xlim_lo = xmin + dx/100.
        r_up = np.count_nonzero(tr.data > xlim_up) / n * 100.
        r_lo = np.count_nonzero(tr.data < xlim_lo) / n * 100.
        if r_up > 0.25 or r_lo > 0.25:
            clip_list.append(tr.id)
            if not silent:
                print('Warning: Potential clipping on {}'.format(tr.id))
    return clip_list


def check_clipped(st, THR_ABS=3e6, THR_MED=2e3, MED_LEN=8, silent=False):
    """
    Get list of clipped traces in stream.

    Parameters
    ----------
    st : obspy.Stream
        All traces of stream will be checked individually.
    THR_ABS : float, optional
        Threshold for absolute digitizer count values above which clipping
        considered likely. The default is 3e6.
    THR_MED : float, optional
        Threshold for median filter values (width = MED_LEN samples) below
        which clipping considered likely if absolute amplitude above THR_ABS.
        The default is 2e3.
    MED_LEN : int
        Median filter kernel length. Default is 8.
    silent : bool
        If True, write message to terminal if clipped trace found.

    Returns
    -------
    clip_list : list
        List with trace IDs of clipped traces.
    """
    clip_list = []
    for tr in st:
        if is_clipped_trace(
                tr, THR_ABS=THR_ABS, THR_MED=THR_MED, MED_LEN=MED_LEN):
            clip_list.append(tr.id)
            if not silent:
                print('Warning: Potential clipping on {}'.format(tr.id))
    return clip_list


def is_clipped_trace(tr, THR_ABS=3e6, THR_MED=2e3, MED_LEN=8):
    """
    Test whether trace is clipped.

    Method: Assume input is raw counts of 24 bit digitizer.
    If absolute amplitude is above THR_ABS, and if running
    median of MED_LEN samples on differentiated trace is below THR_MED,
    mark it as clipped.
    Not optimized, could take short windows instead of whole trace...

    Parameters
    ----------
    tr : obspy.Trace
        Single trace to check.
    THR_ABS : float, optional
        Threshold for absolute digitizer count values above which clipping
        considered likely. The default is 3e6.
    THR_MED : float, optional
        Threshold for median filter values (width = MED_LEN samples) below
        which clipping considered likely if absolute amplitude above THR_ABS.
        The default is 2e3.
    MED_LEN : int
        Median filter kernel length. Default is 8.

    Returns
    -------
    bool
        True if trace is  likely clipped, else False.
    """
    import numpy as np
    import scipy as sp
    # very quick check for amplitude threshold:
    if tr.data.max() < THR_ABS and tr.data.min() > -THR_ABS:
        return False
    # otherwise do full check:
    dabs = np.abs(tr.data)
    ddif = np.abs(np.gradient(tr.data))
    medf = sp.ndimage.median_filter(ddif, MED_LEN)
    return any((dabs > THR_ABS) & (medf < THR_MED))


def station_from_id(id_list):
    """Return list of stations from list of full trace IDs."""
    if id_list:
        return [s.split('.')[1] for s in id_list]
    else:
        return []


def sorted_unique(in_list):
    """Return unique sorted list of given list."""
    if in_list:
        return sorted(list(set(in_list)))
    else:
        return []


def quick_plot_xy(figpath, x, y, x2=None, y2=None, marker='o',
                  xlabel=None, ylabel=None, title=None, labels=None):
    """
    Create and save a simple x,y plot figure.

    Saves the trouble to call matplotlib, figure and axis settings,
    saving and closing. Just create the image file for an easy plot.

    Parameters
    ----------
    figpath : str
        DESCRIPTION.
    x : array
        x-axis values.
    y : array
        y-axis values.
    x2 : array
        (optional) second dataset x values.
    y2 : array
        (optional) second dataset y values.

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    dpi = 200
    fig, ax = plt.subplots(figsize=(8, 6), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    # check if labels given:
    if labels is not None:
        if isinstance(labels, str):
            L1 = labels
            L2 = None
        else:
            L1 = labels[0]
            if len(labels) > 1:
                L2 = labels[1]
            else:
                L2 =None
    else:
        L1 = None
        L2 = None
    # plots
    ax.plot(x, y, marker, label=L1)
    if y2 is not None:
        ax.plot(x2, y2, marker, color='r', label=L2)
    ax.grid(b=True, which='major')
    if labels is not None:
        ax.legend()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    fig.savefig(figpath, bbox_inches='tight')
    plt.close()
    return None


def quick_plot_dict(figpath, dct, marker='o',
                    xlabel=None, ylabel=None, title=None,
                    legend_out=True):
    """
    Create and save x,y plot figure from dict.

    Saves the trouble to call matplotlib, figure and axis settings,
    saving and closing. Just create the image file for an easy plot.

    Parameters
    ----------
    figpath : str
        DESCRIPTION.
    dct : dict
        A dict, whose keys will be plot object labels and the values are
        lists with two elements: x-axis values and y-axis values.
        Example: dct['foo'] = [[4,3,8,1], [5,2,7,9]]

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    dpi = 200
    fig, ax = plt.subplots(figsize=(8, 6), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    # plots
    for lbl, v in dct.items(): 
        ax.plot(v[0], v[1], marker, label=lbl)
    ax.grid(b=True, which='major')
    if legend_out:
        ax.legend(loc='upper left', bbox_to_anchor=(1.01, 1.0))
    else:
        ax.legend()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    fig.savefig(figpath, bbox_inches='tight')
    plt.close()
    return None


def quick_scatter_dict(figpath, ldct,
                       xlabel=None, ylabel=None, title=None,
                       legend=True, legend_out=True,
                       fancy_legend=False, legend_kw=None, legend_title=None,
                       colorbar=False, cbarlabel=None):
    """
    Create and save x,y scatterplot figure from dict or list of dicts.

    Saves the trouble to call matplotlib, figure and axis settings,
    saving and closing. Just create the image file for an easy plot.

    This method is probably both too simple and too complex now, and should
    not be relied on for fancy graphs, but the simple functionality is OK.

    Parameters
    ----------
    figpath : str
        DESCRIPTION.
    ldct : dict or list of dicts
        A dict with plot keys and corresponding values.
        Can be a list with multiple such dicts, all to be plotted.
        Example:
            ldct = [
                {'x':[2,3,5], 'y':[7,3,8], 's':[1,2,1], 'c':[1,2,3],
                 'alpha': 0.5, 'marker': 'D', 'label': 'dataset 1'},
                {'x':[4,7,9], 'y':[2,3,1], 's':[1,2,2], 'c':[1,2,3],
                 'alpha': 0.5, 'marker': 'o', 'label': 'dataset 2'}
                ]

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    dpi = 200
    fig, ax = plt.subplots(figsize=(8, 6), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    # plots
    if isinstance(ldct, dict):
        ldict = [ldct]
    h = [ax.scatter(**d) for d in ldct]
    ax.grid(b=True, which='major')
    if legend:
        if fancy_legend:
            if legend_kw is None:
                legend_kw = {'prop': 'sizes'}
            if legend_out:
                legend1 = ax.legend(
                    loc='upper left', bbox_to_anchor=(1.01, 1.0))
                ax.add_artist(legend1)
                legend2 = ax.legend(
                    *h[0].legend_elements(**legend_kw),
                    title=legend_title,
                    loc='lower left', bbox_to_anchor=(1.01, 0.01))
            else:
                legend1 = ax.legend(loc='upper right')
                ax.add_artist(legend1)
                legend2 = ax.legend(
                    *h[0].legend_elements(**legend_kw),
                    title=legend_title, loc='lower left')
        else:
            if legend_out:
                ax.legend(loc='upper left', bbox_to_anchor=(1.01, 1.0))
            else:
                ax.legend()
    if colorbar:
        hc = fig.colorbar(h[0])
        if cbarlabel:
            hc.set_label(cbarlabel)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    fig.savefig(figpath, bbox_inches='tight')
    plt.close()
    return None


def fancy_scatter_plot(figpath, scdict,
                       xlabel=None, ylabel=None, title=None,
                       legend=True, legend_out=True,
                       fancy_legend=False, legend_kw1=None, legend_kw2=None,
                       colorbar=False, cbarlabel=None):
    """
    Create and save x,y scatterplot figure from dict and legend option dicts.

    Saves the trouble to call matplotlib, figure and axis settings,
    saving and closing. Just create the image file for an easy plot.

    Parameters
    ----------
    figpath : str
        DESCRIPTION.
    scdict : dict
        A dict with plot keys and corresponding values.
        Example:
            scdct = {'x':[2,3,5], 'y':[7,3,8], 's':[1,2,1], 'c':[1,2,3],
                     'alpha': 0.5, 'marker': 'D', 'label': 'dataset 1'}

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    dpi = 200
    fig, ax = plt.subplots(figsize=(8, 6), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    # plot
    h = ax.scatter(**scdict)
    ax.grid(b=True, which='major')
    if legend:
        if fancy_legend:
            if legend_kw1 is None:
                legend_kw1 = {'prop': 'colors'}
            if legend_kw2 is None:
                legend_kw2 = {'prop': 'sizes'}
            ltitle1 = legend_kw1.pop('title', None)
            ltitle2 = legend_kw2.pop('title', None)
            lbl1 = legend_kw1.pop('labels', None)
            lbl2 = legend_kw2.pop('labels', None)
            handles1, labels1 = h.legend_elements(**legend_kw1)
            if lbl1: labels1 = lbl1
            handles2, labels2 = h.legend_elements(**legend_kw2)
            if lbl2: labels2 = lbl2
            if legend_out:
                legend1 = ax.legend(
                    handles1, labels1,
                    title=ltitle1, loc='upper left',
                    bbox_to_anchor=(1.01, 1.0))
                ax.add_artist(legend1)
                legend2 = ax.legend(
                    handles2, labels2,
                    title=ltitle2, loc='lower left',
                    bbox_to_anchor=(1.01, 0.))
            else:
                legend1 = ax.legend(
                    handles1, labels1,
                    title=ltitle1, loc='upper right')
                ax.add_artist(legend1)
                legend2 = ax.legend(
                    handles2, labels2,
                    title=ltitle2, loc='lower left')
        else:
            if legend_out:
                ax.legend(loc='upper left', bbox_to_anchor=(1.01, 1.0))
            else:
                ax.legend()
    if colorbar:
        hc = fig.colorbar(h)
        if cbarlabel:
            hc.set_label(cbarlabel)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    fig.savefig(figpath, bbox_inches='tight')
    plt.close()
    return None


def quick_scatter(figpath, x, y, s=None, c=None,
                  cmap=None, norm=None, marker='o',
                  alpha=None, vmin=None, vmax=None,
                  xlabel=None, ylabel=None, title=None,
                  colorbar=False, cbarlabel=None):
    """
    Create and save a simple x,y scatter plot figure.

    Saves the trouble to call matplotlib, figure and axis settings,
    saving and closing. Just create the image file for an easy plot.

    Parameters
    ----------
    figpath : str
        DESCRIPTION.
    x : array
        x-axis values.
    y : array
        y-axis values.
    s : scalar or array
        (optional) marker size values.
    c : color, sequence, or sequence of color
        (optional) marker color values.

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    dpi = 200
    fig, ax = plt.subplots(figsize=(8, 6), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    # plot
    h = ax.scatter(x, y, s=s, c=c,
                   cmap=cmap, norm=norm, marker=marker,
                   alpha=alpha, vmin=vmin, vmax=vmax)
    ax.grid(b=True, which='major')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    if colorbar:
        hc = fig.colorbar(h)
        if cbarlabel:
            hc.set_label(cbarlabel)
    fig.savefig(figpath, bbox_inches='tight')
    plt.close()
    return None


def quick_hist1(figpath, x, x2=None, 
                xlabel=None, ylabel=None, title=None, labels=None):
    """
    Create and save a simple 1D histogram figure.

    Saves the trouble to call matplotlib, figure and axis settings,
    saving and closing. Just create the image file for an easy plot.

    Parameters
    ----------
    figpath : str
        DESCRIPTION.
    x : array
        DESCRIPTION.

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    import numpy as np
    dpi = 200
    fig, ax = plt.subplots(figsize=(6, 5), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    # check if labels given:
    if labels is not None:
        if isinstance(labels, str):
            L1 = labels
            L2 = None
        else:
            L1 = labels[0]
            if len(labels) > 1:
                L2 = labels[1]
            else:
                L2 =None
    else:
        L1 = None
        L2 = None
    # plot histograms
    n_bins = int(np.sqrt(2*len(x)))
    ax.hist(x, bins=n_bins, color='b', alpha=0.7, label=L1)
    if x2 is not None:
        n_bins = int(np.sqrt(2*len(x2)))
        ax.hist(x2, bins=n_bins, color='r', alpha=0.7, label=L2)
    ax.grid(b=True, which='major')
    if labels is not None:
        ax.legend()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    fig.savefig(figpath, bbox_inches='tight')
    plt.close()
    return None


def quick_plot_scatterhist(st, figdirpath):
    """Create scatter and hist figure of first 2 components, save it."""
    import os
    fn = os.path.join(figdirpath, st[0].stats.station+'_EN_hist.png')
    set_scatter_hist(st[0].data, st[1].data, fn)
    return None


def scatter_hist(x, y, ax, ax_histx, ax_histy):
    import numpy as np
    # no labels
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)

    # the scatter plot:
    ax.scatter(x, y)

    # now determine nice limits by hand:
    binwidth = 5000
    xymax = max(np.max(np.abs(x)), np.max(np.abs(y)))
    lim = (int(xymax/binwidth) + 1) * binwidth

    bins = np.arange(-lim, lim + binwidth, binwidth)
    ax_histx.hist(x, bins=bins, density=True)
    ax_histy.hist(y, bins=bins,
                  orientation='horizontal', density=True)
    return None


def set_scatter_hist(x, y, outfile):
    import matplotlib.pyplot as plt
    # definitions for the axes
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    spacing = 0.005

    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom + height + spacing, width, 0.2]
    rect_histy = [left + width + spacing, bottom, 0.2, height]

    # start with a square Figure
    dpi = 200
    fig = plt.figure(figsize=(8, 8), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend

    ax = fig.add_axes(rect_scatter)
    ax_histx = fig.add_axes(rect_histx, sharex=ax)
    ax_histy = fig.add_axes(rect_histy, sharey=ax)

    # use the previously defined function
    scatter_hist(x, y, ax, ax_histx, ax_histy)

    # save, close, out
    fig.savefig(outfile, bbox_inches='tight')
    plt.close('all')
    return None


def save_mseed(path, st, lbl="", force=False, **kwargs):
    """
    Save all traces in stream as mseed files.

    One mseed file per trace (and segment if gaps).
    Can force data type to be int32 for STEIM2 if desired.
    Merged traces with gaps will be saved as pieces (no fill).

    Parameters
    ----------
    path : str
        Path to directory.
    st : obspy.stream
        All traces of this stream will get saved as separate mseed files.
    lbl: str
        Label to add on mseed filename before .msd extension.
        Could use, e.g. '_raw'
    force: bool
        If true, force trace data to match encoding if given, such that
        if encoding='STEIM2' and tr.data not int32, forcibly convert
        a copy of the trace and write it as int32 STEIM2. Default False.
    kwargs: arguments passed to obspy.io.mseed.core._write_mseed()
        encoding: (int or str) ASCII (0), INT16 (1), INT32 (3), FLOAT32 (4),
            FLOAT64 (5), STEIM1 (10) and STEIM2 (11).
        reclen: (int) from 2**8 to 2**20, default is 4096.
        byteorder: (int or str) 0 little 1 big endian, default 1.
        sequence_number: (int) first record sequence count, def 1.
        flush: (bool) write only full records if False, def True.

    Returns
    -------
    None.

    """
    import os
    import numpy as np

    os.makedirs(path, exist_ok=True)
    for tr in st:
        s = tr.stats.starttime.strftime('%Y%m%d%H%M%S')
        f = os.path.join(path, '{}_{}{}.msd'.format(tr.id, s, lbl))
        # split gap masked arrays, save all pieces (recursive):
        if isinstance(tr.data, np.ma.masked_array):
            sx = tr.split()
            save_mseed(path, sx, lbl=lbl, force=force, **kwargs)
            continue
        # force int32 STEIM2 if wanted and data not int32:
        if (force and
            'encoding' in kwargs and
            (kwargs['encoding'] == 'STEIM2' or
            kwargs['encoding'] == 11) and
            tr.data.dtype.type != np.int32):
                xx = tr.copy()
                xx.data = tr.data.astype(np.int32)
                xx.write(f, format="MSEED", **kwargs)
        else:
            tr.write(f, format="MSEED", **kwargs)
    return None


def get_stream_info(st):
    """
    Get total start, end, number of samples and traces of obspy.stream.

    Parameters
    ----------
    st : obspy.stream
        Input stream to investigate.

    Returns
    -------
    D : dict
        Dict with keys ['start', 'end', 'nsamp', 'ntrac'].
    """
    D = {'start': None, 'end': None, 'nsamp': 0, 'ntrac': 0}
    D['ntrac'] = len(st)
    if D['ntrac'] < 1:
        return D
    D['start'] = min([trace.stats.starttime for trace in st])
    D['end'] = max([trace.stats.endtime for trace in st])
    for tr in st:
        D['nsamp'] += len(tr)
    return D


def get_gappy_stations(st, start, end):
    """
    Get list of stations which do not completely fill chosen time window.

    Any gaps in entire trace or Not filling time from start to end
    will result in station listing.

    Parameters
    ----------
    st : obspy.stream
        Input stream to investigate.
    start : obspy.UTCDateTime
        Start of time window to check.
    end : obspy.UTCDateTime
        Start of time window to check.

    Returns
    -------
    gap_sta : list
        List of stations with gaps or not filling start to end.
    """
    from obspy import UTCDateTime
    Ustart = UTCDateTime(start)
    Uend = UTCDateTime(end)
    # get list of gappy traces
    gap_list = st.get_gaps()
    # get station codes of gappy traces
    gap_sta = sorted_unique([i[1] for i in gap_list])
    # get stations which don't fill entire time from 'start' to 'end'
    for tr in st:
        if tr.stats.station in gap_sta:
            continue
        if ((tr.stats.starttime - Ustart > tr.stats.delta) or
            (Uend - tr.stats.endtime > tr.stats.delta)):
            gap_sta.append(tr.stats.station)
    return sorted(gap_sta)


def get_windowed_maxabs(st, window_length, step, offset=0):
    """
    Get the lowest max amplitude of a series of time windows for each trace.
    
    Return DataFrame, one row per station, four columns E, N, Z, V3.
    Go over each trace in sliding time windows, keep the max abs amplitude,
    return the lowest per trace, i.e. possible noise floor.
    V3 is the vector length of all 3 components.

    Parameters
    ----------
    st : obspy.stream
        Stream of traces to analyze.
    window_length : float
        Window length in seconds.
    step : float
        Stride or step length in seconds.
    offset : float, optional
        Offset from start of trace (s). The default is 0.

    Returns
    -------
    df : pandas.DataFrame
        DataFrame, one row per station, four columns E, N, Z, V3.
    """
    import pandas as pd
    import numpy as np
    df = pd.DataFrame(index=station_stream_list(st), columns=['E','N','Z'],
                      dtype=np.float)
    for tr in st:
        npts = tr.stats.npts
        noff = int(offset*tr.stats.sampling_rate)
        nstep = int(step*tr.stats.sampling_rate)
        nwin = int(window_length*tr.stats.sampling_rate)
        nw = int((npts-noff-nwin)/nstep) + 1
        i1 = noff - 1
        i2 = noff - 1 + nwin
        x = [np.max(np.abs(
                tr.data[i1+(i*nstep):i2+(i*nstep)])) for i in range(nw)]
        df.at[tr.stats.station, tr.id[-1]] = np.min(x)
    df = df.assign(V3=np.sqrt(df.E**2. + df.N**2. + df.Z**2.))
    return df


def get_azim(coords_1, coords_2):
    """
    Get azimuth between given coordinates.

    Parameters
    ----------
    coords_1 : list or tuple
        Reference (latitude, longitude).
    coords_2 : list of lists or tuple
        Either simple (lat, lon) or list of multiple (lat,lon).

    Returns
    -------
    float or list of float
        Azimuth in deg for each pair or coordinates.
    """
    from geographiclib.geodesic import Geodesic
    if hasattr(coords_2[0], '__iter__'):
        # element iterable, have [(lat1,lon1), (lat2,lon2), ...]
        return [Geodesic.WGS84.Inverse(*coords_1, *x)['azi1']
                for x in coords_2]
    else:
        # element not iterable, only simple (lat,lon)
        return Geodesic.WGS84.Inverse(*coords_1, *coords_2)['azi1']


def get_distances(coords_1, coords_2):
    """
    Get geodesic distance in km between given coordinates.

    Parameters
    ----------
    coords_1 : list or tuple
        Reference (latitude, longitude).
    coords_2 : list of lists or tuple
        Either simple (lat, lon) or list of multiple (lat,lon).

    Returns
    -------
    float or list of float
        Distance in km for each pair or coordinates.
    """
    from geopy import distance
    if hasattr(coords_2[0], '__iter__'):
        # element iterable, have [(lat1,lon1), (lat2,lon2), ...]
        return [distance.distance(coords_1, x).km for x in coords_2]
    else:
        # element not iterable, only simple (lat,lon)
        return distance.distance(coords_1, coords_2).km


def pgv_gmm_C(M, r):
    """
    Get PGV (m/s) by model C of IMO report 2009-012.

    Parameters
    ----------
    M : float
        Magnitude.
    r : float
        Epicentral distance (km).

    Returns
    -------
    float
        peak ground velocity (m/s) estimate.
    """
    import numpy as np
    y = -1.69*np.log10(r + 0.00299*10**(0.621*M)) + 1.05*M - 4.96
    return 10**y


def ddm_gmm_C(M, r):
    """Calculate M-derivative of model C of IMO report 2019-012."""
    # y = log10(PGV) model C of IMO report 2019-012
    # dy/dm = (abc*10**(cM))/(b*10**(cM)+r)+d
    # -1.69*0.00299*0.621 = -0.0031379751
    return (-0.0031379751*10**(0.621*M))/(0.00299*10**(0.621*M)+r) + 1.05


def pgv_gmm_A(M, r):
    """Get PGV [m/s] by linear model A of IMO report 2009-012."""
    import numpy as np
    return 10**(-1.63*np.log10(r) + M - 4.88)


def m_by_pgv_r_A(pgv, r):
    """Get M from linear model A of IMO report 2009-012."""
    import numpy as np
    return np.log10(pgv) + 1.63*np.log10(r) + 4.88


def logpgv_gmm_C(M, r):
    """Get log10(PGV [m/s]) by model C of IMO report 2009-012."""
    import numpy as np
    return -1.69*np.log10(r + 0.00299*10**(0.621*M)) + 1.05*M - 4.96


def m_by_pgv_r_C(pgv, r):
    """Get M from nonlinear model C of IMO report 2009-012."""
    import scipy.optimize as so
    import numpy as np
    # handle nan:
    if np.isnan(pgv) or np.isnan(r):
        return pgv
    # initial guess from linear function:
    x0 = m_by_pgv_r_A(pgv, r)
    # function to be zero: residual (logPGV-y(M,r))
    f = lambda x, y, z: np.log10(z)-logpgv_gmm_C(x, y)
    # derivative of residual function: -dy/dM
    fp = lambda x, y, z: -ddm_gmm_C(x, y)
    # using Newton-Raphson algorithm
    return so.newton(f, x0, fprime=fp, args=(r, pgv),
                     tol=1.e-5, maxiter=50, disp=False)


def PV09C_r_by_pgv_m(pgv, M):
    """Get distance from given PGV and M by GMM C in IMO report 2009-012."""
    import numpy as np
    return (np.power(pgv*np.power(10., 4.96-1.05*M), -1/1.69)
            - 0.00299*np.power(10., 0.621*M))


def get_pgv(st, comp=None):
    """
    Get peak ground values of all components and some combinations.

    Returns a DataFrame with one row per station. Absolute maxima of the
    E, N, Z channels, and some combinations of only horizontal channels,
    i.e. largest value of both (LV), geometric mean (GM), rotation-
    invariant maximum (VH = hor. vector length), vector length of all
    3 channels (V3).
    If velocity traces given, returns PGV, for acceleration PGA.

    Parameters
    ----------
    st : obspy.stream
        Stream to get data from. Assuming all stations have exactly three
        traces.
    comp : str
        Can select to get just one specific PGV component, e.g. 'LV' or 'V3'.

    Returns
    -------
    pandas.DataFrame
        DataFrame, indices = station codes, seven columns.
        If type

    """
    import numpy as np
    import pandas

    cod = station_stream_list(st)
    cols = ['E', 'N', 'Z', 'LV', 'GM', 'VH', 'V3']
    if comp not in cols:
        comp = None
    if comp:
        df = pandas.DataFrame(index=cod, columns=[comp])
    else:
        df = pandas.DataFrame(index=cod, columns=cols)
    e = 'HHE'
    n = 'HHN'
    z = 'HHZ'
    for i in range(0, len(st), 3):
        s = st[i].stats.station
        c = {}  # make sure get correct indices for channels e,n,z
        c[st[i].stats.channel] = i
        c[st[i+1].stats.channel] = i+1
        c[st[i+2].stats.channel] = i+2
        if comp:
            if comp == 'E':
                df.loc[s, 'E'] = np.abs(st[c[e]].data).max()
            elif comp == 'N':
                df.loc[s, 'N'] = np.abs(st[c[n]].data).max()
            elif comp == 'Z':
                df.loc[s, 'Z'] = np.abs(st[c[z]].data).max()
            elif comp == 'LV':
                df.loc[s, 'LV'] = max([np.abs(st[c[e]].data).max(),
                                       np.abs(st[c[n]].data).max()])
            elif comp == 'GM':
                df.loc[s, 'GM'] = np.sqrt(np.abs(st[c[e]].data *
                                                 st[c[n]].data)).max()
            elif comp == 'VH':
                df.loc[s, 'VH'] = np.sqrt(np.square(st[c[e]].data) +
                                          np.square(st[c[n]].data)).max()
            elif comp == 'V3':
                df.loc[s, 'V3'] = np.sqrt(np.square(st[c[z]].data) +
                                          np.square(st[c[e]].data) +
                                          np.square(st[c[n]].data)).max()
        else:
            df.loc[s, 'E'] = np.abs(st[c[e]].data).max()
            df.loc[s, 'N'] = np.abs(st[c[n]].data).max()
            df.loc[s, 'Z'] = np.abs(st[c[z]].data).max()
            df.loc[s, 'LV'] = df.loc[s, ['E', 'N']].values.max()
            df.loc[s, 'GM'] = np.sqrt(np.abs(st[c[e]].data *
                                             st[c[n]].data)).max()
            df.loc[s, 'VH'] = np.sqrt(
                np.square(st[c[e]].data) + np.square(st[c[n]].data)).max()
            df.loc[s, 'V3'] = np.sqrt(np.square(st[c[z]].data) +
                np.square(st[c[e]].data) + np.square(st[c[n]].data)).max()
    return df


def get_pgv_time(st, comp=None, t_start=None, t_stop=None):
    """
    Get peak ground values and their times from all stations in stream.

    Returns a DataFrame with one row per station. Absolute maxima of the
    E, N, Z channels, and some combinations of only horizontal channels,
    i.e. largest value of both (LV), geometric mean (GM), rotation-
    invariant maximum (VH = hor. vector length), vector length of all
    3 channels (V3) can be chosen.
    The corresponding time of the PGV measurement is given in column 'pgvtime'.
    If velocity traces given, returns PGV, for acceleration PGA.
    
    Two lists of start and stop times with an entry for each station can be
    given to limit the PGV search to those time windows (optional).

    Parameters
    ----------
    st : obspy.stream
        Stream to get data from. Assuming all stations have exactly three
        traces.
    comp : str
        Can select to get just one specific PGV component, e.g. 'LV' or 'V3'.
        Default None will return all component combinations.
    t_start : list of obspy.UTCDateTime
        Absolute times for each station when to start the analysis window.
        The order must be the same as the stations in the stream.
        If given, must also give t_stop. None means use entire stream.
    t_stop : list of obspy.UTCDateTime
        Absolute times for each station when to stop the analysis window.

    Returns
    -------
    pandas.DataFrame
        DataFrame, indices = station codes, columns = comp, 'pgvtime'.
        The 'pgvtime' is given in obspy.UTCDateTime.

    """
    import numpy as np
    import pandas

    tcol = 'pgvtime'
    cod = station_stream_list(st)
    cols = ['E', 'N', 'Z', 'LV', 'GM', 'VH', 'V3']
    if comp not in cols:
        comp = None
    if comp:
        df = pandas.DataFrame(index=cod, columns=[comp, tcol], dtype=np.float)
    else:
        df = pandas.DataFrame(index=cod, columns=cols, dtype=np.float)
    e = 'HHE'
    n = 'HHN'
    z = 'HHZ'
    if t_start is not None and t_stop is None:
            raise RuntimeError('Require t_stop if t_start given!')
    if t_start is None and t_stop is not None:
            raise RuntimeError('Require t_start if t_stop given!')
    for i in range(0, len(st), 3):
        s = st[i].stats.station
        c = {}  # make sure get correct indices for channels e,n,z
        c[st[i].stats.channel] = i
        c[st[i+1].stats.channel] = i+1
        c[st[i+2].stats.channel] = i+2
        Q = {}
        k = int(1/3)
        if t_start is not None:
            Q[e] = st[c[e]].slice(starttime=t_start[k], endtime=t_stop[k])
            Q[n] = st[c[n]].slice(starttime=t_start[k], endtime=t_stop[k])
            Q[z] = st[c[z]].slice(starttime=t_start[k], endtime=t_stop[k])
        else:
            Q[e] = st[c[e]].copy()
            Q[n] = st[c[n]].copy()
            Q[z] = st[c[z]].copy()
        if comp:
            if comp == 'E':
                comptrace = np.abs(Q[e].data)
            elif comp == 'N':
                comptrace = np.abs(Q[n].data)
            elif comp == 'Z':
                comptrace = np.abs(Q[z].data)
            elif comp == 'LV':
                compEmax = np.abs(Q[e].data).max()
                compNmax = np.abs(Q[n].data).max()
                if compEmax > compNmax:
                    comptrace = np.abs(Q[e].data)
                else:
                    comptrace = np.abs(Q[n].data)
            elif comp == 'GM':
                comptrace = np.sqrt(np.abs(Q[e].data *
                                           Q[n].data))
            elif comp == 'VH':
                comptrace = np.sqrt(np.square(Q[e].data) +
                                    np.square(Q[n].data))
            elif comp == 'V3':
                comptrace = np.sqrt(np.square(Q[z].data) +
                                    np.square(Q[e].data) +
                                    np.square(Q[n].data))
            else:
                raise RuntimeError(
                    'Unknown component name for PGV: {}'.format(comp))
            tpgv = (Q[e].stats.starttime +
                    comptrace.argmax() * Q[e].stats.delta)
            df.loc[s, [comp, tcol]] = [comptrace.max(), tpgv]
        else:
            df.loc[s, 'E'] = np.abs(Q[e].data).max()
            df.loc[s, 'N'] = np.abs(Q[n].data).max()
            df.loc[s, 'Z'] = np.abs(Q[z].data).max()
            df.loc[s, 'LV'] = df.loc[s, ['E', 'N']].values.max()
            df.loc[s, 'GM'] = np.sqrt(np.abs(Q[e].data *
                                             Q[n].data)).max()
            df.loc[s, 'VH'] = np.sqrt(
                                    np.square(Q[e].data) +
                                    np.square(Q[n].data)).max()
            df.loc[s, 'V3'] = np.sqrt(
                                    np.square(Q[z].data) +
                                    np.square(Q[e].data) +
                                    np.square(Q[n].data)).max()
    return df


def plot_pgv_r(df, outfile='plot_pgv_r.png', M=None, title=None,
               label_outliers=False, label_clipped=False, label_index=None):
    """
    Create and save PGV vs Repi plot.

    Parameters
    ----------
    df : pandas.DataFrame
        Contains columns 'repi' and 'pgv', index should be station names.
        Optionally a bool column 'clip' can be used to mark any clipped
        station's dots by red crosses '+' and labels if label_clipped=True.
    outfile : str, optional
        Filepath for output image. The default is 'plot_pgv_r.png'.
    M : float, optional
        Reference magnitude value. Plot gray dashed line, and if
        label_outliers, calc misfit to data and mark and label data points
        with high misfit. The default is None.
    title : str, optional
        Plot title text. The default is None.
    label_outliers : bool, optional
        If true and M given, mark and label outliers. The default is False.
    label_clipped : bool, optional
        If true and have df['clip'], mark and label clipped station data.
        The default is False.

    Returns
    -------
    None.

    """
    import matplotlib.pyplot as plt
    import numpy as np

    dpi = 200
    fig, ax = plt.subplots(figsize=(8, 6), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    # gray line for reference magntiude:
    if M:
        r = np.logspace(0.5,  2.5, 50)
        y = pgv_gmm_C(M, r)
        ax.loglog(r, y, '--', color='gray')
    # plot all data points:
    ax.loglog(df['repi'].values.tolist(),
              df['pgv'].values.tolist(), 'o')
    # determine and mark/label outliers:
    if label_outliers and M:
        yp = pgv_gmm_C(M, df['repi'].values.tolist())
        df = df.assign(yp=yp)
        df = df.assign(err=np.log10((df['pgv']/df['yp']).astype(np.float64)))
        dfo = df[abs(df['err']) > 0.6]  # 0.6|0.7|1 : factor 4|5|10 error
        if not dfo.empty:
            ax.loglog(dfo['repi'].values.tolist(),
                      dfo['pgv'].values.tolist(),
                      'x', color='k')
        if not dfo.empty and len(dfo) < 100:
            for tt, xx, yy in zip(dfo.index, dfo['repi'], dfo['pgv']):
                ax.text(xx*1.03, yy*1.05, tt)
    # mark/label clipped data:
    if label_clipped and 'clip' in df and not df['clip'].empty:
        dfc = df[df['clip']]
        ax.loglog(dfc['repi'].values.tolist(),
                  dfc['pgv'].values.tolist(), '+', color='r')
        for tt, xx, yy in zip(dfc.index, dfc['repi'], dfc['pgv']):
            ax.text(xx*0.92, yy*1.1, tt, color='r')
    # always mark/label specific stations if list given:
    if label_index:
        dfi = df[[x in label_index for x in df.index]]
        if not dfi.empty:
            ax.loglog(dfi['repi'].values.tolist(),
                      dfi['pgv'].values.tolist(),
                      'o', color='indigo')
            for tt, xx, yy in zip(dfi.index, dfi['repi'], dfi['pgv']):
                ax.text(xx*0.92, yy*1.1, tt, color='indigo')
    # finish plot settings, title, save to file:
    ax.grid(b=True, which='major')
    ax.set_xlabel('epicentral distance (km)')
    ax.set_ylabel('PGV (m/s)')
    if title:
        ax.set_title(title)
    fig.savefig(outfile, bbox_inches='tight')
    plt.close()
    return None


def plot_var_reltime(var, theTime, originTime=None,
                     outfile='plot_r_t.png',
                     xlabel='epicentral distance (km)',
                     ylabel='travel time (s)',
                     title=None):
    """
    Create and save variable vs. relative time plot.

    Parameters
    ----------
    var : float array
        Variable for x axis.
    theTime : float array or obspy.UTCDateTime array
        Variable for y axis. Can be absolute datetime, but then need originTime
        value to calcualte raltive time.
    originTime : obspy.UTCDateTime, datetime, optional
        Absolute x-axis origin time. The default is None.
    outfile : str, optional
        Filepath for output image. The default is 'plot_r_t.png'.
    xlabel : TYPE, optional
        DESCRIPTION. The default is 'epicentral distance (km)'.
    ylabel : TYPE, optional
        DESCRIPTION. The default is 'travel time (s)'.
    title : TYPE, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    None.

    """
    import matplotlib.pyplot as plt
    dpi = 200
    fig, ax = plt.subplots(figsize=(8, 6), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    if originTime is not None:
        try:
            # hopefully both datetime.datetime, then
            # must also be both either with or without timezone awareness
            theTime = (theTime - originTime).apply(lambda x: x.total_seconds())
        except TypeError:
            # both obspy.UTCDateTime
            theTime = theTime - originTime
        except AttributeError:
            # both obspy.UTCDateTime
            theTime = theTime - originTime
    ax.plot(var, theTime, 'o')
    # finish plot settings, title, save to file:
    ax.grid(b=True, which='major')
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    fig.savefig(outfile, bbox_inches='tight')
    plt.close()
    return None


def quick_trig(st, stationlist, sta, lta):
    from obspy import Stream
    st_trig = Stream()
    for s in stationlist:
        x = st.select(station=s, component='Z')
        st_trig.append(x[0].copy())
    st_trig.trigger('recstalta', sta=sta, lta=lta)
    st_trig.plot(equal_scale=False)


def stream_pick(st, flo=1., fhi=35., tlta=5., tsta=0.25, skip=None):
    """
    Pick P and S onsets on entire stream using AR-picker.

    Pick parameters are fixed here, this is a test routine for now...

    Parameters
    ----------
    st : obspy.stream
        Stream of E,N,Z traces for one or more stations.
    skip : float
        Skip the first x seconds. Can be useful to avoid triggering
        at start of tapered traces.

    Returns
    -------
    None.

    """
    from obspy.signal.trigger import ar_pick
    import pandas as pd
    slist = station_stream_list(st)
    df = pd.DataFrame(index=slist, columns=['P', 'S'])
    for s in slist:
        tE = st.select(station=s, component='E')[0]
        tN = st.select(station=s, component='N')[0]
        tZ = st.select(station=s, component='Z')[0]
        fs = tE.stats.sampling_rate
        if skip:
            tE = tE.slice(tE.stats.starttime + skip, tE.stats.endtime)
            tN = tN.slice(tN.stats.starttime + skip, tN.stats.endtime)
            tZ = tZ.slice(tZ.stats.starttime + skip, tZ.stats.endtime)
        p_pick, s_pick = ar_pick(tZ.data, tN.data, tE.data, fs,
                                 1.0, 35.0, 5.0, 0.25, 4.0, 1.0,
                                 2, 8, 0.1, 0.2)
        df.loc[s] = [tZ.stats.starttime + p_pick,
                     tZ.stats.starttime + s_pick]
    return df


def xfft(a, dt):
    """
    Get one-sided fft of real signal, correctly scaled.

    Parameters
    ----------
    a : float array
        Real input signal, any imag. part will be ignored.
    dt : float
        Sample interval, dt = 1./sampling_frequency.

    Returns
    -------
    f : float array
        Frequency vector for FAS and phase (0...fNyquist).
    FAS : float array
        Fourier amplitude spectrum, FAS = abs(fft(a)).
    phase : float array
        Fourier phase spectrum, phase = angle(fft(a)).
    """
    import numpy as np
    from scipy import fft
    n = len(a)
    Y = fft.rfft(a) * dt  # scale with sample length
    f = fft.rfftfreq(n, dt)
    phase = np.angle(Y)
    FAS = np.abs(Y)
    return f, FAS, phase


def eduration(acc, dt, limits=[0.05, 0.95], intype='acc'):
    """
    Get acceleration time series energy duration.

    Parameters
    ----------
    acc : float array
        Acceleration time series. If velocity given, can set 'intype'
        to 'vel' to correctly differentiate and use energy based on acc.
    dt : float
        Sample interval [sec].
    limits : 2 element list, optional
        Lower and upper energy limit to find. The default is [0.05, 0.95].
    intype : str, optional
        Can give velocity data and indicate with 'vel'.
        The default is 'acc'.

    Returns
    -------
    Tdur : float
        Time span between lower and upper energy limit (duration in sec).
    tt : 2 element array
        Relative times of lower and upper energy limit.
    ii : 2 element array
        Indices of lower and upper energy limit.
    """
    import numpy as np
    import scipy as sp
    if intype == 'vel':
        acc = np.diff(acc)
    acc = sp.signal.detrend(acc)
    acc = np.cumsum(np.square(acc))
    i1 = np.where(acc <= limits[0]*acc[-1])[0].max()
    i2 = np.where(acc >= limits[1]*acc[-1])[0].min()
    Tdur = (i2-i1)*dt
    ii = np.array([i1, i2])
    tt = ii*dt
    return Tdur, tt, ii


def costaper(data, alpha=0.05, compensate=True):
    """
    Apply cosine taper (Tukey window) to data.

    Parameters
    ----------
    data : float array
        Input data to apply window on.
    alpha : float, optional
        Taper width parameter. Half of this ratio is tapered left and
        half right. The default is 0.05.
    compensate : bool, optional
        Correct trace energy for taper loss. The default is True.

    Returns
    -------
    float array
        Data with applied taper window and energy-loss compensation.
    """
    import scipy as sp
    import numpy as np
    dtap = data * sp.signal.tukey(len(data), alpha=alpha)
    # compensate energy loss
    if compensate:
        e1 = np.sum(np.square(data))
        e2 = np.sum(np.square(dtap))
        return dtap*np.sqrt(e1/e2)
    else:
        return dtap


def noise_signal_spectra(st, dfPick, outdir=None, fn_prefix=None, pdict=None):
    """
    Get noise and S-wave spectra.

    Parameters
    ----------
    st : obspy.Stream
        Stream with velocity traces.
    dfPick : pandas.DataFrame
        Dataframe with station names as indices and P and S column
        containung absolute time pick values.

    Returns
    -------
    (FFSIGNAL, FFNOISE)
    Two lists wich contain tuples of frequency and amplitude for each trace
    in stream/trace order of S-signal and noise window, respectively. 
    """
    import matplotlib.pyplot as plt
    from u_io import get_def
    import os
    if outdir:
        os.makedirs(outdir, exist_ok=True)
    if not fn_prefix:
        fn_prefix = ''
    if not pdict:
        pdict = {}
    # fraction of energy to stop S window
    estop = get_def(pdict, 'estop', 0.8)
    # time (s) to start window before S pick time
    tspre = get_def(pdict, 'tspre', 1.)
    # time (s) to stop window after estop energy for S spectrum
    tpost = get_def(pdict, 'tpost', 1.)
    # desired noise length (s)
    tnoise = get_def(pdict, 'tnoise', 30.)
    # time (s) to stop noise window before P pick
    tppre = get_def(pdict, 'tppre', 3.)

    FFSIGNAL = []
    FFNOISE = []
    dpi = 200
    for tr in st:
        sta = tr.stats.station
        dt = tr.stats.delta
        n = tr.stats.npts
        if sta not in dfPick.index:
            FFSIGNAL.append((0, 0))
            FFNOISE.append((0, 0))
            print('Warning: No pick for {}'.format(sta))
            continue
        # S-wave spectrum
        tp, ts = dfPick.loc[sta]
        if estop > 0.:  # use energy duration
            ii = eduration(tr.data, dt, [0.05, estop], 'vel')[2]
        else:  # or use S pick time
            ii = [0, int((ts - tr.stats.starttime)/dt)]
        j1 = (ts - tspre - tr.stats.starttime) / dt
        jj = int(j1), int(ii[1]+tpost/dt+1)
        if jj[0] < 0:
            jj[0] = 0
        if jj[1] > n:
            jj[1] = n
        dsig = tr.data[jj[0]:jj[1]]
        n_s = len(dsig)
        d_s = n_s*dt
        dtap = costaper(dsig, 0.05)
        fs, As, phs = xfft(dtap, dt)
        # Noise spectrum
        j2 = int((tp - tppre - tr.stats.starttime) / dt)
        j1 = int(j2 - tnoise/dt)
        if j1 < 0 or j2 < 0:
            FFSIGNAL.append((0, 0))
            FFNOISE.append((0, 0))
            print('Warning: not enough noise for {}'.format(sta))
            continue
        dnos = tr.data[j1:j2+1]
        n_n = len(dnos)
        d_n = n_n*dt
        dtap = costaper(dnos, 0.05)
        fn, An, phn = xfft(dtap, dt)

        FFSIGNAL.append((fs, As))
        FFNOISE.append((fn, An))
        if not outdir:
            continue
        # create figures (debug)
        pfig = os.path.join(outdir, fn_prefix + tr.id + '.png')
        fig, ax = plt.subplots(nrows=1, figsize=(4, 4.5), dpi=dpi)
        fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
        ax.loglog(fn, An, 'g', linewidth=2)
        ax.loglog(fs, As, 'k', linewidth=2)
        ax.annotate('N_s = {}, T_s = {:g}'.format(n_s, d_s),
                    (0.05, 0.2), xycoords='axes fraction')
        ax.annotate('N_n = {}, T_n = {:g}'.format(n_n, d_n),
                    (0.05, 0.1), xycoords='axes fraction')
        ax.grid(True)
        ax.set_xlabel('Frequency (Hz)')
        ax.set_ylabel('Amplitude (m/s)')
        ax.set_title('{}: Spectra of S-wave and noise'.format(tr.id))
        fig.savefig(pfig, bbox_inches='tight')
        plt.close('all')
    return FFSIGNAL, FFNOISE


def plot_fft(st, legend_out=True, show=True):
    """
    Create Fourier amplitude spectrum plot of given obspy stream traces.

    Parameters
    ----------
    st : obspy.stream
        Traces to be transformed and plotted.
    legend_out : bool, optional
        Whether the legend should be outside the plot. The default is True.
    show : bool, optional
        If True, show the plot interactively after plotting and do not return
        a figure handle object, else return the object. Defaults to True.
    Returns
    -------
    fig : TYPE
        DESCRIPTION.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    dpi = 200
    n = len(st)
    xlim = [np.inf, -np.inf]
    ylim = [np.inf, -np.inf]
    fig, ax = plt.subplots(nrows=1, figsize=(8, 5.5), dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    for tr in st:
        y = tr.data.astype(np.float)
        y -= y.mean()
        f, fas, _ = xfft(y, tr.stats.delta)
        f = f[1:-1]
        fas = fas[1:-1]
        xlim = [np.min([f[0], xlim[0]]), np.max([f[-1], xlim[1]])]
        ylim = [np.min([fas.min(), ylim[0]]), np.max([fas.max(), ylim[1]])]
        ax.loglog(f, fas, label=tr.id)
    ax.set_xlim(xlim[0], xlim[1])
    ax.set_ylim(ylim[0]*0.8, ylim[1]*1.2)
    ax.grid(True)
    ax.set_xlabel('Frequency (Hz)')
    ax.set_ylabel('Amplitude')
    if legend_out:
        ax.legend(loc='upper left', bbox_to_anchor=(1.01, 1.0))
    else:
        ax.legend()
    plt.tight_layout()
    if show:
        plt.show()
    else:
        return fig


def print_stream_info(traces):
    """
    just print stream info to terminal.
    """
    print(traces.__str__(extended=True))
    return


def save_obspy_PIL(fpng, st, mode='1'):
    """Create obspy stream plot and save as file using pillow (PIL)."""
    import matplotlib.pyplot as plt
    n = len(st)
    dpi = 100  # dots per inch (figure resolution setting)
    dpph = 250  # pixels per traceplot height
    dppw = 680  # pixels per traceplot width
    pleft = 80  # pixels on left figure margin, fixed in obspy to 80
    pright = 40  # pixels on right figure margin, fixed in obspy to 40
    pbottom = 40  # pixels on bottom figure margin, fixed in obspy to 40
    ptop = 60  # pixels on top figure margin, fixed in obspy to 60
    pixh = n*dpph + pbottom + ptop  # total figure height in pixels
    pixw = dppw + pleft + pright  # total figure width in pixels
    height = pixh/dpi
    width = pixw/dpi

    cfig = plt.figure(figsize=(width, height), dpi=dpi)
    cfig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    st.plot(fig=cfig, equal_scale=False, size=(pixw, pixh))
    si = get_stream_info(st)
    suptitle = '{}  -  {}'.format(str(si['start']).rstrip("Z0").rstrip("."),
                                  str(si['end']).rstrip("Z0").rstrip("."))
    cfig.suptitle(suptitle, y=((pixh-15.)/pixh), fontsize='small',
                  horizontalalignment='center')
    save_fig_PIL(cfig, pixw, pixh, fpng, mode=mode)
    plt.close()


def save_fig_PIL(fig, pixw, pixh, fpng, mode='1',
                 optimize=False, compress_level=1, colors=8):
    """
    Save figure as PNG file using pillow (PIL) directly.

    Parameters
    ----------
    fig : matplotlib.figure.Figure
        Figure handle.
    pixw : int
        Width of figure in pixels.
    pixh : int
        Height of figure in pixels.
    fpng : str
        Output filepath.
    mode : str
        PIL image mode to convert to. E.g. '1' (1bit black and white),
        'L' (8bit grayscale), 'P' (8bit color palette), 'RGB' etc.
        The default is '1'.
    optimize : bool
        Optimize PNG file size. If true, compress_level is ignored (auto 9).
        The ddefault is False.
    compress_level : int
        ZLIB compression level from 0 to 9. 0 is no compression, 1 is fastest,
        9 is best compression. Fixed to 9 if optimize=True. The default is 1.
    colors : int
        Only used if mode='P'. Number of palette colors. The default is 8.

    Returns
    -------
    None.
    """
    import warnings
    from PIL import Image
    from io import BytesIO
    import matplotlib.pyplot as plt
    DATELOCATOR_WARNING_MSG = (
        "AutoDateLocator was unable to pick an appropriate interval for this "
        "date range. It may be necessary to add an interval value to the "
        "AutoDateLocator's intervald dictionary.")
    with warnings.catch_warnings(record=True):
        warnings.filterwarnings("ignore", DATELOCATOR_WARNING_MSG,
                                UserWarning, "matplotlib.dates")
        buf = BytesIO()
        plt.savefig(buf, format='raw')
        buf.seek(0)
        pil_im = Image.frombytes('RGBA', size=(pixw, pixh),
                                 data=buf.getvalue())
        if mode == 'P':
            pil_im.convert(mode, palette=Image.ADAPTIVE,
                           colors=colors,
                           dither=Image.NONE).save(
                fpng, format='PNG', optimize=optimize,
                compress_level=compress_level)
        else:
            pil_im.convert(mode, dither=Image.NONE).save(
                fpng, format='PNG', optimize=optimize,
                compress_level=compress_level)
    return None


def save_bw_traceplot(fpng, stream, title='', figsize=(12.,6.75), dpi=200):
    """
    Create obspy trace plot, put title, then use pillow (PIL) to convert
    and save image as 1-bit PNG (black and white) to save space.
    Fine for figures that don't need very high quality or colors.
    """
    from PIL import Image
    import matplotlib.pyplot as plt
    cfig = plt.figure(figsize=figsize, dpi=dpi)
    stream.plot(fig=cfig)
    cfig.axes[0].set_title(title)
    plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, 
                        right=0.995, hspace=0.0, wspace=0.0)
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    pil_im = Image.frombytes('RGB', canvas.get_width_height(), 
                 canvas.tostring_rgb())
    im2 = pil_im.convert('RGB').convert('1')
    im2.save( fpng , format='PNG', optimize=True)
    plt.close()


def combine_figures(figlist,fpng,append_vh='v'):
    """
    Use system ImageMagick to append images together.
    append_vh='v': vertical,
    append_vh='h': horizontal
    """
    import subprocess
    if append_vh == 'v':
        params = ['convert','-append']
    else:
        params = ['convert','+append']
    params.extend(figlist)
    params.append(fpng)
    subprocess.check_call(params)


def save_spectrogram(tr,pdict):
    """
    Create spectrogram of given trace and save figure.
    INPUT:
        tr = obspy trace object
        pdict = dict with spectral/graphical parameters
    """
    import matplotlib.pyplot as plt
    sd = pdict['spectrogram']
    gx = pdict['graphics']
    specg_mult=4.  # Pad zeros to length mult * wlen, spectrogram smoother
    sel_cmap='Greys'  # color map
    fpng = pdict['paths']['pbase'] + '_spectrogram_tr1.png'
    use_cmap = plt.get_cmap(sel_cmap)
    title='ICEARRAY1 ' + str(tr.stats.starttime),
    #
    fig = plt.figure(figsize=gx['fsize_spec'], dpi=gx['dpi'])
    fig.set_dpi(gx['dpi'])  # need to set it again if using 'ps' backend
    ax = fig.add_subplot(111)
    ax = tr.spectrogram(
            axes=ax,
            log=sd['islog'],
            per_lap=sd['overlap'],
            wlen=sd['wlen'],
            mult=specg_mult,
            clip=sd['clip'],
            cmap=use_cmap)
    ax.set_ylim([sd['fmin'],sd['fmax']])
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Frequency [Hz]')
    ax.set_title(title)
    fig.savefig(fpng, bbox_inches = "tight")
    plt.close()


def save_multi_spectrogram(st,pdict,fpng=None):
    """
    Create spectrograms of all traces in stream and save as one figure.
    INPUT:
        st = obspy stream object
        pdict = dict with spectral/graphical parameters
    """
    import matplotlib.pyplot as plt
    sd = pdict['spectrogram']
    gx = pdict['graphics']
    specg_mult=4.  # Pad zeros to length mult * wlen, spectrogram smoother
    sel_cmap='Greys'  # color map
    if fpng is None:
        fpng = pdict['paths']['pbase'] + '_spectrograms.png'
    use_cmap = plt.get_cmap(sel_cmap)
    n = len(st)
    title='ICEARRAY1, {}, (Nstat={})'.format(
            str(st[0].stats.starttime),n)
    #
    fig,axes=plt.subplots(
            n,1,sharex=True,sharey=True,squeeze=True,
            gridspec_kw={'hspace':0.},
            figsize=gx['fsize_spec'], dpi=gx['dpi'])
    fig.set_dpi(gx['dpi'])  # need to set it again if using 'ps' backend
    for i in range(0,n):
        st[i].spectrogram(
                axes=axes[i],
                log=sd['islog'],
                per_lap=sd['overlap'],
                wlen=sd['wlen'],
                mult=specg_mult,
                clip=sd['clip'],
                cmap=use_cmap)
        axes[i].set_ylim([sd['fmin'],sd['fmax']])
        axes[i].text(0.01, 0.95, st[i].stats.station,
            horizontalalignment='left', verticalalignment='top',
            transform=axes[i].transAxes)
        axes[i].grid(b=True,axis='x',linestyle='--',color=(.8,.8,.8))
    k=int(n/2)  # middle subplot gets ylabel
    axes[n-1].set_xlabel('Time [s]')
    axes[k].set_ylabel('Frequency [Hz]')
    axes[0].set_title(title)
    fig.savefig(fpng, bbox_inches = "tight")
    plt.close()


def simple_streamplot(st, pdict, fpng):
    """
    Make simple streamplot without obspy plot method (mainly for testing).

    Parameters
    ----------
    st : obspy.Stream
        Stream of which all traces will be plotted.
    pdict : dict
        Not used, but could contain plot options.
    fpng : str
        Output image file path.

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    dpi=100
    fsize=(12, 8)
    n = len(st)
    fig, axes = plt.subplots(
                n, 1, sharex=True, sharey=False, squeeze=True,
                gridspec_kw={'hspace': 0.},
                figsize=fsize, dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    for i in range(0, n):
        axes[i].plot(st[i].times("matplotlib"), st[i].data, 'k-')
        axes[i].text(0.01, 0.95, st[i].id,
                     horizontalalignment='left', verticalalignment='top',
                     transform=axes[i].transAxes)
        axes[i].xaxis_date()
        axes[i].grid(b=True, axis='x', linestyle='--', color=(.8, .8, .8))
    axes[n-1].set_xlabel('Time [s]')
    fig.savefig(fpng, bbox_inches="tight")
    plt.close()
    return None


def simple_traceplot(tr, pdict, fpng=None):
    """
    Make simple traceplot with xlim/ylim option.

    Parameters
    ----------
    tr : obspy.Trace
        Stream of which all traces will be plotted.
    pdict : dict
        Contains plot options:
        dpi = dots per inch. Default is 100.
        size = figure size in inches. Default is (12, 8).
        xlim = x-axis limits, either given as absolute times in UTCDateTime
            or seconds relative to trace start time (set option 'relt'=True).
            Default is first and last samples of trace.
        relt = If false, xlim is absolute UTCDateTime, if true xlim is
            seconds relative to trace start time. Default is False.
        ylim = y-axis limits. Default is plot auto limits.
        xgrid = If want x-axis grid. Default is False.
        ygrid = If want y-axis grid. Default is False.
        hline = [], supply list of values for horizontal lines. Default None.
        vline = [], supply list of values for vertical lines. Default None.
    fpng : str
        Output image file path.

    Returns
    -------
    None.
    """
    import matplotlib.pyplot as plt
    import u_io

    dpi = u_io.get_def(pdict, 'dpi', 100)
    size = u_io.get_def(pdict, 'size', (12, 8))
    xlim = u_io.get_def(pdict, 'xlim', None)
    ylim = u_io.get_def(pdict, 'ylim', None)
    relt = u_io.get_def(pdict, 'relt', False)
    xgrd = u_io.get_def(pdict, 'xgrid', False)
    ygrd = u_io.get_def(pdict, 'ygrid', False)
    hlin = u_io.get_def(pdict, 'hline', None)
    vlin = u_io.get_def(pdict, 'vline', None)
    fig, ax = plt.subplots(1, 1, figsize=size, dpi=dpi)
    fig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    if xlim:
        if relt:
            dt = tr.stats.starttime
            trC = tr.slice(dt+xlim[0], dt+xlim[1])
        else:
            trC = tr.slice(xlim[0], xlim[1])
    else:
        trC = tr.copy()
    times = trC.times("matplotlib")
    ax.plot(times, trC.data, 'k-')
    if ylim:
        ax.set_ylim(ylim)
    ax.set_xlim([times[0], times[-1]])
    ax.text(0.01, 0.95, trC.id,
            horizontalalignment='left', verticalalignment='top',
            transform=ax.transAxes)
    ax.xaxis_date()
    if hlin:
        for v in hlin:
            ax.axhline(v, color='k', lw=2)
    if vlin:
        for v in vlin:
            ax.axvline(v, color='k', lw=2)
    if xgrd:
        ax.grid(b=True, axis='x', linestyle='--', color=(.8, .8, .8))
    if ygrd:
        ax.grid(b=True, axis='y', linestyle='--', color=(.8, .8, .8))
    if fpng:
        fig.savefig(fpng, bbox_inches="tight")
        plt.close()
        return None
    return fig


def points_in_polygon(polygon, points):
    """
    Test whether points located within polygon.

    Parameters
    ----------
    polygon : list or numpy.array
        List of 2D coordiante points taken as polygon corners. Last point
        does not need to be same as first, will be closed automatically.
    points : list or numpy.array
        List of 2D coordiante points to check if they are within the polygon.

    Returns
    -------
    bool
        Boolean array of same length as given points array. Elements are true
        if points are within polygon, else false.
    """
    import matplotlib.path as mpath
    import numpy as np

    polygon = np.array(polygon)
    points = np.array(points)
    path = mpath.Path(polygon)
    return path.contains_points(points)


def df_in_polygon(df, polygon, xcol='longitude', ycol='latitude'):
    """
    Filter dataframe by location within polygon.

    Parameters
    ----------
    df : pandas.DataFrame
        Must contain at least two columns with x- and y-coordinates. Their
        names can be given in xcol and ycol.
    polygon : list or numpy.array
        List of 2D coordiante points taken as polygon corners. Last point
        does not need to be same as first, will be closed automatically.
    xcol : string, optional
        Column name to use for x-coordinates. The default is 'longitude'.
    ycol : string, optional
        Column name to use for y-coordinates. The default is 'latitude'.

    Returns
    -------
    pandas.DataFrame
        Dataframe containing only rows located within the polygon.

    """
    points = df[[xcol, ycol]].values.tolist()
    b = points_in_polygon(polygon, points)
    return df[b]


def moment2mw(M0):
    """Get moment magntidue from moment M0 [Nm]."""
    import numpy as np
    return (np.log10(np.abs(M0)) + 7)*2/3-10.7
