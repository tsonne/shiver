#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 19:05:32 2020

@author: ts
"""


def attach_coordinates(st,inventories):
    """
    Search for station coordinates in inventory and attach to each trace
    as trace.stats.coordinates. Returns list of traces missing info.
    
    :type st: :class:`~obspy.core.stream.Stream` 
        or :class:`~obspy.core.trace.Trace`
    :param st: Stream of trace, if stream all traces will be handled.
    :type inventories: :class:`~obspy.core.inventory.inventory.Inventory`
        or :class:`~obspy.core.inventory.network.Network` or a list
        containing objects of these types.
    :param inventories: Station metadata to use in search for coordinates for
        this trace or each trace in the stream.
    :rtype: list of :class:`~obspy.core.trace.Trace`
    :returns: List of traces for which no metadata could be found.
    """
    import warnings
    skipped_traces = []
    if not hasattr(st,'traces'):
        st = [st]
    for tr in st:
        try:
            tr.stats.coordinates = inventories.get_coordinates(
                    tr.id, tr.stats.starttime)
        except Exception as e:
            if str(e) == "No matching channel metadata found.":
                warnings.warn(str(e))
                skipped_traces.append(tr)
            else:
                raise
    return skipped_traces


def get_station_coords_from_stream(st):
    """
    Get station coordinates as stored in given stream 'st', assuming
    lon, lat and elevation are stored. Return them as numpy array.
    """
    import numpy as np
    coords=[]
    for tr in st:
        coords.append([tr.stats.coordinates.longitude,
                       tr.stats.coordinates.latitude,
                       tr.stats.coordinates.elevation])
    return np.array(coords)


def read_station_table(file, sep=r'\s+', header=0):
    import pandas as pd
    return pd.read_csv(file, header=header, sep=sep)


def array_transfer_k_plot(coords, klim=40., kstep=0.4, coordsys='lonlat',
                          plottype='contourf'):
    import numpy as np
    import matplotlib.pyplot as plt
    from obspy.signal.array_analysis import array_transff_wavenumber

    # compute transfer function as a function of wavenumber difference
    tf = array_transff_wavenumber(coords, klim, kstep, coordsys=coordsys)
    # plot
    fig, ax = plt.subplots(1, 1, figsize=(4, 3.5), dpi=200,
                           subplot_kw={'aspect': 'equal'})
    if plottype == 'pcolor':
        x = np.arange(-klim, klim + kstep * 1.1, kstep) - kstep / 2.
        img = ax.pcolor(x, x, tf.T, cmap='jet', vmin=0., vmax=1.)
    elif plottype == 'pcolormesh':
        imax = klim+kstep/2.
        inum = 2*int(klim/kstep+1)
        y, x = np.mgrid[-imax:imax:inum*1j, -imax:imax:inum*1j]
        img = ax.pcolormesh(x, y, tf.T, cmap='jet', vmin=0., vmax=1.)
    elif plottype == 'contourf':
        y, x = np.mgrid[-klim:klim:int(2*klim/kstep+1)*1j,
                        -klim:klim:int(2*klim/kstep+1)*1j]
        img = ax.contourf(x, y, tf.T, levels=19, cmap='jet', vmin=0., vmax=1.)
    else:
        raise RuntimeError('Unknown plottype: {}'.format(plottype))
    ax.set_xlabel('wave number X (rad/km)')
    ax.set_ylabel('wave number Y (rad/km)')
    ax.set_title('Array Transfer Function')
    fig.colorbar(img)
    return fig


def save_array_transfer_k_plot(st,fpng,coordsys='lonlat'):
    """
    Calculate the array transfer function in the wavenumber domain,
    save the plot to filename 'fpng'.
    Use station coordinates as stored in given stream 'st', assuming
    lon, lat and elevation are stored.
    """
    import PIL
    import numpy as np
    import matplotlib.pyplot as plt
    
    from obspy.imaging.cm import obspy_sequential
    from obspy.signal.array_analysis import array_transff_wavenumber
    
    # get array coordinates as numpy array
    npco = get_station_coords_from_stream(st)
    
    # set limits for wavenumber differences to analyze
    klim = 40.
    kxmin = -klim
    kxmax = klim
    kymin = -klim
    kymax = klim
    kstep = klim / 100.
    
    # compute transfer function as a function of wavenumber difference
    transff = array_transff_wavenumber(npco, klim, kstep, coordsys=coordsys)
    
    # plot
    plt.figure(figsize=(4,3.1), dpi=300)
    plt.pcolor(np.arange(kxmin, kxmax + kstep * 1.1, kstep) - kstep / 2.,
               np.arange(kymin, kymax + kstep * 1.1, kstep) - kstep / 2.,
               transff.T, cmap=obspy_sequential)
    
    plt.colorbar()
    plt.clim(vmin=0., vmax=1.)
    plt.xlim(kxmin, kxmax)
    plt.ylim(kymin, kymax)
    plt.xlabel('wave number X (rad/km)')
    plt.ylabel('wave number Y (rad/km)')
    plt.title('Array Transfer Function')
    #plt.savefig(fpng, bbox_inches = "tight")
    plt.subplots_adjust(top=0.92, bottom=0.14, left=0.16, right=0.99)
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    pil_im = PIL.Image.frombytes('RGB', canvas.get_width_height(), 
                 canvas.tostring_rgb())
    im2 = pil_im.convert('RGB',dither=0).convert(
            mode='P',dither=0,palette=PIL.Image.ADAPTIVE)
    im2.save( fpng , format='PNG', optimize=True)
    plt.close()


def save_array_transfer_s_plot(
        st, fpng, 
        slim=0.25, sstep=0.0025,
        fmin=1, fmax=10, fstep=1, 
        coordsys='lonlat'):
    """
    Calculate the array transfer function in the slowness domain,
    assume frequency range and step,
    save the plot to filename 'fpng'.
    Use station coordinates as stored in given stream 'st', assuming
    lon, lat and elevation are stored.
    INPUT:
        stream, output filepath,
        limit and step of slowness difference,
        min/max frequency in signal, freq sample distance,
        coordinate system type 'lonlat' or 'xy'
    """
    import PIL
    import numpy as np
    import matplotlib.pyplot as plt
    
    from obspy.imaging.cm import obspy_sequential
    from obspy.signal.array_analysis import array_transff_freqslowness
    
    # get array coordinates as numpy array
    coords = get_station_coords_from_stream(st)
    
    # set limits for slowness differences to analyze
    sxmin = -slim
    sxmax = slim
    symin = -slim
    symax = slim
    
    # compute transfer function as a function of wavenumber difference
    #transff = array_transff_wavenumber(npco, klim, kstep, coordsys=coordsys)
    transff = array_transff_freqslowness(
            coords, slim, sstep, fmin, fmax, fstep, coordsys=coordsys)
    # plot
    plt.figure(figsize=(4,3.1), dpi=300)
    plt.pcolor(np.arange(sxmin, sxmax + sstep * 1.1, sstep) - sstep / 2.,
               np.arange(symin, symax + sstep * 1.1, sstep) - sstep / 2.,
               transff.T, cmap=obspy_sequential)
    
    plt.colorbar()
    plt.clim(vmin=0., vmax=1.)
    plt.xlim(sxmin, sxmax)
    plt.ylim(symin, symax)
    plt.xlabel('slowness X (sec/km)')
    plt.ylabel('slowness Y (sec/km)')
    plt.title('Array Transfer Function, f=[{},{},{}]'.format(
            fmin,fmax,fstep))
    #plt.savefig(fpng, bbox_inches = "tight")
    plt.subplots_adjust(top=0.92, bottom=0.14, left=0.16, right=0.99)
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    pil_im = PIL.Image.frombytes('RGB', canvas.get_width_height(), 
                 canvas.tostring_rgb())
    im2 = pil_im.convert('RGB',dither=0).convert(
            mode='P',dither=0,palette=PIL.Image.ADAPTIVE)
    im2.save( fpng , format='PNG', optimize=True)
    plt.close()


def save_movfk_plot(fpng, fkdat, ffk, stime, FSizeI=(8,6), myDPI=200):
    """
    Plot FK results and save figure to file.
    INPUT: 
        fpng = output image file path
        fkdat = output from obspy.signal.array_analysis.array_processing()
        ffk = lower,upper corner freq: (f_lo,f_hi)
        stime = moving fk abs. start time (obspy datetime)
        FSizeI = FK plot figure size in inches (width, height)
        myDPI = dpi of figure
    """
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
    #from obspy.imaging.cm import obspy_sequential
    from obspy.imaging.cm import viridis_white_r
    import PIL
    
    labels = ['rel.power', 'abs.power', 'baz', 'slow']
    xlocator = mdates.AutoDateLocator()
    fig = plt.figure(figsize=FSizeI, dpi=myDPI)
    for i, lab in enumerate(labels):
        ax = fig.add_subplot(4, 1, i + 1)
        ax.scatter(fkdat[:, 0], fkdat[:, i + 1], c=fkdat[:, 1], alpha=0.6,
                   edgecolors='none', cmap=viridis_white_r)
        ax.set_ylabel(lab)
        ax.set_xlim(fkdat[0, 0], fkdat[-1, 0])
        ax.set_ylim(fkdat[:, i + 1].min(), fkdat[:, i + 1].max())
        ax.xaxis.set_major_locator(xlocator)
        ax.xaxis.set_major_formatter(mdates.AutoDateFormatter(xlocator))
        ax.grid(b=True,linestyle='--',color=(.8,.8,.8))
    
    fig.suptitle('ICEARRAY1 FK ({:5.2f}, {:5.2f}) {}'.format(
            ffk[0], ffk[1], stime.strftime('%Y-%m-%d %H:%M:%S.%f')))
    fig.autofmt_xdate()
    fig.subplots_adjust(
            left=0.09, top=0.95, right=0.995, bottom=0.12, hspace=0)
    #plt.savefig(fpng)  # last line if not using PIL
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    pil_im = PIL.Image.frombytes('RGB', canvas.get_width_height(), 
                 canvas.tostring_rgb())
    im2 = pil_im.convert('RGB').convert(mode='P')
    im2.save( fpng , format='PNG', optimize=True)
    plt.close()


def save_movfk_plot2(fpng, fkdat, ffk, stime, conf):
    """
    Scatterplot over time of FK results, save figure to file.

    Back azimuth axis fixed from 0 to 360 deg,
    slowness axis fixed from 0 to max searched (slim*sqrt(2))
    INPUT:
        fpng = output image file path
        fkdat = output from obspy.signal.array_analysis.array_processing()
        ffk = lower,upper corner freq: (f_lo,f_hi)
        stime = moving fk abs. start time (obspy datetime)
        conf = dict containing fk settings
            FSizeI = FK plot figure size in inches (width, height)
            myDPI = dpi of figure
    """
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
    # from obspy.imaging.cm import obspy_sequential
    from obspy.imaging.cm import viridis_white_r
    from PIL import Image

    FSizeI = conf['graphics']['fsize_fk']
    myDPI = conf['graphics']['dpi']
    slim = conf['fk']['slim']

    labels = ['rel.power', 'abs.power', 'baz', 'slow']
    # make output human readable, adjust backazimuth to values between 0
    # and 360
    # t, rel_power, abs_power, baz, slow = fkdat.T
    # baz[baz < 0.0] += 360.
    fkdat[fkdat[:, 3] < 0.0, 3] += 360.

    xlocator = mdates.AutoDateLocator()
    fig = plt.figure(figsize=FSizeI, dpi=myDPI)
    for i, lab in enumerate(labels):
        ax = fig.add_subplot(4, 1, i + 1)
        ax.scatter(fkdat[:, 0], fkdat[:, i + 1], c=fkdat[:, 1], alpha=0.6,
                   edgecolors='none', cmap=viridis_white_r)
        ax.set_ylabel(lab)
        ax.set_xlim(fkdat[0, 0], fkdat[-1, 0])
        if i == 2:  # baz
            ax.set_ylim(0., 360.)
        elif i == 3:  # slow
            ax.set_ylim(0., slim*1.42)
        else:
            ax.set_ylim(fkdat[:, i + 1].min(), fkdat[:, i + 1].max())
        ax.xaxis.set_major_locator(xlocator)
        ax.xaxis.set_major_formatter(mdates.AutoDateFormatter(xlocator))
        ax.grid(b=True, linestyle='--', color=(.8, .8, .8))

    fig.suptitle('ICEARRAY1 FK ({:5.2f}, {:5.2f}) {}'.format(
            ffk[0], ffk[1], stime.strftime('%Y-%m-%d %H:%M:%S.%f')))
    # fig.autofmt_xdate()
    fig.subplots_adjust(
            left=0.09, top=0.93, right=0.995, bottom=0.10, hspace=0)
    # plt.savefig(fpng)  # last line if not using PIL
    canvas = plt.get_current_fig_manager().canvas
    canvas.draw()
    pil_im = Image.frombytes('RGB', canvas.get_width_height(),
                             canvas.tostring_rgb())
    im2 = pil_im.convert('RGB').convert(mode='P')
    im2.save(fpng, format='PNG', optimize=True)
    plt.close()


def get_delays_from_geo(geometry,smpdel,baz,slw):
    """
    Using stream st, back azimuth baz and slowness slw,
    return each station's time delay, number of samples to delay,
    and the assumed center coordinates (lon,lat,elevation).
    
    geometry - obspy.signal.array_analysis.get_geometry array
    smpdel - sample length (s), st[0].stats.delta
    baz - back azimuth (deg)
    slw - slowness (s/km)
    """
    from obspy.signal.array_analysis import get_timeshift
    import numpy as np

    pi=np.pi
    n = len(geometry)
    baz=baz*pi/180.
    sx = slw*np.cos(pi/2-baz); # E-W
    sy = slw*np.sin(pi/2-baz); # N-S
    kwargs = dict(sll_x=sx, sll_y=sy, sl_s=.0, grdpts_x=1, grdpts_y=1)
    delay=get_timeshift(geometry[0:n-1], **kwargs)
    # now delay[:,0,0] is a 1D array of timeshifts for each station
    delay= delay[:,0,0]
    # get integer number of samples to shift
    ndelay = delay/smpdel
    ndelay = np.asarray(ndelay.round(), dtype=int)
    return {'delay':delay, 'ndelay':ndelay, 'center':geometry[n-1]}


def get_delays(st,baz,slw):
    """
    Using stream st, back azimuth baz and slowness slw,
    return each station's time delay, number of samples to delay,
    and the assumed center coordinates (lon,lat,elevation).
    
    st - stream
    baz - back azimuth (deg)
    slw - slowness (s/km)
    """
    from obspy.signal.array_analysis import (get_geometry,
                                             get_timeshift)
    import numpy as np
    #
    smpdel = st[0].stats.delta
    pi=np.pi
    geometry = get_geometry(st, coordsys='lonlat', return_center=True)
    n = len(geometry)
    baz=baz*pi/180.
    sx = slw*np.cos(pi/2-baz); # E-W
    sy = slw*np.sin(pi/2-baz); # N-S
    kwargs = dict(sll_x=sx, sll_y=sy, sl_s=.0, grdpts_x=1, grdpts_y=1)
    delay=get_timeshift(geometry[0:n-1], **kwargs)
    # now delay[:,0,0] is a 1D array of timeshifts for each station
    delay= delay[:,0,0]
    # get integer number of samples to shift
    ndelay = delay/smpdel
    ndelay = np.asarray(ndelay.round(), dtype=int)
    return {'delay':delay, 'ndelay':ndelay, 'center':geometry[n-1]}


def get_beam_ndelay(st,ndelay):
    """
    Use copied stream to shift traces and stack to beam.
    ndelay is samples to shift for each station (same order as stream)
    """
    import numpy as np
    zz=st.copy()
    for i, tr in enumerate(zz):
        tr.data = np.roll(tr.data, -ndelay[i])
    #zz.stack() # Obspy manual says this works but it doesn't exist!!!
    beam = np.sum([tr.data for tr in zz], axis=0) / len(zz)
    return beam


def get_beam(st,baz,slw):
    """
    Get stack (beam) using back azimuth and slowness.

    Parameters
    ----------
    st : obspy.stream
        Stream of array data.
    baz : float
        back azimuth (deg).
    slw : float
        slowness (s/km).

    Returns
    -------
    btr : obspy.trace
        Beam as Obspy trace with full metadata.
    """
    from obspy.core import Trace
    dd = get_delays(st,baz,slw)
    beam = get_beam_ndelay(st,dd['ndelay'])
    btr = Trace(
            data=beam,
            header=dict(
                network=st[0].stats.network,
                station='BEAM',
                location=st[0].stats.location,
                channel=st[0].stats.channel,
                delta=st[0].stats.delta,
                starttime=st[0].stats.starttime,
                endtime=st[0].stats.endtime,
                sampling_rate=st[0].stats.sampling_rate,
                npts=st[0].stats.npts,
                coordinates=dict(
                    latitude=dd['center'][1],
                    longitude=dd['center'][0],
                    elevation=dd['center'][2])))
    return btr


def fk_analysis(st, conf, iFB):
    #
    import pickle
    from obspy.signal.array_analysis import array_processing
    from u_obs import save_bw_traceplot
    #
    # assign variables for readability
    k_FB = iFB+1
    sk_FB = str(k_FB)
    cfk = conf['fk']
    stride = cfk['stride']
    presig = cfk['presig']
    postsig = cfk['postsig']
    min_wlen = cfk['min_wlen']
    FIXED_WLEN = cfk['FIXED_WLEN']
    set_wlen = cfk['set_wlen']
    slim = cfk['slim']
    sstep = cfk['sstep']
    pbase = conf['paths']['pbase']
    smpdel = conf['cur']['smpdel']
    time0 = conf['cur']['time0']
    timeZ = conf['cur']['timeZ']
    ons = conf['cur']['ons']
    #
    # corner frequencies
    ffk1 = cfk['fbands'][iFB][0]
    ffk2 = cfk['fbands'][iFB][1]
    fbb1 = ffk1 * 0.9
    fbb2 = ffk2 * 1.1
    # taper, filter and normalize traces
    st_f = st.copy()
    st_f.taper(max_percentage=cfk['taper_perc'])
    st_f.filter('bandpass',
                freqmin=fbb1, freqmax=fbb2, corners=3, zerophase=True)
    st_f.normalize()
    # save figure
    ftitle = 'BandPass({:5.2f}, {:5.2f}), norm'.format(fbb1,fbb2)
    fpng = pbase + '_bb_f' + sk_FB + '.png'
    save_bw_traceplot(fpng, st_f, ftitle)
    #
    # do fk analysis
    # moving window fk analysis, write results to png and binary files,
    # window length is too long for short signals and low frequencies,
    # so have four periods for 16 Hz but two periods at 1 Hz,
    # wlen = 1/fc * ( 0.1333*fc + 1.8667 ),
    # but at least about 0.35 s to take delay over array into account.
    # Stride shall be fixed time length here, so wfrac is calculated
    # from stride and wlen (add small number avoid losing one sample to int()).
    if FIXED_WLEN:
        wlen = set_wlen
    else:
        wlen = (0.1333 * ffk1 + 1.8667) / ffk1
        if wlen < min_wlen:
            wlen = min_wlen
    wlen = float(round(wlen/smpdel))*smpdel
    wfrac = stride/wlen + 0.000001
    time_onset = time0 + ons
    stime = time_onset - presig
    etime = time_onset + postsig
    # adapt if trace too short on either end for desired window around onset
    if stime < time0:
        stime = time0
    if etime > timeZ:
        etime = timeZ
    kwargs = dict(
        # slowness grid: X min, X max, Y min, Y max, Slow Step
        sll_x=-slim, slm_x=slim, sll_y=-slim, slm_y=slim, sl_s=sstep,
        # sliding window properties
        win_len=wlen, win_frac=wfrac,
        # frequency properties
        frqlow=ffk1, frqhigh=ffk2, prewhiten=0,
        # restrict output
        semb_thres=-1e9, vel_thres=-1e9, timestamp='mlabday',
        stime=stime, etime=etime
    )
    fkdat = array_processing(st, **kwargs)
    #
    print('FK  PAR.: FREQ {},{} WLEN {:5.3f}'.format(ffk1,ffk2,wlen))
    #print('SNR PAR.: LENGTH STA {:5.3f} LTA {:5.3f}'.format(lenSTA,lenLTA))
    #
    # Plot and save figure of FK results
    fpng = pbase + '_fk_f' + sk_FB + '.png'
    save_movfk_plot2(fpng, fkdat, [ffk1,ffk2], stime, conf)
    # create pandas dataframe with fk data
    #col_lbl = ['time', 'rel.power', 'abs.power', 'baz', 'slow']
    #pd_fkdat = pd.DataFrame(data=fkdat, columns=col_lbl)
    # save fkdat to pickle file
    outdatf = pbase + '_fk_f' + sk_FB + '.p'
    with open( outdatf, "wb" ) as f:
        pickle.dump( fkdat, f , pickle.HIGHEST_PROTOCOL)


def nor_sta_lta(a, nsta, ndel, zeta):
    """
    Compute modified recursive STA/LTA by Norsar from input array a.
    
    The length of the STA is given by nsta in samples, the LTA window stops
    at ndel samples before the end of the STA window. The recursive STA and
    LTA are modified by factors using exponent zeta. Equation of this
    delayed recursive Norsar STA/LTA from NMSOP ch. 9.6.

    Fast version written in C.

    :type a: NumPy :class:`~numpy.ndarray`
    :param a: Seismic Trace
    :type nsta: int
    :param nsta: Length of short time average window in samples
    :type ndel: int
    :param ndel: Length of LTA to STA delay in samples
    :type zeta: int
    :param zeta: Exponent to diminish LTA with distance to STA
    :rtype: NumPy :class:`~numpy.ndarray`
    :return: Characteristic function of Norsar STA/LTA
    """
    import numpy as np
    from shiver.c_fct.headers import clibsignal, head_norstalta_t

    data = a
    # initialize C struct / NumPy structured array
    head = np.empty(1, dtype=head_norstalta_t)
    head[:] = (len(data), nsta, ndel, zeta)
    # ensure correct type and contiguous of data
    data = np.ascontiguousarray(data, dtype=np.float64)
    # all memory should be allocated by python
    charfct = np.empty(len(data), dtype=np.float64)
    # run and check the error-code
    errcode = clibsignal.norstalta(head, data, charfct)
    if errcode != 0:
        raise Exception('ERROR %d stalta: len(data) < nlta' % errcode)
    return charfct
