#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 18:45:48 2020

@author: ts
"""


def isempty_dict(dct):
    """
    Tell if dict is empty for all keys.

    Standard Python definition of empty is used, meaning
    None, False, numeric zero (0, 0.0, 0j), and
    empty sequences ('', [], {}, ()) are all objects which evaluate to
    false when tested, and are thus considered empty.

    Parameters
    ----------
    dct : dict
        Dict object, to check if all fields have empty values.

    Returns
    -------
    bool
        True if all dict values are empty, else False.

    """
    if not dct:
        return True
    Ns = len(dct)
    Nempty = 0
    for k, v in dct.items():
        if not v:
            Nempty += 1
    if Nempty == Ns:
        return True
    return False


def get_def(adict, key, default):
    """
    Get dict value or given default if not found or empty.

    Dict.get(key, default) only gives default if key not in dict,
    but this method returns default also if key exists but has empty value.
    """
    if key in adict and adict[key]:
        return adict[key]
    else:
        return default


def parse_yaml_conf(filename):
    """Read the configuration YAML file to get input parameters."""
    import yaml
    with open(filename, 'r') as stream:
        conf = yaml.safe_load(stream)
    return conf


def save_pickle(filename, data):
    """
    Save the "data" to pickle file "filename".

    Parameters
    ----------
    filename : str
        Path of output file.
    data : obj
        Data object to pickle.

    Returns
    -------
    None.
    """
    import pickle
    with open(filename, 'wb') as f:
        pickle.dump(data, f)
    return


def load_pickle(filename):
    """
    Load pickle file and return data object.

    Parameters
    ----------
    filename : str
        Path of input file.

    Returns
    -------
    data : obj
        Data object from file.
    """
    import pickle
    with open(filename, 'rb') as f:
        data = pickle.load(f)
    return data


def save_bz2pickle(filename, data):
    """
    Pickle an object and BZ2-compress it to file.

    Parameters
    ----------
    filename : str
        Path of output file.
    data : obj
        Data object to pickle.

    Returns
    -------
    None.
    """
    import bz2
    import _pickle as cPickle
    with bz2.BZ2File(filename, mode='w', compresslevel=9) as f:
        cPickle.dump(data, f)
    return


def load_bz2pickle(filename):
    """
    Load BZ2-compressed pickle file and return data object.

    Parameters
    ----------
    filename : str
        Path of input file.

    Returns
    -------
    data : obj
        Data object from file.
    """
    import bz2
    import _pickle as cPickle
    with bz2.BZ2File(filename, mode='r') as f:
        data = cPickle.load(f)
    return data


def save_xzpickle(filename, data):
    """
    Pickle an object and LZMA-compress (xz) it to file.

    Parameters
    ----------
    filename : str
        Path of output file.
    data : obj
        Data object to pickle.

    Returns
    -------
    None.
    """
    import lzma
    import _pickle as cPickle
    with lzma.LZMAFile(filename, mode='w') as f:
        cPickle.dump(data, f)
    return


def load_xzpickle(filename):
    """
    Load LZMA-compressed pickle file and return data object.

    Parameters
    ----------
    filename : str
        Path of input file.

    Returns
    -------
    data : obj
        Data object from file.
    """
    import lzma
    import _pickle as cPickle
    with lzma.LZMAFile(filename, mode='r') as f:
        data = cPickle.load(f)
    return data


def read_pd_file(fn, filetype=None):
    """
    Read pandas dataframe saved as pickle or parquet file.

    This method is for convenience to read any of the two formats and any
    compression without having to specify the pandas read method.

    Parameters
    ----------
    fn : string
        Path to file, expected to be only pandas dataframe.
    filetype : string, optional
        Can specify if 'pickle' or 'parquet'. The default is None.

    Returns
    -------
    pandas.DataFrame
        Object which had been stored as file.
    """
    import pandas as pd
    pickle_opt = ['pickle', 'pkl', 'pik']
    parquet_opt = ['parquet', 'prq', 'pqt']
    ext_pkl_comp = ['.gz', '.bz2', '.xz']
    ext_prq_comp = ['.zst', '.gz', '.lz4', '.snp', '.lzo']
    ext_pkl = ['pickle', 'pkl', 'pik']
    ext_prq = ['parquet', 'prq', 'pqt']
    # if filetype specified by user:
    if filetype:
        if filetype.lower() in pickle_opt:
            return pd.read_pickle(fn)
        elif filetype.lower() in parquet_opt:
            return pd.read_parquet(fn)
        else:
            raise RuntimeError(
                "Given filetype '{}' unavailable.".format(filetype))
    fl = fn.lower()
    # if file extension matches common ones for (compressed) pickle/parquet:
    if any([fl.endswith(x) for x in ext_pkl_comp]):
        e2 = fl.split('.')[-2]
        if any([e2 == x for x in ext_pkl]):
            try:
                return pd.read_pickle(fn)
            except Exception as e:
                print('Warning: read as pickle (based on file-ext.) failed.')
                raise e
    if any([fl.endswith(x) for x in ext_prq_comp]):
        e2 = fl.split('.')[-2]
        if any([e2 == x for x in ext_prq]):
            try:
                return pd.read_parquet(fn)
            except Exception as e:
                print('Warning: read as parquet (based on file-ext.) failed.')
                raise e
    if any([fl.endswith(x) for x in ext_pkl]):
        try:
            return pd.read_pickle(fn)
        except Exception as e:
            print('Warning: read as pickle (based on file-ext.) failed.')
            raise e
    if any([fl.endswith(x) for x in ext_prq]):
        try:
            return pd.read_parquet(fn)
        except Exception as e:
            print('Warning: read as parquet (based on file-ext.) failed.')
            raise e
    # if no info given and file ext not typical, try both:
    try:
        return pd.read_pickle(fn)
    except:
        pass
    try:
        return pd.read_parquet(fn)
    except Exception as e:
        print('Warning: read as pickle and parquet failed.')
        raise e
