#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 19:04:07 2020

@author: ts
"""


def get_immediate_subdirectories(topdir):
    """Get list of subdirs, depth=1."""
    from glob import glob
    lst = glob(topdir + '/*/')
    return sorted(lst, key=str.lower)


def list_subdir_selection(topdir, first, last):
    """Get subdirectory n from given directory."""
    subdirs = get_immediate_subdirectories(topdir)
    n = len(subdirs)
    if first is None:
        first = 1
    if last is None:
        last = n
    first -= 1
    return subdirs[first:last]


def get_stream(mdir,comp='Z'):
    """
    read mseed, return obspy stream/traces of given directory and component.
    comp = 'Z', 'E', 'N', 'NE, 'or 'ZNE'
    """
    import obspy
    if comp == 'Z' or comp == 'E' or comp == 'N':
        pattern = mdir + '/*HH' + comp + '*.msd'
    elif comp == 'NE':
        pattern = mdir + '/*HH[NE]*.msd'
    elif comp == 'ZNE':
        pattern = mdir + '/*HH[ZNE]*.msd'
    else:
        print('WARNING: unknown component input: {}'.format(comp))
        return None
    st = obspy.read(pattern)
    return st


def get_station_table(file='../dat/ICE1/ICEARRAY1_stats.tab'):
    """
    read station table file.
    """
    stats={}
    for line in open(file):
        line=line.strip().split()
        stats[line[0]] = [float(line[1]),float(line[2])]
    return stats


def assign_station_coords(stats,traces):
    """
    assign coordinates to loaded obspy 'traces' using station table 'stats'.
    """
    from obspy.core.util import AttribDict
    for tr in traces:
        lat=stats[tr.stats.station][0]
        lon=stats[tr.stats.station][1]
        tr.stats.coordinates = AttribDict({
            'latitude':lat,
            'longitude':lon,
            'elevation':0.})
    return None


def get_onset(f_ons,mdir):
    """
    get relative onset time from file f_ons for event in mdir.
    return onset time and basename of mdir (last subdir name is event ID).
    """
    import os
    from datetime import datetime
    dd=mdir
    if dd.endswith('/'):
        dd=dd[:-1]
    bn=os.path.basename(dd)
    bndt = datetime(int(bn[0:4]),int(bn[4:6]),int(bn[6:8]))
    doy = bndt.timetuple().tm_yday
    refstr = (bn[0:4] + '-' + str(doy) + ':' + bn[8:10] + '.'
              + bn[10:12] + '.' + bn[12:14] + '.' + bn[15:18])
    ons=-9.
    for line in open(f_ons):
        line=line.strip().split()
        if line[0] == refstr:
            ons=float(line[2])
            break
    return ons,bn
