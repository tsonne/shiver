#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 18:37:27 2020

@author: ts
"""
# Seismic sensor names in SIL and their corner frequencies [Hz] (no guarantee)
SENSOR_CORNERS = {
    'GUR3TA': 0.0083,
    'GUR6T': 0.033,
    'GUR6Ta': 0.033,
    'GUR6Tb': 0.033,
    'GUR6Tc': 0.083,
    'GUR6TD': 0.033,
    'GUR40T': 0.033,
    'GURAL': 0.00833,
    'GURALP': 0.00833,
    'GURESP': 0.033,
    'GURESPA': 0.0167,
    'GURESPB': 0.0167,
    'GURESPC': 0.033,
    'GURPAS': 0.00833,
    'GURflut': 0.033,
    'KS2000M': 0.00833,
    'LE1': 1.0,
    'LE20': 0.05,
    'LE5': 0.2,
    'LE5III': 0.2,
    'S13': 1.0,
    'STS2': 0.00833,
    'STS2a': 0.00833,
    'TR240': 0.00417,
    'TRC20': 0.05,
    'TRCAT': 0.00833,
    'TRILL20': 0.05,
    }

def station_from_ringcf(station, file_path):
    """
    Get line of given station from ringstation.cf file.

    Parameters
    ----------
    station : str
        Station code, e.g. 'mjo'.
    file_path : str
        SIL config file ringstation.cf file path.

    Returns
    -------
    L : str
        Entire line with 'streamcode stationcode digitizer sensor'.
    """
    LL = read_from_sil(file_path)
    for L in LL:
        if L.split()[1] == station:
            return L
    return None


def read_from_sil(file_path):
    """
    Get list of strings before #-comments from file.

    Parameters
    ----------
    file_path : str
        Input file path.

    Returns
    -------
    o : list
        List of file lines with comments stripped away.
        Comment lines without preceding values are skipped.
    """
    # issue with at least one file (GUR5T.resp), had non-UTF8 encoding
    try:
        o = []
        with open(file_path, encoding='utf8') as f:
            for L in f:
                L = L.split('#', 1)[0].rstrip()
                if L:
                    o.append(L)
    except UnicodeDecodeError:
        o = []
        with open(file_path, encoding='cp1252') as f:
            for L in f:
                L = L.split('#', 1)[0].rstrip()
                if L:
                    o.append(L)
    return o


def str2complex(s):
    """
    Convert string with two numbers to one complex number.

    Parameters
    ----------
    s : str
        String of two numbers, e.g. '1.23  -0.45'.
        First number = real part,
        Second number = imaginary part.

    Returns
    -------
    complex
        Complex type number.

    """
    x = s.split()
    return complex(float(x[0]), float(x[1]))


def paz_from_sil(par):
    """
    Get dict of gain, poles, zeros from list of response parameter strings.

    Parameters
    ----------
    par : list
        List of parameter lines from SIL-format response file.
        Example input:
            ['3.429046e+12',
             '3',
             '-0.074036  0.074036',
             '-0.074036 -0.074036',
             '-1130.97   0.0',
             '2',
             '0.0 0.0',
             '0.0 0.0']

    Returns
    -------
    dict
        Dict with gain, poles and zeros.
    """
    poles = []
    zeros = []
    n = len(par)
    # awkward gain due to bad format in at least one file
    # (T3755.resp has too many numbers on first line)
    gain = float(par[0].split()[0])
    i = 2  # line 2 = num of poles, poles (always 2 numbers) from line 3 if any
    while i < n and len(par[i].split()) > 1:
        poles.append(str2complex(par[i]))
        i += 1
    i += 1  # same pattern with zeros just below poles
    while i < n and len(par[i].split()) > 1:
        zeros.append(str2complex(par[i]))
        i += 1
    return {'gain': gain, 'poles': poles, 'zeros': zeros}


def sildev2paz(device_name, is_path=False, path_etc='/usr/sil/etc'):
    """
    Get paz dict for SIL sensor or digitizer.

    The poles and zeros (paz) for sensors and digitizers must be read from
    their SIL response files. The SIL response files are expected in a
    directory path_etc. This function allows also just giving a file path.

    Parameters
    ----------
    device_name : str
        Sensor or digitizer name, i.e. response file name without extension.
    is_path : bool
        Whether device_name is a full file path (True) or just the filename
        without directory path and extension (False). Default is False.
    path_etc : str
        Path to sil/etc/ directory which contains the response files.
        Used if just filename given (is_path=False).

    Returns
    -------
    dict
        Dict with gain, poles, zeros. This is used by methods
        obspy.core.trace.Trace.{simulate, remove_response}().
    """
    if is_path:
        fpath = device_name
    else:
        import os
        fpath = os.path.join(path_etc, device_name + '.resp')
    return paz_from_sil(read_from_sil(fpath))


def sil2paz(s_sensor, s_digitizer, path_etc='/usr/sil/etc'):
    """
    Get paz dict for SIL sensor and digitizer.

    The poles and zeros (paz) for sensors and digitizers must be read from
    their SIL response files. The SIL-specific waveform format BC lists
    names of sensor and digitizer in its header. The SIL response files are
    expected in a directory path_etc.

    Parameters
    ----------
    s_sensor : str
        Sensor name, i.e. response file name without extension.
    s_digitizer : str
        Digitizer name, i.e. response file name without extension.
    path_etc : str
        Path to sil/etc/ directory which contains the response files.

    Returns
    -------
    paz : dict
        Dict with gain, poles, zeros, sensitivity. This is used by methods
        obspy.core.trace.Trace.{simulate, remove_response}().
    """
    import os
    fsen = os.path.join(path_etc, s_sensor + '.resp')
    fdig = os.path.join(path_etc, s_digitizer + '.resp')
    paz_sns = paz_from_sil(read_from_sil(fsen))
    paz_dgt = paz_from_sil(read_from_sil(fdig))
    paz = {}
    paz['gain'] = paz_sns['gain']
    paz['sensitivity'] = paz_dgt['gain']
    paz['poles'] = paz_sns['poles']
    paz['zeros'] = paz_sns['zeros']
    if paz_dgt['poles']:
        paz['poles'].extend(paz_dgt['poles'])
    # cut last digitizer zero, as SIL files have one too many,
    # SIL would convert to DISP while this method shall give VEL.
    if len(paz_dgt['zeros']) > 1:
        paz['zeros'].extend(paz_dgt['zeros'][:-1])
    return paz


def remove_resp_sil(st, pre_filt, path_etc):
    """
    Revome instrument response from stream using SIL response files.

    The input stream should be created from BC waveform files with header
    info stored, such that sensor and digitizer names are included.
    Input is assumed raw velocity counts, output will be velocity in m/s.

    Parameters
    ----------
    st : obspy.stream
        Stream of traces, each trace must have stats.bc_header metadata.
    pre_filt : list
        List of 4 corner frequencies of spectral cosine filter.
    path_etc : str
        Path to sil/etc/ directory which contains the response files.

    Returns
    -------
    stcor : obspy.stream
        Output stream with instrument-corrected traces.
    """
    stcor = st.copy()
    for tr in stcor:
        try:
            s_sensor, s_digitizer = tr.stats['bc_header'][0].split()
            paz = sil2paz(s_sensor, s_digitizer, path_etc=path_etc)
            tr.simulate(paz_remove=paz,
                        remove_sensitivity=True, simulate_sensitivity=False,
                        water_level=60.0, zero_mean=True, taper=True,
                        taper_fraction=0.03, pre_filt=pre_filt)
        except ValueError as e:
            print('ValueError: {} {}'.format(e, tr.id))
            stcor.remove(tr)
            print('# Removed trace from further processing.')
    return stcor


def get_station_table_df(path=None):
    """
    Get pandas.DataFrame of all SIL stations, lat, lon, elev.

    Parameters
    ----------
    path : str
        Path to csv file. Expect no header, space separated table of station,
        latitude, longitude, elevation.

    Returns
    -------
    df : pandas.DataFrame
        SIL station table: station code and coordinates.
    """
    import pandas as pd
    if path is None:
        path = ('/home/ts/Documents/IMO/PROJECT/'
                'Seismology/dat/SIL_stations.dat')
    SIL_coln = ['station', 'lat', 'lon', 'elev']
    df = pd.read_csv(path, header=None,
                     delimiter=r'\s+', names=SIL_coln,
                     index_col='station')
    return df


def get_SIL_station_table_df(path_etc=None):
    """
    Get pandas.DataFrame of all SIL stations, lat, lon, elev from net.dat.

    Parameters
    ----------
    path_etc : str
        Path to sil/etc/ directory which contains the net.dat file.

    Returns
    -------
    df : pandas.DataFrame
        SIL station table: station code and coordinates.
    """
    import pandas as pd
    import os
    if path_etc is None:
        path_etc = '/usr/sil/etc'
    path_SIL_STA = os.path.join(path_etc, 'net.dat')
    df = pd.DataFrame(columns=['lat', 'lon', 'elev'])
    df.index.name = 'station'
    with open(path_SIL_STA) as f:
        for line in f:
            x4 = read_netdat_line(line)
            if x4:
                df.loc[x4[0]] = [x4[1], x4[2], x4[3]]
    return df


def read_netdat_line(line):
    """
    Read /sil/etc/net.dat style line and return station, lat, lon, elev.

    Returns empty if commented line or anything that's not full info.

    Parameters
    ----------
    line : str
        one line from a net.dat style file.

    Returns
    -------
    x : list
        4-elements: string(station), float(lat, lon, elev).
    """
    if len(line) < 28:
        x = []
    else:
        x = [line[0:4].strip(), float(line[4:13].strip()),
             float(line[13:22].strip()), float(line[22:28].strip())]
        if x[0][0] == '#':
            x = []
    return x


def get_station_codes(station, use_etc=True, path=None):
    """
    Get specific list of station codes.

    Return either given string or list as list (out=in), or get all station
    codes from a file if input = '_all'.

    Parameters
    ----------
    station : str, list
        DESCRIPTION.
    use_etc : bool
        If True, use sil/etc/net.dat file, else read from csv file.
        Default is True.
    path : str
        Either path of sil/etc directory, or path to csv file.

    Returns
    -------
    station_list : list
        DESCRIPTION.

    """
    if isinstance(station, str):
        station = [station]
    if len(station) == 1 and station[0] == '_all':
        if use_etc:
            df = get_SIL_station_table_df(path_etc=path)
        else:
            df = get_station_table_df(path=path)
        station_list = df.index.values.tolist()
    else:
        station_list = station
    return station_list


def get_bc_filelist(start, end, station,
                    path_BC='/mnt/sil/bc',
                    BC_LEN=120):
    """
    Find the list of BC files matching a given time and station.

    Parameters
    ----------
    start : str
        Start time, format '%Y-%m-%d %H:%M:%S'.
    end : TYPE
        End time, format '%Y-%m-%d %H:%M:%S'.
    station : str, list
        Station code, either one station string, or list of stations.
    path_BC : str, optional
        Path to bc directory. The default is '/mnt/sil/bc'.
    BC_LEN : int, optional
        Length of a standard bc file in seconds. The default is 120.

    Returns
    -------
    bc_dct : dict
        Dict of stations with file lists.

    """
    from gtimes import timefunc
    import pandas as pd
    from datetime import datetime, timedelta
    import os
    import glob

    if isinstance(start, str):
        fmt_dt = '%Y-%m-%d %H:%M:%S'
        dt_start = datetime.strptime(start, fmt_dt)
        dt_end = datetime.strptime(end, fmt_dt)
    else:
        dt_start = start
        dt_end = end
    dt_tmin = dt_start - timedelta(seconds=BC_LEN)

    bc_dct = {}
    if isinstance(station, str):
        station = [station]
    # elif not station:
    #    return bc_dct
    for STA in station:
        fstr1 = "{0}/%Y/#b/%d/{1}".format(os.path.join(path_BC), STA)
        fstr2 = "{0}/%Y/%b/%d/{1}/%H%M%S00.bc".format(
            os.path.join(path_BC), STA)
        lfrequency = '1D'
        bc_dirs = timefunc.datepathlist(
                    fstr1, lfrequency, starttime=dt_tmin.date(),
                    endtime=dt_end.date(), closed=None)
        bc_files = []  # get paths to all bc files in returned dirs
        for dd in bc_dirs:
            bc_files = bc_files + glob.glob("{}/*.bc".format(dd))
        bc_dts = [datetime.strptime(x, fstr2) for x in bc_files]
        bc_df = pd.DataFrame({'file': bc_files}, index=bc_dts)
        bc_df.sort_index(inplace=True)
        bc_dct[STA] = bc_df.loc[dt_tmin:dt_end, 'file'].tolist()
    return bc_dct


def read_bc_by_date(start, end, station,
                    codes={},
                    path_BC='/mnt/sil/bc',
                    BC_LEN=120):
    """
    Get dict of obspy.streams from BC database by given time and stations.

    Returned streams are sliced to given time range. With input dict 'codes',
    the stream metadata for network and/or location can be set to desired
    values, like codes={'location': '00', 'network': 'xx'}.

    Parameters
    ----------
    start : str
        Start time, format '%Y-%m-%d %H:%M:%S'.
    end : TYPE
        End time, format '%Y-%m-%d %H:%M:%S'.
    station : str, list
        Station code, either one station string, or list of stations.
    codes: dict
        dict with keys 'location' and/or 'network'. Will set those ID
        values in the obspy.traces accordingly. Default is empty.
    path_BC : str, optional
        Path to bc directory. The default is '/mnt/sil/bc'.
    BC_LEN : int, optional
        Length of a standard bc file in seconds. The default is 120.

    Returns
    -------
    stream_dct : dict
        Dict of one obspy.stream per station. If no data found for given
        time and station, value of corresponding key will be None.

    """
    from obspy import UTCDateTime

    t1 = UTCDateTime(start)
    t2 = UTCDateTime(end)
    bc_dct = get_bc_filelist(start, end, station, path_BC, BC_LEN)
    if not bc_dct:
        return None
    stream_dct = {}
    for k, v in bc_dct.items():
        stream_dct[k] = get_bc_stream(v, codes)
        if stream_dct[k]:
            stream_dct[k] = stream_dct[k].slice(t1, t2)
    return stream_dct


def read_bc_by_time(start, end, station,
                    codes={},
                    path_BC='/mnt/sil/bc',
                    BC_LEN=120):
    """
    Get one obspy.stream from BC database by given time and stations.

    Returned stream traces are sliced to given time range. With input dict
    'codes', the trace metadata for network and/or location can be set to
    desired values, like codes={'location': '00', 'network': 'xx'}.

    Parameters
    ----------
    start : str
        Start time, format '%Y-%m-%d %H:%M:%S'.
    end : TYPE
        End time, format '%Y-%m-%d %H:%M:%S'.
    station : str, list
        Station code, either one station string, or list of stations.
    codes: dict
        dict with keys 'location' and/or 'network'. Will set those ID
        values in the obspy.traces accordingly. Default is empty.
    path_BC : str, optional
        Path to bc directory. The default is '/mnt/sil/bc'.
    BC_LEN : int, optional
        Length of a standard bc file in seconds. The default is 120.

    Returns
    -------
    st : obspy.stream
        obspy.stream combining all components of all stations. If no data found
        for given time and station, return empty stream.
    """
    from obspy import Stream, UTCDateTime

    t1 = UTCDateTime(start)
    t2 = UTCDateTime(end)
    bc_dct = get_bc_filelist(start, end, station, path_BC, BC_LEN)
    if not bc_dct:
        return None
    st = Stream()
    for k, v in bc_dct.items():
        x = get_bc_stream(v, codes)
        if x:
            st = st + x.slice(t1, t2)
    if len(st) == 0:
        print('WARNING:read_bc_by_time: No data found.')
    return st


def get_bc_stream(files, codes={}):
    """
    Read BC data file(s) and return obspy Stream object from all components.

    Will combine all given files to one stream and merge the traces.
    This should give continuous long traces for each station's component.
    Three components in every BC file are expected (e, n, z).
    Merge by method=-1, such that gaps result in multiple traces, not
    masked arrays.

    Parameters
    ----------
    files : str, list
        one path to BC file, or list of paths for multiple files
        to be combined to one stream.
    codes: dict
        dict with keys 'location' and/or 'network'. Will set those ID
        values in the obspy.traces accordingly. Default is empty.

    Returns
    -------
    obspy.Stream
        Obspy stream object created from BC file.
    Or None if input 'files' is empty.

    The BC file header is stored in trace.stats.bc_header as list,
    see the origianl description of the BC header here:
    D['header']    - list of ascii header lines, may be empty
    D['header'][i] - ascii header line 1 as a string, typically there
                     are 3 header lines with the content:
                         ['SensorID DigitizerID',
                         'Creator_of_file',
                         'timing status']

    """
    from BcPy import BcRead
    from obspy import Stream

    if not files:
        return None
    if isinstance(files, str):
        files = [files]
    S = Stream()
    for f in files:
        D = BcRead(f, mode=1, time='datetime')
        for k, v in {'e': 'HHE', 'n': 'HHN', 'z': 'HHZ'}.items():
            S += build_trace(
                D['data'][k], D['start'], D['sps'], D['station'], v,
                header={'bc_header': D['header']}, **codes)
    S.merge(-1)
    return S


def save_bc_headers(st, outfile='bc_headers.csv'):
    """
    Write BC headers of traces in stream to text file.

    Parameters
    ----------
    st : obspy.Stream
        Stream with traces which have entry 'bc_header' in stats.
    outfile : str
        Output file path.

    Returns
    -------
    None.
    """
    with open(outfile, 'w') as f:
        for tr in st:
            f.write(tr.id + ',' + ','.join(tr.stats.bc_header) + '\n')
    return


def read_mseed_bc(start=None, end=None,
                  stations=None, pattern='*',
                  bc_headers_file='bc_headers.csv',
                  bc_headers_in_mseed_dir=True):
    """
    Read mseed files and bc header csv, attach headers to traces.

    Parameters
    ----------
    start : datetime or str, optional
        Start time of stream. The default is None.
    end : datetime or str, optional
        End time of stream. The default is None.
    stations : list of str, optional
        List of wanted stations. Get all if None. The default is None.
    pattern : str, optional
        Path pattern for mseed files. The default is '*'.
    bc_headers_file : str, optional
        File basename of bc header list to read. Can be absolute path to other
        location than mseed dir if bc_headers_in_mseed_dir=False.
        The default is 'bc_headers.csv'.
    bc_headers_in_mseed_dir : bool, optional
        Whether bc header file is in mseed dir. The default is True.

    Returns
    -------
    st : obspy.Stream
        Stream with attached bc header lines for each trace. Should be as if
        BC files used instead of mseed. If nothing found return empty stream.
    """
    from os import path
    import pandas as pd
    from obspy import read, UTCDateTime
    # read mseed files
    st = read(pattern)
    if len(st) == 0:
        return st
    # filter by station
    if stations:
        xid=[]
        for i in range(len(st)):
            if st[i].id.split('.')[1] in stations:
                xid.append(i)
        if len(xid) == 0:
            st.clear()
            return st
        if len(xid) < len(st):
            st.traces = [st.traces[i] for i in xid]
    # trim by start/end
    if start is not None or end is not None:
        if start is not None:
            start = UTCDateTime(start)
        if end is not None:
            end = UTCDateTime(end)
        st.trim(starttime=start, endtime=end)
    # set bc header list file path
    if bc_headers_in_mseed_dir:
        bc_headers_file = path.join(path.dirname(pattern), bc_headers_file)
    # read bc headers, assign to traces stats dict
    df = pd.read_csv(bc_headers_file, header=None)
    for tr in st:
        tr.stats.bc_header = df[df[0]==tr.id].iloc[0].tolist()[1:]
    return st


def get_bc_headers(files):
    """
    Get list of BC file headers for given list of files.

    Parameters
    ----------
    files : str, list
        one path to BC file, or list of paths for multiple files.

    Returns
    -------
    headers : list
        List of header objects from given BC files.
    """
    from BcPy import BcRead
    if not files:
        return None
    if isinstance(files, str):
        files = [files]
    headers = []
    for f in files:
        D = BcRead(f, mode=0, time='datetime')
        headers.append(D['header'])
    return headers


def build_trace(data, start, sps, sta, chn,
                network="", location="", header=None):
    """
    Create obspy Trace object from input.

    Trace header attributes auto-calculated in obspy:
        delta, endtime, npts

    Parameters
    ----------
    data : object
        array of data samples.
    start : int, float, str, datetime.datetime
        time of first data sample, can be float timestamp
        (unix time), datetime.datetime object, or ISO8601
        format string, e.g. '2011-01-28T22:45:12.345'.
    sps : float, int
        sampling rate (Hz).
    sta : str
        station code.
    chn : str
        channel code.
    network: str
        network code, default is empty.
    location: str
        location code, default is empty.
    header : dict
        dict containing extra trace header values to be included.

    Returns
    -------
    obspy.Trace
        Obspy trace object created from input.

    """
    from obspy import Trace, UTCDateTime
    import numpy as np

    h1 = {"starttime": UTCDateTime(start),
          "sampling_rate": sps,
          "network": network,
          "location": location,
          "station": sta,
          "channel": chn}
    h2 = header if header else {}
    hh = {**h2, **h1}
    return Trace(data=np.array(data, dtype=np.float64), header=hh)


def silcat_by_conf(CAT):
    """
    Get SIL catalog selection using conf dict input.

    This method uses only certain dict keys, such that the input dict can
    still contain other things which will be ignored by read_sil_data().

    Parameters
    ----------
    CAT : dict
        Configuration file derived dict which has keys:
            ['start','end','lat_S','lat_N','lon_W','lon_E',
             'depth_min','depth_max','mlw_min','mlw_max',
             'SIL_CAT_top','SIL_CAT_file','fread','rcolumns','cum_moment']

    Returns
    -------
    pandas.DataFrame
        Dataframe representation of SIL catalog. Index is event origin
        datetime, columns are by default "latitude", "Dlatitude", "longitude",
        "Dlongitude", "depth", "Ddepth", "MLW", "seismic_moment", but can be
        different based on INPUT fread and rcolumns.
    """
    from geo_dataread.sil_read import read_sil_data
    from shiver.u_io import get_def
    kw = {}
    kw['start'] = get_def(CAT, 'start', "1990-01-01")
    kw['end'] = get_def(CAT, 'end', None)
    kw['fname'] = get_def(CAT, 'SIL_CAT_file', None)
    if kw['fname']:
        kw['frfile'] = True
    else:
        kw['frfile'] = False
        kw['base_path'] = get_def(CAT, 'SIL_CAT_top', '/mnt/sk2')
    kw['fread'] = get_def(CAT, 'fread', 'events.fps')
    kw['rcolumns'] = get_def(CAT, 'rcolumns', 'all')
    kw['cum_moment'] = get_def(CAT, 'cum_moment', False)
    kw['min_lat'] = get_def(CAT, 'lat_S', None)
    kw['max_lat'] = get_def(CAT, 'lat_N', None)
    kw['min_lon'] = get_def(CAT, 'lon_W', None)
    kw['max_lon'] = get_def(CAT, 'lon_E', None)
    kw['min_depth'] = get_def(CAT, 'depth_min', None)
    kw['max_depth'] = get_def(CAT, 'depth_max', None)
    kw['min_mlw'] = get_def(CAT, 'mlw_min', None)
    kw['max_mlw'] = get_def(CAT, 'mlw_max', None)
    kw['logging_level'] = get_def(CAT, 'logging_level', None)
    return read_sil_data(**kw)


def print_some_info_SILCAT(df, verbose=False):
    """
    Print info (number events, biggest event...) from SIL events dataframe.

    Parameters
    ----------
    df : pandas.DataFrame
        Dataframe returned from geo_dataread.sil_read.read_sil_data().
    verbose: bool
        Write more info if True, else give concise one-liner. Default is False.

    Returns
    -------
    None.
    """
    if df.empty:
        print("Empty DataFrame: {}".format(df))
        return
    if verbose:
        print("Have {} earthquakes and {} columns".format(*df.shape))
        print("First entry: \n{}".format(df.index[0]))
        print("Last entry: \n{}".format(df.index[-1]))
        if 'num_of_polarities' in df:
            print("Largest magnitude event:")
            kw = ['MLW', 'depth', 'num_of_polarities', 'num_of_amplitudes']
            print("Mlw: {}, depth: {:4.1f}, Npol: {}, Namp: {}".format(
                  *df.loc[df['MLW'].idxmax(), kw].tolist()))
            print("Largest magnitude event with more than 2 polarities:")
            X1 = df.query('num_of_polarities > 2')
            if len(X1) > 0:
                print("Mlw: {}, depth: {:4.1f}, Npol: {}, Namp: {}".format(
                      *X1.loc[X1['MLW'].idxmax(), kw].tolist()))
        else:
            print("Largest magnitude event:")
            kw = ['MLW', 'depth', 'latitude', 'longitude']
            print("Mlw: {}, depth: {:4.1f}, lat: {:8.5f}, lon: {:8.5f}".format(
                  *df.loc[df['MLW'].idxmax(), kw].tolist()))
    else:
        print("{} EQ, {} col, [{} : {}]".format(
            *df.shape, df.index[0], df.index[-1]))
    return


def update_silcat_df(CAT):
    """
    Read SIL catalog from file, add new SIL events to dataframe.

    This method can be used to load a pickle/parquet pandas dataframe of SIL
    events and then read newer events from the SIL directories, combine the
    two and return the updated dataframe. It could then be saved to a new file.

    Parameters
    ----------
    CAT : dict
        SIL catalog and file parameters (examples in parentheses):
            CAT = {'SIL_CAT_file': old_file_path('cat/ev_20200529.prq.zst'),
                   'SIL_CAT_top': path_to_SIL_dirs('/sk1'),
                   'fread': SIL_event_file_type('events.fps'),
                   'rcolumns': used_columns('default' or 'all'),
                   'start': None(can be used to limit old file data),
                   'end': None(can be used to limit old file data)}

    Returns
    -------
    Z : pandas.DatFrame
        Combined dataframe of saved old and new SIL event data.
    """
    from geo_dataread.sil_read import read_sil_data
    from datetime import datetime
    import pandas as pd
    from shiver.u_io import get_def

    # load stored dataframe file
    kw = {}
    kw['start'] = get_def(CAT, 'start', "1990-01-01")
    kw['end'] = get_def(CAT, 'end', None)
    kw['fname'] = get_def(CAT, 'SIL_CAT_file', None)
    if not kw['fname']:
        raise RuntimeError("Need filepath to update file.")
    kw['frfile'] = True
    kw['fread'] = get_def(CAT, 'fread', 'events.fps')
    kw['rcolumns'] = get_def(CAT, 'rcolumns', 'all')
    kw['cum_moment'] = get_def(CAT, 'cum_moment', False)
    X = read_sil_data(**kw)
    print("Loaded event catalog file:")
    print_some_info_SILCAT(X)

    # load rest until now from SIL directory structure
    s_lastday = X.index[-1].date().strftime('%Y-%m-%d 00:00:00')
    kw['start'] = s_lastday
    kw['end'] = None
    kw['frfile'] = False
    kw['base_path'] = get_def(CAT, 'SIL_CAT_top', '/mnt/sk2')
    kw['fname'] = None
    Y = read_sil_data(**kw)
    print("New events from SIL catalog:")
    print_some_info_SILCAT(Y)

    # merge (cut last day of file and append new SIL events)
    t_lastday = datetime.strptime(s_lastday, '%Y-%m-%d %H:%M:%S')
    b_file = X.index < t_lastday
    Z = pd.concat([X[b_file], Y])
    print("Updated event catalog dataframe:")
    print_some_info_SILCAT(Z)
    return Z


def write_silcat_file(df, OUT):
    """
    Write new SIL catalog dataframe file.

    Currently always assuming file format = parquet.

    Parameters
    ----------
    df : pandas.DataFrame
        SIL event catalog as pandas dataframe.
    OUT : dict
        Dict with output file info:
            OUT = {'IO_dir': output_dir_path('/my_data/sil_cat'),
                   'ext': file_extension('.prq.zst'),
                   'cmpr': compression('zstd')}
        Filename will be automatic reflecting start and end date, or can be set
        by optional key OUT['basename'].

    Returns
    -------
    str
        Full path to saved file.
    """
    import os
    if 'basename' in OUT and OUT['basename']:
        out_file = os.path.join(OUT['IO_dir'], OUT['basename'] + OUT['ext'])
    else:
        st1 = df.index[0].date().strftime('%Y%m%d')
        st2 = df.index[-1].date().strftime('%Y%m%d')
        out_file = os.path.join(
            OUT['IO_dir'], 'events_' + st1 + '_' + st2 + OUT['ext'])
    df.to_parquet(out_file, compression=OUT['cmpr'])
    print("# Wrote new SIL catalog dataframe file:")
    print("# {}".format(out_file))
    return out_file


def update_silcat_file(CAT, OUT):
    """
    Run SIL catalog dataframe update and write new file.

    Parameters
    ----------
    CAT : dict
        See definition in update_silcat_df(CAT).
    OUT : dict
        See definition in write_silcat_file().

    Returns
    -------
    X : pandas.DatFrame
        Combined dataframe of saved old and new SIL event data.
    """
    # update to new dataframe
    X = update_silcat_df(CAT)
    # save it to file, name includes start and end dates
    write_silcat_file(X, OUT)
    return X


def update_default_silcat_file():
    """
    Update and overwrite the default SIL event catalog dataframe file.

    Returns
    -------
    X : pandas.DatFrame
        Combined dataframe of saved old and new SIL event data.
    """
    # old catalog file parameters
    CAT = {'SIL_CAT_file': ('../dat/sil/catalog/sil_events.prq.zst'),
           'SIL_CAT_top': '/mnt/sk2',  # top dir to real IMO SIL catalog
           'start': None,  # use first event of catalog file
           'end': None,  # get last event of SIL catalog
           'fread': 'events.fps',
           'rcolumns': 'all'}
    # new file parameters:
    OUT = {'IO_dir': '../dat/sil/catalog',
           'basename': 'sil_events',
           'ext': '.prq.zst',
           'cmpr': 'zstd'}
    # update to new dataframe
    return update_silcat_file(CAT, OUT)
