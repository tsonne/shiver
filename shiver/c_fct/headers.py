# -*- coding: utf-8 -*-
"""
Defines the libsignal and evalresp structures and blockettes.
"""
from __future__ import (absolute_import, division, print_function,
                        unicode_literals)
from future.builtins import *  # NOQA
from future.utils import native_str

import ctypes as C  # NOQA

import numpy as np

from shiver.util.libnames import _load_cdll


# Import shared libsignal
clibsignal = _load_cdll("c_fct")

"""
Delayed recursive Norsar STA/LTA, NMSOP ch. 9.6.

Run as:
    from shiver.c_fct.headers import clibsignal, head_norstalta_t  # get this
    head = np.empty(1, dtype=head_norstalta_t)  # init C struct
    head[:] = (len(data), nsta, ndel, zeta)
    data = np.ascontiguousarray(data, dtype=np.float64)  # contiguous mem
    charfct = np.empty(len(data), dtype=np.float64)  # mem alloc
    errcode = clibsignal.norstalta(head, data, charfct)  # run

Used in shiver.u_array.norstalta(), see there for more info.
"""
head_norstalta_t = np.dtype([
    (native_str('N'), np.uint32),
    (native_str('Nsta'), np.uint32),
    (native_str('Ndel'), np.uint32),
    (native_str('Zeta'), np.uint32),
], align=True)

clibsignal.norstalta.argtypes = [
    np.ctypeslib.ndpointer(dtype=head_norstalta_t, ndim=1,
                           flags=native_str('C_CONTIGUOUS')),
    np.ctypeslib.ndpointer(dtype=np.float64, ndim=1,
                           flags=native_str('C_CONTIGUOUS')),
    np.ctypeslib.ndpointer(dtype=np.float64, ndim=1,
                           flags=native_str('C_CONTIGUOUS')),
]
clibsignal.norstalta.restype = C.c_int
