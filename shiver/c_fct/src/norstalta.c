/*--------------------------------------------------------------------
# Filename: norstalta.c
#  Purpose: Delayed recursive Norsar STA/LTA, NMSOP ch. 9.6
#   Author: Tim Sonnemann
# Copyright (C) 2020 tim@vedur.is
#
# Concept:
#   w(t) = seismic trace
#   STA(t) = 1/Nsta * sum(i=0;i<Nsta;abs(w(t-i)))
#   C = pow(2., -Zeta);
#   LTA(t) = C * STA(t-Ndel) + (1-C) * LTA(t-1)
#   SNR(t) = STA(t)/LTA(t)
#
#
# |           |........Ndel.........|
# |-----------|----------|----------|---------------> t
# | LTA(t)    |          |   S(t)   |
# | S(t-Ndel) |
#
#---------------------------------------------------------------------*/
#include <math.h>

typedef struct _headS {
    int N;    // Number of samples in given data array
    int Nsta; // Number of samples in STA window
    int Ndel; // LTA delayed to STA by this many samples
    int Zeta; // LTA update recursive diminution parameter (5 or 6 is fine)
} headS;

int norstalta(const headS *head, const double *data, double *charfct)
{
    int i;
    const double csta = 1./((double)head->Nsta);
    const int Ndel = head->Ndel;
    const double A = pow(2., -head->Zeta);
    const double B = 1. - A;
    double sta = 0.; // current STA(t)
    double dsta = 0.;  // delayed STA(t-Ndel)
    double lta = 0.;
    
    if ((head->N < Ndel + head->Nsta + 1) || (Ndel < 0) || (A > 1.)) {
        return 1;
    }
    // first LTA (delayed STA)
    for (i = 0; i < head->Nsta; ++i) {
        dsta += fabs(data[i]);
    }
    dsta *= csta;
    lta = dsta;
    // first STA
    for (i = Ndel; i < Ndel + head->Nsta; ++i) {
        sta += fabs(data[i]);
    }
    sta *= csta;
    // first characteristic function values
    for (i = 0; i < Ndel + head->Nsta; ++i) {
        charfct[i] = 0.;
    }
    charfct[Ndel + head->Nsta] = sta / lta;
    // recursive part
    for (i = Ndel + head->Nsta; i < head->N; i++) {
        sta += (fabs(data[i]) - fabs(data[i - head->Nsta])) * csta;
        dsta += (fabs(data[i - Ndel]) - fabs(data[i - head->Nsta - Ndel])) * csta;
        lta = A * dsta + B * lta;
        charfct[i] = sta / lta;
    }

    return 0;
}
