# ObsPy Course 1
* Date: 2020-10-07
* Speaker: Tim Sonnemann
* Purpose: Explain basic installation and introduce ObsPy

## Motivation
* Want to manage, analyze and plot seismic data (waveforms, events, station meta)
* Want to use IMO data specifically (SIL system)
* Want to have fast and ready tools for it (always available, free, open source)
* Want to quickly develop more specific applications (updates, good docs)
* Conclusion: Get Python, ObsPy, Pyrocko, shiver, make your own from there

## Quick Installation Tutorial...?
* If you're at IMO and know Python and Conda, concise instructions are here:
    * https://git.vedur.is/tim/shiver#installation-with-conda

## Requirements:
* Requirements are specific to this course, assuming IMO applications
* Operating systems: Linux or MacOS
    * if Microsoft Windows: use VirtualBox to create a Debian/Ubuntu system
* Space: > 2G
* Anaconda (Conda) includes all further requirements
    * top-level packages such as `git python gcc`
    * Python packages such as `numpy obspy pyrocko vtk`
* No admin permissions needed for Conda installation
    * (this might be relevant to some people)
* Access to IMO seismic data for practical work
    * Read-access is enough (consider it if you do the mounting)
    * need `viti:/sil/bc/` for BC waveform data
    * need `granit:/sk2/` for SIL event files
    * need `/net/granit.vedur.is/home/sil/etc/` for SIL station metadata

## Installation
* Can be done on your personal computer or IMO computer
* Only need data access from it

### Python by Conda
* Alternative: install Python directly, but then have no flexibility
* Two basic installation types exist: Anaconda and Miniconda
    * Anaconda is the full bag, 1500 packages at 3 GB
        * Pro: All inclusive from start
        * Con: Takes at least a few minutes to get/install and lots of space
    * Miniconda is a fast, lightweight installation
        * Pro: Get fast access to Python and conda, not much space needed
        * Con: Have to install any wanted Python packages later
* Get Miniconda:
    * Main Download page: https://docs.conda.io/en/latest/miniconda.html
    * Download for your OS
        * Linux: https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
        * `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`
        * Mac: https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
        * `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh`

* Install Miniconda:
	* Linux:
		* `bash Miniconda3-latest-Linux-x86_64.sh`
	* Mac:
		* `bash Miniconda3-latest-MacOSX-x86_64.sh`

### Setup Conda Environment
* Create a Python environment and install packages
	* environment given name `seis` here
```sh
conda config --add channels conda-forge
conda create -n seis python=3.7
conda activate seis
conda install spyder obspy pandas fastparquet zstandard
conda install conda-build pillow geopy
```

### Get Git Repos
* A few git projects are needed to handle SIL data
	* put them into `git` directory and install them
```sh
# prepare directory for git repos
mkdir git
cd git
git init
# gpslibrary modules
git clone https://gitlab.com/gpskings/gpslibrary.git
conda develop gpslibrary/gtimes
conda develop gpslibrary/geo_dataread
# BcPy module
git clone https://gitlab.com/tsonne/bcpy.git
cd bcpy
python setup.py install
cd ..
# shiver repo
git clone https://gitlab.com/tsonne/shiver.git
conda develop shiver
```


## Use Python and ObsPy

### Need IDE
* Let's create a Python script
* We need a text editor, any will do, but an integrated development environment (IDE) like `spyder` has certain advantages
```sh
cd shiver/scripts
spyder
```

* The Spyder documentation website has a little tour and links to videos showing what features can be used and how
	* https://docs.spyder-ide.org/current/index.html

### Official Tutorial
* Go through the official ObsPy tutorial pages: https://docs.obspy.org/tutorial/index.html
 
### Use SIL waveform data
* See also script [tests_some_bc.py](https://gitlab.com/tsonne/shiver/-/blob/master/scripts/tests_some_bc.py)
* Get SIL BC waveform data
	* plot them
	* remove instrument response
	* bandpass filter
	* cut to time window
	* plot Fourier spectra

```python
# if interactive zoomable plots wanted, use magic command of IPython kernel:
%matplotlib auto
# magic commands start with '%'.
# to show plots inline or in the Plots Pane (default), use this:
#%matplotlib inline

# import packages, modules, methods
from obspy import Stream, UTCDateTime
from shiver import u_obs
from shiver import u_SIL

# 
start = '2020-07-19 23:35:42'
end = '2020-07-19 23:45:42'
stations = ['kri']  #, 'lfe', 'vog']
code_dct = {'network': 'VI'}
path_BC = '/mnt/sil/bc'

# get the BC data:
st0 = u_SIL.read_bc_by_time(start, end, stations,
                            codes=code_dct, path_BC=path_BC)
st = st0.copy()

# show me
st.plot()

# remove response
pre_filt = [0.1, 1.0, 35., 45.]
etc_dir = 'dat/sil/etc/'
stcor = u_SIL.remove_resp_sil(st, pre_filt, etc_dir)
stcor.plot()

# filter
stcor.taper(0.01)
stcor.filter('bandpass', freqmin=0.4, freqmax=15.)
stcor.plot()

# cut stream to 30 sec before P-arrival, 2 min after P, P at 23:36:15
s_tP = '20200719233615'
t_pre = 5.
t_post = 35.
u_tP = UTCDateTime(s_tP)
st_cut = st.slice(starttime=u_tP-t_pre, endtime=u_tP+t_post)

# plot spectra of cut stream
u_obs.plot_fft(st_cut)
```

## Future Topics
* GUI for waveform data with zoom, filter, spectra and more (Pyrocko Snuffler)
* GUI for event catalogs in 3D and make movies (Pyrocko Sparrow)
* Convert SIL events to QuakeML (Silcomp)
* Use SeisComP?
* ???
