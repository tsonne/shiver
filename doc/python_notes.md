# Python Installation and Config
- Notes about installing and using Python
- OS: MacOS

## Installation
- Anaconda includes most packages already, has GUI, quite big
- Miniconda is very small, no GUI, can install whatever is needed afterwards

### Install Anaconda
- graphical installer of full Anaconda:
    - https://docs.anaconda.com/anaconda/install/mac-os/
    - (has instructions)
- download either graphical or CLI installer for MacOS:
    - https://www.anaconda.com/products/individual#macos
    - (see last third of that page)
- default install dir:
    - `~/opt/anaconda<2 or 3>` (GUI installer)
    - `/home/<user>/anaconda<2 or 3>` (CLI installer)
    - (check if already installed here)

### Install Miniconda
- https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html
    - (has instructions)
- https://docs.conda.io/en/latest/miniconda.html
    - (lists downloads of miniconda)
    - https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
        - (for CLI installation)
    - https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.pkg
        - (Mac pkg for GUI installation)

- commands:
```sh
cd Downloads
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
bash Miniconda3-latest-MacOSX-x86_64.sh
# follow instructions...
```

### Initialize Conda in Terminal if not done before
- terminal should get new `base` environment
- if not, then it's not setup up yet
    - normally happens at end of installation
    - need to close/open terminal after first time, or source rc file
    - `bash`: `/home/$USER/.bashrc`
    - `zsh`: `/home/$USER/.zshrc`
- if not happened yet, can do manually:

```sh
source <path to conda>/bin/activate
conda init
# if using zsh instead of bash, do:
# conda init zsh
source $HOME/.bashrc
# if using zsh instead of bash, do:
# source $HOME/.zshrc
```

- to continue working in same terminal with new environment, source rc file:

```sh
source $HOME/.bashrc
# if using zsh instead of bash, do:
# source $HOME/.zshrc
```

## Setup Conda Environment
- Create a new Python environment and install packages
- environment given name `myfirst` here

```sh
conda create -n myfirst python=3.8
conda activate myfirst
```

## Default package channels
- typically, latest Python package versions are in `conda-forge`
    - 
- to make it the default package source, do:
    - `conda config --add channels conda-forge`

## Install Python packages in conda environment
- activate environment first if not done already
- install package with: `conda install <package-name> [more packages...]`
- example:

```sh
conda activate myfirst
conda install pandas
conda install spyder
conda install conda-build pillow geopy
```

## Install Python packages without conda environment
- not recommended when using conda
    - might mess up environment and its dependencies
- default way to install packages with bare Python

```sh
pip install <package-name>
```

## Install Python package from different channel
- Why? Some special projects have their own source channel
    - e.g. `pyrocko` (seismology package from GFZ Potsdam)
    - are often very specific communities, not basic/general packages
- install package from specified channel:

```sh
conda install -c <channel-name> <package-name>
```

- example:
    - `conda install -c pyrocko pyrocko`

### Install local Python package, e.g. from Git repository
- Why? Special work groups' packages may only be on GitLab
    - also might want to create your own package
- TLDR: all of these can do it if have package dir:
    - `conda develop <package-dir>`
        - requires conda-build, get it: `conda install conda-build`
    - `cd <package-dir> && python setup.py install`
    - `pip install -e <package-dir>`

- Get the **git** repo
	- `git init`
	- `git clone <git-repo-address.git>`
- Install local Python packages, e.g. from a **git** repo
	- one requirement for `conda` to use `develop` command
		- `conda install conda-build`
	- install local Python package with `conda`
		- `conda develop <package-directory-path>`
	- However, if non-Python code must be built (e.g. C, Fortran)
		- (must be in project directory with `setup.py` file)
		- `python setup.py install`
- Update **git** repo
	- (must be in corresponding repository directory)
	- `git pull`
	- (might want to reinstall package then to update built files)

## Update packages and conda itself
- update conda itself:
    - `conda update -n base -c defaults conda`
- update an installed Python package in current environment:
    - `conda update <package-name>`
- update all installed packages in current environment
    - `conda update --all -c conda-forge`


# Working with Python Code and Scripts

## Python Terminal or Environment
- first most basic way to open and use Python in terminal:
    - `python`
    - starts Python terminal session
    - exit by command `quit()` or type CTRL-D
- next best way is IPython, an interactive version for terminal:
    - `ipython`
    - usually included already with conda
    - can have interactive plot windows, integrated with `matplotlib`
- other Python environments and advanced editors often include IPython

## Editors
- Could use basic text editor, but not very nice for developing code
- Full IDEs (integrated development environments) exist for Python
    - can be similar to Matlab or Mathematica
- These are well-used:
    - Spyder
        - has IPython, variable overview, debugger, profiler and lots more
        - similar look and functionality to Matlab editor
    - PyCharm
        - free for non-commercial use, otherwise commercial
        - similar to Spyder, many advanced dev tools
    - Jupyter Notebooks
        - browser-based mix of code/markdown editor and output presentation
        - also used for other programming languages (R, Julia, etc.)
        - useful interface for cloud computing apps, demo scripts
    - Visual Studio Code
        - like most others (autocomplete, debugger,...)
        - also can edit and run Jupyter notebooks









