# ObsPy Course 2
* Date: 2020-10-14
* Speaker: Tim Sonnemann
* Purpose: Scripts to process signals by station and event

## Install and Update Software

### From Anaconda channels
* Install Python packages in **Anaconda**
	* first time, add `conda-forge` channel as default source
		* `conda config --add channels conda-forge`
	* install package
		* `conda install <package>`
* Updating Python packages in **Anaconda**
	* update conda itself
		* `conda update -n base -c defaults conda`
	* update all installed packages in current environment
		* `conda update --all -c conda-forge`

### From Git repositories
* Get the **git** repo
	* `git init`
	* `git clone <git-repo-address.git>`
* Install local Python packages, e.g. from a **git** repo
	* one requirement for `conda` to use `develop` command
		* `conda install conda-build`
	* install local Python package with `conda`
		* `conda develop <package-directory-path>`
	* However, if non-Python code must be built (e.g. C, Fortran)
		* (must be in project directory with `setup.py` file)
		* `python setup.py install`
* Update **git** repo
	* (must be in corresponding repository directory)
	* `git pull`

## Mount IMO machine locally
* This is only useful when using non-IMO computer, which does not have desired IMO machines (`granit`, `viti`, ...) mounted.
* One alternative presented here:
	* Do not edit `/etc/fstab`
	* Mount manually, entering password each time
		* could also create ssh key to avoid entering passwords
	* Require Linux package `sshfs` (Debian/Ubuntu)
		* `sudo apt-get install sshfs`
	* Recommended: mount as read-only to avoid trouble
		* `sshfs -o ro <user>@<remote-host>[:<dir>] <mountpoint>`
	* Unmounting either by computer logout/shutdown, or
		* `fusermount -u <mountpoint>`
	* Example script for such a setup can be found here:
		* [mount_vi](https://gitlab.com/tsonne/shiver/-/blob/master/doc/mount_vi)

## Test Dataset
* There are some BC waveform files in the expected structure here:
	* `dat/sil/bc/2020/jun/21/`
		* 30 stations, one hour

## Scripts

### Get SIL Event Catalog
* Read entire SIL catalog (all event metadata) from `granit`
	* [tests_get_entire_catalog.py](https://gitlab.com/tsonne/shiver/-/blob/master/scripts/tests_get_entire_catalog.py)
	* WARNING: This will probably take about 20 - 30 min!

* Read a saved SIL catalog dataframe file
	* example file: `dat/sil/catalog/sil_events.prq.zst`
	* file format: zstandard-compressed parquet container of `pandas.DataFrame`
	* simple way to just read it:
		* `import pandas as pd`
		* `fn = 'dat/sil/catalog/sil_events.prq.zst'`
		* `df = pd.read_parquet(fn)`
	* more complex way of reading and filtering it:
		* [tests_read_saved_catalog.py](https://gitlab.com/tsonne/shiver/-/blob/master/scripts/tests_read_saved_catalog.py)

### Convert BC waveform files to MSEED
* Let's use the example dataset in `dat/sil/bc`
```python
import os
from shiver import u_SIL
from datetime import datetime
from shiver import u_obs

start = '2020-06-21 19:05:52'
end = '2020-06-21 19:25:52'
stations = ['ada' , 'bjk', 'mjo']
code_dct = {'network': 'VI'}
path_BC = '../dat/sil/bc'

# get the BC data:
st = u_SIL.read_bc_by_time(start, end, stations,
                           codes=code_dct, path_BC=path_BC)

# make output directory for mseed files
dt_start = datetime.strptime(start, '%Y-%m-%d %H:%M:%S')
s_start = dt_start.strftime('%Y%m%d%H%M%S')
mseed_dir = '../dat/sil/mseed/' + s_start
os.makedirs(mseed_dir, exist_ok=True)

# save stream as mseed, also save BC headers as text file
u_obs.save_mseed(mseed_dir, st, lbl="", force=True, encoding='STEIM2')
u_SIL.save_bc_headers(st, mseed_dir + '/bc_headers.csv')
```

### Process waveforms by event
* Several examples in `scripts`:
	* [msme.py](https://gitlab.com/tsonne/shiver/-/blob/master/scripts/msme.py)
	* [adv_msme.py](https://gitlab.com/tsonne/shiver/-/blob/master/scripts/adv_msme.py)
	* [msme_snr.py](https://gitlab.com/tsonne/shiver/-/blob/master/scripts/msme_snr.py)

* Specific tasks done on many stations, many events
* Can go through scripts to see how it generally works
