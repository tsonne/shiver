# ObsPy Course 3
* Date: 2020-10-21
* Speaker: Tim Sonnemann
* Purpose:
	* Stream waveforms (FDSN, seedlink),
	* Calculate travel times,
	* Use GUI to view waveforms,

## Install Pyrocko
* Pyrocko is a Python package developed at GFZ/Germany
	* https://pyrocko.org/
	* website contains documentation and examples
* It is independent of ObsPy and covers various data management, research and graphical interaction applications for seismic, InSAR and GPS data
* Get it by Anaconda:
```sh
conda activate seis
conda install -c pyrocko pyrocko
```

## Update shiver
* Obtain the latest lesson and script files in `shiver`
```sh
cd path/to/git/shiver
git pull
```

## Scripts and Examples

### Pyrocko
* want to get live seismograms
    * `pyrocko.snuffler` manual says to install `slinktool`
    * installing `slinktool` from IRIS
        * read: http://ds.iris.edu/ds/nodes/dmc/services/seedlink/
        * dl: https://github.com/iris-edu/slinktool/releases

#### Install slinktool
* Install `slinktool` and its manpage:
```sh
cd git  # directory to unpack and build, not really git
wget https://github.com/iris-edu/slinktool/archive/v4.3.tar.gz
tar xf v4.3.tar.gz
cd slinktool-4.3
make  # sudo apt-get install build-essential
sudo cp slinktool /usr/local/bin/
# cp slinktool ~/bin/
# IF TERMINAL MANUAL PAGES DESIRED:
gzip -k doc/slinktool.1
sudo cp doc/slinktool.1.gz /usr/local/share/man/man1/
sudo mandb
```

#### Slinktool Seedlink query channels
* query available streams at IMO with `slinktool`:
	* `slinktool -Q sandur.sil.vedur.is:18000`
	* `slinktool -Q geofon.gfz-potsdam.de | less`
		* many lines, write or pipe to reader

#### Seedlink with Pyrocko Snuffler example
* try out seedlink in `snuffler`
    * `snuffler seedlink://geofon.gfz-potsdam.de/GE.*.*.SHZ --follow=200`
* get some IMO streams:
```sh
snuffler seedlink://sandur.sil.vedur.is:18000/VI_ask:HHZ,VI_mjo:HHZ,VI_kri:HHZ --follow 100
```

* ObsPy has no GUI app, but can also connect to seedlink servers
* https://docs.obspy.org/tutorial/code_snippets/easyseedlink.html

#### Pyrocko Jackseis example
* use `jackseis` to restructure one big file to station-channel-hour
    * very fast and convenient
    * read help/manual for details
```sh
# This is a shell script
INDIR=/mnt/M2_1/DATA/GR/GERES/
OUTDIR=/mnt/M2_1/DATA/GR/
TINC=3600
TEMPL='%(wmin_year)s/%n/%s/%c/%n.%s.%l.%c.%(wmin)s'
#conda activate seis
jackseis $INDIR --output=$TEMPL \
  --tinc=$TINC --output-dir=$OUTDIR
```

### One Long Step-by-step Example
* Please have a look at the following script
	* [tests_tutorial3.py](https://gitlab.com/tsonne/shiver/-/blob/master/scripts/tests_tutorial3.py)

