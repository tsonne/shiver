# ObsPy Course 4
* Date: 2020-10-28
* Speaker: Tim Sonnemann
* Purpose:
	* More Python/ObsPy tutorials
	* Data locations at IMO
	* Exercise

## Learning more Python

### Quick Intro to Data Processing with Python
* [OpenTechSchool](https://www.opentechschool.org/) is a good, free online learning resource for Python
    * [Python for Beginners](http://opentechschool.github.io/python-beginners/)
        * basic exercises with examples for new programmers
    * [Data Processing with Python](http://opentechschool.github.io/python-data-intro/)
        * contains a recap of basic Python (short but good)
        * introduces Jupyter Notebooks
    * more tutorials on things like web app development and JavaScript

### Extensive Intro to scientific Python
* [Scipy Lecture Notes](http://scipy-lectures.org/) are tutorials on scientific Python
    * beginner introduction, advanced topics and specific packages
    * each subchapter might take about 1 to 2 hours (total maybe 30 h)

### Seismo-Live: A Training Platform for Seismology
* [Seismo-Live](http://seismo-live.org/) by the ObsPy team
    * Scripts to demonstrate and teach seismic data handling in Python
    * Jupyter notebooks for live code execution in online virtual machine
    * Contains examples for ObsPy usage, signal processing and more

### Package Documentation
* [Official Python website](https://www.python.org/)
    * links to Python beginner's guide and documentation
    * [Python documentation](https://docs.python.org/3/)
        * docs on Python standard library with tutorial
* [NumPy and SciPy Docs](https://docs.scipy.org/doc/)
    * [complete NumPy docs](https://numpy.org/doc/stable/)
    * [complete SciPy docs](https://docs.scipy.org/doc/scipy/reference/)
    * contains full tutorials and docs of all functions
* [Matplotlib docs](https://matplotlib.org/)
    * Python plotting gallery, examples and docs for everything


## Data Locations at IMO

### SIL Event Parameter Files
* SIL event parameter files location:
    * `granit:/mnt/sdc1/sk2/` (direct)
    * `eldstod:/net/granit/mnt/sdc1/sk2/` (link)
    * `eldstod:/sk2/` (sym.link)
    * `hristingur:/sk1/` (link)
    * pattern: `/sil/bc/%Y/%b/%d/%H:%M:%S`

### Seismic Waveform Data
* BC format, all stations, all time (2 min files):
    * `viti:/sil/bc/` (direct)
    * `eldstod:/net/granit/mnt/sdb1/sil/bc/` (link to viti)
    * `hristingur:/net/viti.sil/sil/bc/` (link, doesn't always appear)
    * `hristingur:/sil/bc/` (link to above, always works)
    * `hristingur:/net/granit.vedur.is/mnt/sdb1/sil/bc/` (link to viti)
    * pattern: `/sil/bc/%Y/%b/%d/%STA/`

* MSEED format, only 5-6 years, only until 2018:
    * `eldstod:/net/frumgogn04/data/mseed/` (link)

* MSEED and GCF formats in same folder, last 2 years until now:
    * `eldstod:/net/frumgogn04/data/guralp/nam1/` (link)
        * ~970 subdirs, one per day, pattern: `%Y-%m-%d`
        * earliest continuous: `2017-12-05/`
        * next lower level: ~35535 files (mseed,gcf)
            * mseed pattern: `%S.%C.%L-%H%M.mseed`
                * `%S` = stream code (3355 for kri)
                * `%C` = component code (HHX)
                * `%L` = location (00, can also be empty)
                * `%H%M` = hour minute
            * show all station IDs with gcf data:
            * `find /net/frumgogn04/data/guralp/nam1/2020-07-12 -name '*gcf' | sed 's/.*\///g;s/\(.*\)-\(.*\)-\(.*\)/\1/' | sort -u`
            * show all station IDs with mseed data:
            * `find /net/frumgogn04/data/guralp/nam1/2020-07-12 -name '*mseed' | sed 's/.*\///g;s/\(.*\)\.\(.*\)\.\(.*\)\.\(.*\)/\1/' | sort -u`
            * show all components of all mseed stations:
            * `find /net/frumgogn04/data/guralp/nam1/2020-07-12 -name '*mseed' | sed 's/.*\///g;s/\(.*\)\.\(.*\)\.\(.*\)/\1/' | sort -u`
            * show all components of a station/stream:
            * `ls /net/frumgogn04/data/guralp/nam1/2020-01-14/3355* | sed 's/.*\///g;s/\(.*\)\.\(.*\)\.\(.*\)/\1/' | sort -u`

* Waveform data streams:
    * `SCREAM!` server on `sandur` (10.170.10.12)
        * alias `sandur.sil.vedur.is`
    * query available streams with `slinktool`:
		* `slinktool -Q sandur.sil.vedur.is:18000`
	* show some stations with `pyrocko.snuffler`:
		* `snuffler seedlink://sandur.sil.vedur.is:18000/VI_ask:HHZ,VI_mjo:HHZ --follow 100`

### Seismic Station Info
* Response, location and network files
* should be in `/usr/sil/etc/` but not up to date on each machine
    * found in `viti, granit, skuti, hellir`
    * up to date only in `skuti` (?)
    * `hristingur:/net/granit.vedur.is/home/sil/etc/` (looks recent)
        * `granit:/home/sil/etc/` (direct)
	* up to date station codes with coordinates: `net.dat`
	* stale copies from 2020-05-25 in
		* `shiver/dat/sil/etc/`
* How to get station inventory.xml
    * go to https://smp.gempa.de
    * user: sil; pass: ***
    * select "sil" in Repositories (front page after login)
    * select "export" on left, click "Export" on latest version (top)
    * can choose xml version, can also deselect networks/stations
        * if using obspy, only XML versions 0.7 to 0.9 works for now
    * download, see some local copies:
		* `shiver/dat/sil/inventory/`
* inventory file should be always up-to-date,
* but only reliable back to about 2015 (check if still correct!)

### Tremor Data
* `granit:/mnt/sdd1/sil/trem/` (direct)
* `eldstod:/net/granit/mnt/sdd1/sil/trem/` (link)
* `hristingur:/net/granit.vedur.is/mnt/sdd1/sil/trem/` (link)

### Strain Data
* `frumgogn04:/mnt/strain/data/` (direct)
* `eldstod:/net/frumgogn04/mnt/strain/data/` (link)


## Exercise
* Load SIL catalog
* Extract most recent event of M > 5.5
* Get BC waveform data for that event
* Remove response
* Plot waveforms
* Calculate horizontal maximum velocity
* Calculate epicentral distances R
* Invert for Mpgv using PGV and R
