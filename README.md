# shiver
* Purpose: collection of seismic analysis, plotting, mapping code
* Dates:
    * 2020-04-30 first collection
    * 2020-05-24 first git push
* Authors:
    * Tim (main dev, tim@vedur.is)
    * Yesim (first BC to obspy stream method)
    * Claudia (bugfix)

## Purpose
* get response-corrected SIL waveform data
* station-event map/depth plots
* seismic array data processing
* SIL waveform data overviews per station
* utilities for seismic analysis

## Requirements
Need something to read BC wavforms, do some time-based string and dir patterns, and other things:
* `BcPy` by E. Kjartansson, R. Bödvarsson, A. Pharasyn, P. Schmidt, T. Sonnemann
* `gpslibrary/gtimes` by B. G. Ófeigsson
* `gpslibrary/geo_dataread` by B. G. Ófeigsson, T. Sonnemann
* `obspy`
* `fastparquet` (handle parquet file format)
* `zstandard` (use zstandard compression for parquet files)

## Installation with conda
* require `conda` Python environment
    * `miniconda` is enough, smaller and faster to install
    * https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html

* setup conda environment:
```sh
conda config --add channels conda-forge
conda create -n <env_name> python=3.7  # choose a name for this environment
conda activate <env_name>  # start using that environment
conda install spyder obspy pandas fastparquet zstandard
conda install conda-build pillow geopy
```

* get git repos and install:
```sh
# prepare directory for git repos
mkdir my_new_repo_path
cd my_new_repo_path
git init
# gpslibrary modules
git clone https://gitlab.com/gpskings/gpslibrary.git
conda develop gpslibrary/gtimes
conda develop gpslibrary/geo_dataread
# BcPy module
git clone https://gitlab.com/tsonne/bcpy.git
cd bcpy
python setup.py install
cd ..
# shiver repo
git clone https://gitlab.com/tsonne/shiver.git
cd shiver
python setup.py install
```

## Deinstallation
* if desired, a package installed with `conda develop` can be removed:
```sh
conda develop -u <path_to_package>
```

## Structure
* `config/` config files for scripts
* `dat/` example input data (coordinates, response...)
* `doc` course material for introduction to Python/ObsPy/shiver
* `out/` processed output
* `scripts/` various scripts that use `shiver`
* `shiver/` core utility modules

### File naming pattern?
* `u_*.py` for utility scripts with functions
    * these are called by main scripts for all processing
    * not run by themselves
* otherwise not really fixed, suggestions:
    * prefix on basic action, `proc, plot, sim`
    * name part on data type, `array, ss, cat`
        * array, single station, catalog
    * name part on dataset, `SIL, ICE1`
    * suffix on mode of action, `rt` real-time
* no need to actually be strict with this...

## Documentation

### `shiver/`
* application (util) modules
    * `u_array.py` fk analysis, beamforming, array TF
    * `u_ICE1.py` use ICEARRAY1 files and coords
    * `u_io.py` config parsing, basic utils
    * `u_map.py` (not yet created)
    * `u_obs.py` seismic trace processing, plots
    * `u_SIL.py` SIL BC stream, SIL response, SIL event catalog

### `scripts/`
* scripts using `shiver` modules for various tasks
    * `plot_ss_events.py` script to collect waveforms for one station and many events, then create trace and spectra plots with some basic processing
    * `proc_array_ICE1.py` ICEARRAY1 data proc (Norsar, SERA)
        * input data unavailable, not yet useful
    * `proc_SIL_single.py` script to demonstrate basic processing of SIL data for one event (not yet done)
        * get waveforms (all stations, one time window)
        * option to save as mseed
        * remove response, filter all
        * check for clipped traces
        * get PGV, Mpgv
        * plot all traces (raw, corrected, filtered)
        * plot PGV vs epicentral distance
    * `msme.py` multi station multi event processing
        * select events from catalog
        * for each event, get all SIL waveforms
        * remove response, filter, check clipped
        * get distance, PGV, Mpgv
        * plot all traces (raw, corrected, filtered)
        * plot PGV vs epicentral distance

### `dat/`
* `ICE1/` contains ICEARRAY1 station table
* `map/` contains some topo/land/river data for Iceland
* `sil/`
	* `bc/` has a few example BC waveform files
    * `catalog/` has `pandas.DataFrame` file of SIL event catalog
        * from 1991 to 2020-06-23, `parquet` file `zstandard` compr. 
    * `etc/` same as `skuti:/usr/sil/etc/` from 2020-07-07
        * contains SIL response and coordinate files
    * `inventory/` has seiscomp3 stationXML file for SIL
        * from 2020-03-06, cannot update as too many issues...
    * `tables/` has simple table of SIL station locations
        * from 2020-03-04, see `etc/net.dat` for up-to-date info

### `config/`
* `20200420_conf.yml` config for `proc_SIL_single.py`
* `array_prc.yml` config for `proc_array_ICE1.py`
* `MJO_sse_conf.yml` config for `plot_ss_events.py`
* `SIL_msme_conf.yml` config for `msme.py`

