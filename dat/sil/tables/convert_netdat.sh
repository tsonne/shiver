#!/bin/bash
# convert net.dat file from granit:/home/sil/etc/net.dat
# to have consisten table file with spaces between columns
# 2020-03-04 tim
awk '!/^#/{
    x=$0;
    if(length(x)>22){
        printf("%s %s %s %s\n",
               substr(x,1,5),
               substr(x,6,8),
               substr(x,14,9),
               substr(x,23,6))}}' net.dat > SIL_stations.dat