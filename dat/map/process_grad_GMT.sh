#!/bin/bash
#
# tif to grd, gradient, grd to tif
# 2019-06-03 ts IMO, Iceland
###############################################################################
grd1=50n030w_20101117_gmted_med075.tif
grd2=Iceland_20101117_gmted_med075.grd
grd3=Iceland_20101117_gmted_med075_grad_fgN.grd
grd4=Iceland_20101117_gmted_med075_grad_fgN.tif

#grdfft $grd1 -G$grd2 -F-/350/3 -fg -N+l -V || exit 1
#grdsample $grd2 -G$grd3 -I5s -nl -V || exit 1
r="-25/-13/63/67"
grdconvert $grd1 -R$r $grd2
grdcut $rgd2 -G$grd3 -R$r
grdgradient $grd2 -G$grd3 -A315 -N
grdconvert $grd3 $grd4=gd:GTIFF

echo "ok, done!"
