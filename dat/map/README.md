# Mapping Data
* Some topo and polygon data for maps

## What to do here?
* extract `iceland_gis.tar.xz` to get a precalculated shade map as georeferenced tif of Iceland and a few polygon shapefiles from openstreetmap (only land and water objects, from 2016)
    * on Linux, need `xz-utils` to decompress, run:
        * on Ubuntu/Debian: `sudo apt-get install xz-utils`
        * on CentOS/Fedora/RHEL: `yum install xz`
        * to unpack: `tar xf iceland_gis.tar.xz`

## About Data Sources and Processing
* if complete up-to-date shapefiles for Iceland are desired see: [http://download.geofabrik.de/europe/iceland.html](http://download.geofabrik.de/europe/iceland.html)
* the original topography file was obtained from [USGS EarthExplorer](https://earthexplorer.usgs.gov/)
    * to use [GMT](https://www.generic-mapping-tools.org/) to cut and get a gradient map, check the script `process_grad_GMT.sh`
