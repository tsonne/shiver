#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 15:50:40 2020

@author: ts
"""
import u_SIL
import u_obs
import os
from timeit import default_timer as timer

import numpy as np
import scipy as sp
import matplotlib.pyplot as plt

path_BC = '/mnt/sil/bc/'

t_start = '2020-06-21 19:07:22'
t_end = '2020-06-21 19:10:22'
stations = ['dyn']
out_dir = '../out/clip_20200621190752dyn'
xlim = [85, 110]
k = 1  # component
"""
t_start = '2020-06-21 19:07:22'
t_end = '2020-06-21 19:10:22'
stations = ['asb']
out_dir = '../out/clip_20200621190752'
xlim = [119, 123]
k = 0  # component

t_start = '2020-04-20 03:53:47'
t_end = '2020-04-20 03:59:47'
stations = ['dyn']
out_dir = '../out/clip_20200420035347'
xlim = [67, 72]
k = 0  # component
"""
st0 = u_SIL.read_bc_by_time(t_start, t_end, stations, path_BC=path_BC)
os.makedirs(out_dir, exist_ok=True)

figpath = os.path.join(out_dir, 'traces_long.png')
st0.plot(outfile=figpath, size=(1900, 1000))

figpath = os.path.join(out_dir, 'traces_detail.png')
dt = st0[k].stats.starttime
st0.plot(outfile=figpath, starttime=dt + 60, endtime=dt + 90,
         size=(1900, 1000))


# ylim = [2e6, 3.3e6]
# %matplotlib auto
# %matplotlib inline
#
threshold = 3e6
figpath = os.path.join(out_dir, 'traces_detail2.png')
pdict = {'dpi': 100, 'size': (20, 10), 'xgrid': True, 'ygrid': True,
         'xlim': xlim, 'ylim': None, 'relt': True,
         'hline': [threshold, -threshold]}
u_obs.simple_traceplot(st0[k], pdict, figpath)

xx = st0[k].copy()
xx.data = np.abs(np.gradient(xx.data))
figpath = os.path.join(out_dir, 'tdiff_detail.png')
pdict = {'dpi': 100, 'size': (20, 10),
         'xlim': xlim, 'ylim': None, 'relt': True}
u_obs.simple_traceplot(xx, pdict, figpath)

thr_med = 2e3
data = st0[k].data.copy()
dabs = np.abs(data.copy())
babsthr = dabs > threshold
ddif = np.abs(np.gradient(data))
medf = sp.ndimage.median_filter(ddif, 8)
unis = sp.ndimage.uniform_filter(ddif, 8)
bmedthr = medf < thr_med
bb = babsthr & bmedthr
xx = st0[k].copy()
xx.data = ddif
figpath = os.path.join(out_dir, 'tdiff_abs.png')
pdict = {'dpi': 100, 'size': (20, 10), 'xgrid': True, 'ygrid': True,
         'xlim': xlim, 'ylim': [-1e3, 1e4], 'relt': True}
fig = u_obs.simple_traceplot(xx, pdict)
tt = xx.times('matplotlib')
ax = fig.axes[0]
ax.plot(tt, medf, 'r-')
ax.plot(tt, unis, 'b-')
ax.fill_between(tt, 0, 1, where=babsthr,
                color='red', alpha=0.2, transform=ax.get_xaxis_transform())
ax.fill_between(tt, 0, 1, where=bb,
                color='indigo', alpha=0.4, transform=ax.get_xaxis_transform())
fig.savefig(figpath, bbox_inches="tight")
plt.close('all')
# fig.show()


# TIME CHECKS FOR MAX ABS VALUE:
TCHEK1 = False
if TCHEK1:
    N = 1000
    time_bc1 = timer()
    for i in range(N):
        b1 = st0[2].data.max() > threshold or st0[2].data.min() < -threshold
    time_bc2 = timer()
    print('# 1, took {:6.3f} s'.format(time_bc2 - time_bc1))

    time_bc1 = timer()
    for i in range(N):
        b1 = np.max(np.abs([st0[2].data.max(), st0[2].data.min()])) > threshold
    time_bc2 = timer()
    print('# 2, took {:6.3f} s'.format(time_bc2 - time_bc1))

    time_bc1 = timer()
    for i in range(N):
        b1 = np.max(np.abs(st0[2].data)) > threshold
    time_bc2 = timer()
    print('# 3, took {:6.3f} s'.format(time_bc2 - time_bc1))

    time_bc1 = timer()
    for i in range(N):
        b1 = np.abs(st0[2].data).max() > threshold
    time_bc2 = timer()
    print('# 4, took {:6.3f} s'.format(time_bc2 - time_bc1))

    time_bc1 = timer()
    for i in range(N):
        b1 = any(np.abs(st0[2].data) > threshold)
    time_bc2 = timer()
    print('# 5, took {:6.3f} s'.format(time_bc2 - time_bc1))


# USE U_OBS METHODS:
clip_list = u_obs.check_clipped(st0, THR_ABS=threshold)
TCHEK2 = False
if TCHEK2:
    N = 1000
    time_bc1 = timer()
    for i in range(N):
        clip_list = u_obs.check_clipped(st0, silent=True)
    time_bc2 = timer()
    print('# 1, took {:6.3f} s'.format(time_bc2 - time_bc1))

    time_bc1 = timer()
    for i in range(N):
        clip_list = u_obs.check_clipped_old(st0, silent=True)
    time_bc2 = timer()
    print('# 2, took {:6.3f} s'.format(time_bc2 - time_bc1))
