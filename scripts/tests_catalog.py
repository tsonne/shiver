#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 19 15:26:29 2020

@author: ts

test updating SIL catalog file,
seems fairly fast (~7 s) reading pickle file and 4 months of new event data,
then saving to parquet file.

Warning: original sil_read function can only read pickle files!
"""
import os
from datetime import datetime
from timeit import default_timer as timer
from shiver import u_SIL
from shiver import u_io

# catalog parameters
CAT = {'SIL_CAT_file': ('/home/ts/Documents/IMO/PROJECT/Seismology/out/'
                        'sil_cat/events_19900101_20200225.pkl.gz'),
       'SIL_CAT_top': '/mnt/sk2',
       'start': None,  # '1990-01-01 00:00:00',
       'end': None,  # '2020-02-25 00:00:00',
       'fread': 'events.fps',
       'rcolumns': 'all'}
# new file parameters:
OUT = {'IO_dir': '/home/ts/Documents/IMO/PROJECT/Seismology/out/sil_cat',
       'ext': '.prq.zst',
       'cmpr': 'zstd'}
# update to new dataframe, save to file, name includes start and end dates
t1 = timer()
X = u_SIL.update_silcat_file(CAT, OUT)
t2 = timer()
print(t2 - t1)

# cut out transitional part to check if it's alright
t1 = datetime.strptime('2020-02-23 23:00:00', '%Y-%m-%d %H:%M:%S')
t2 = datetime.strptime('2020-02-25 01:00:00', '%Y-%m-%d %H:%M:%S')
b1 = X.index > t1
b2 = X.index < t2
Z = X[b1 & b2]

# test pandas autodetect format read method
st1 = X.index[0].date().strftime('%Y%m%d')
st2 = X.index[-1].date().strftime('%Y%m%d')
out_file = os.path.join(
    OUT['IO_dir'], 'events_' + st1 + '_' + st2 + OUT['ext'])
f3 = os.path.join(
    OUT['IO_dir'], 'events_' + st1 + '_' + st2 + '.p.zst')
A = u_io.read_pd_file(CAT['SIL_CAT_file'])
B = u_io.read_pd_file(CAT['SIL_CAT_file'], filetype='pickle')
C = u_io.read_pd_file(out_file)
D = u_io.read_pd_file(out_file, filetype='parquet')
E = u_io.read_pd_file(f3)  # if we copy a file to nontypical name
K = u_io.read_pd_file(out_file, filetype='par')  # raise for unknown option
