#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 11:17:58 2020

@author: ts

Get waveform data by FDSN,
Calculate phase travel times (standard model iasp91)
"""
# IPython magic command: set plots to be interactive
%matplotlib auto

# Imports
# for timing
from obspy import UTCDateTime
# for FDSN
from obspy.clients.fdsn import Client
# for calculating phase arrivals
from obspy.taup import TauPyModel
# for using pyrocko snuffler on data
from pyrocko import obspy_compat


# Initialize client, let's use IRIS here
client = Client('IRIS')

# Set network, station and time window
network = 'DK'
station = 'SCO'
location = '*'
channel = 'BHZ'
t_orig = UTCDateTime('20201003132247')
(t_pre, t_post) = (0, 240)
t_start = t_orig + t_pre
t_end = t_orig + t_post

# Get specific piece of data, plot loaded stream
st = client.get_waveforms(network, station, location, channel,
                          t_start, t_end,
                          attach_response=True)
st.plot()

# Get station metadata
inventory = client.get_stations(network=network, station=station,
                                starttime=t_start,
                                endtime=t_end,
                                level="response")
station_id = st[0].id
station_meta = inventory.get_channel_metadata(station_id)
station_lat = station_meta['latitude']
station_lon = station_meta['longitude']

# We are interested in a specific earthquake,
t_orig = UTCDateTime('20201020134315')  # Reykjanes, M5.6
src_lat = 63.895
src_lon = -22.144
depth = 3.3

# Calculate travel times for our chosen station
model = TauPyModel(model='iasp91')
arrivals = model.get_ray_paths_geo(
    source_depth_in_km=depth,
    source_latitude_in_deg=src_lat,
    source_longitude_in_deg=src_lon,
    receiver_latitude_in_deg=station_lat,
    receiver_longitude_in_deg=station_lon,
    phase_list=(['P','S']))

# Plot arrival times, list them
arrivals.plot_times()
print('Have {} arrival objects.'.format( len(arrivals) ))
distance = arrivals[0].distance
arr_times = [x.time for x in arrivals]
time_1 = min(arr_times)
time_2 = max(arr_times)

# Get another time window around P and S for this event
t_start = t_orig + time_1 - 30.
t_end = t_orig + time_2 + 300.
st2 = client.get_waveforms(network, station, location, channel,
                           t_start, t_end,
                           attach_response=True)
st2.plot()

# Show instrument response for station/channel
inventory.plot_response(min_freq=0.001,
                        station=station, channel=channel)

# Remove response without and with prefilter
tr2 = st2[0].copy()
tr2.remove_response()  # default output velocity
tr2.plot()  # note high-frequency ringing/noise

pre_filt = [0.001, 0.002, 6., 8.]
tr2 = st2[0].copy()
tr2.remove_response(pre_filt=pre_filt)
tr2.plot()  # high-frequency ringing/noise gone

# Use advanced GUI 'Snuffler' from Pyrocko to view data
obspy_compat.plant()  # extend ObsPy classes with some pyrocko methods 
tr2.snuffle()

