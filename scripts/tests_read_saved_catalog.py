#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 07:54:14 2020

@author: ts

Read a saved SIL catalog file with some filters,
inspect the dataframe a bit.
"""
from shiver import u_SIL

catalog = {
    # SIL catalog file path (pickle.gz, has precedence if file given)
    'SIL_CAT_file': '../dat/sil/catalog/sil_events.prq.zst',
    # or SIL catalog directory (top dir, keep SIL_CAT_file empty to use)
    #'SIL_CAT_top': '/mnt/sk2',
    # search within these catalog start and end times
    'start': '2015-01-01 00:00:00',
    'end': '2020-08-04 00:00:00',
    # region corner longitudes, latitudes
    # e.g. [-24.5, 63.0, -13.5, 67.0] for all of Iceland
    'lon_W': -24.5,
    'lat_S': 63.0,
    'lon_E': -13.5,
    'lat_N': 67.0,
    # event depth restrictions
    'depth_min': 0.0,
    'depth_max': None,
    # event MLW (SIL magnitude) restrictions
    'mlw_min': 3.5,
    'mlw_max': None,
    # SIL file type to use ('lib.mag' or 'events.fps' (default))
    'fread': 'events.fps',
    # SIL catalog columns to return ('default' or 'all')
    'rcolumns': 'all',
    }

# read the catalog file and return selection as dataframe
df = u_SIL.silcat_by_conf(catalog)

# print some info in terminal
u_SIL.print_some_info_SILCAT(df, verbose=True)

# filter dataframe by magnitude 'MLW'
df2 = df[df['MLW'] > 5.0]
print('Have {} earthquakes.'.format(df2.shape[0]))

# get NumPy array of index values (to terminal)
df2.index.values

# get list of index values (to terminal)
df2.index.tolist()

# print largest magnitude
df2['MLW'].max()

# get index of event with largest magnitude
df2['MLW'].idxmax()

# print time and location of event with largest magnitude
df2.loc[df2['MLW'].idxmax(), ['latitude', 'longitude', 'depth', 'MLW']]
