#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 11:21:10 2020

@author: ts
"""

import pyqtgraph as pg
import pyqtgraph.exporters

# generate something to export
plt = pg.plot([1,5,2,4,3])

# create an exporter instance, as an argument give it
# the item you wish to export
exporter = pg.exporters.ImageExporter(plt.plotItem)

# set export parameters if needed
exporter.parameters()['width'] = 100   # (note this also affects height parameter)

# save to file
exporter.export('fileName.png')