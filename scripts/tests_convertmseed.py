#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  3 17:37:26 2020

@author: ts

Fetch seismic data files, rename network/station/location, save mseed STEIM2.

Must ensure access to input mseed dir (machine frumgogn04 mounted?).
This script gets the data day by day, component by component,
and writes out mseed/STEIM2 files exactly 24 hours long.
(~8M/day/comp, 3comp: ~24M/day, ~730M/month, ~8760M/year)
(takes ~22sec/day to get and write files)

further compression:
    tar.xz (lvl6) reduces to ~83% size (no advantage from higher lvl)
    tar.bz2 reduces to ~86% size

more details:
    Origianl mseed files on nam1 do not have same end and start samples
    repeated in subsequent files (e.g. last 59.99, next file first 00.00),
    and each component has its own file start/end times.
    This script makes non-overlapping files that start at exactly 00.00 each
    day and end at exactly 23:59:59.99 (or last sample before 00.00 if not 100
    Hz) for all components.
    It seems that reading all components together by glob patterns (HH[ZNE])
    as in method convms() is a bit faster than reading three times one
    component as in method convms_sep().
"""
import os
from shiver import u_io
from shiver import u_obs
from shiver import u_SIL
from timeit import default_timer as timer
from obspy import read, Stream, UTCDateTime
from datetime import datetime, timedelta

# CONFIGURATION
# GET INPUT SETTINGS:
conf = u_io.parse_yaml_conf('../config/convertmseed_conf.yml')

start = conf['start']
end = conf['end']
ID = conf['ID']
destination = conf['destination']
ringcf_path = conf['ringcf_path']
indir = conf['indir']
DFMT_IN = conf['DFMT_IN']
DFMT_OUT = conf['DFMT_OUT']
PFMT_OUT = conf['PFMT_OUT']

# CHECK INPUT, FIND STATION STREAM CODE ID, MAKE OUTPUT PATH FORMAT
ring = u_SIL.station_from_ringcf(ID['station'], ringcf_path)
if not ring:
    raise RuntimeError('Could not find in ringstation.cf: ' + ID['station'])
streamc = ring.split()[0]
t_start = datetime.strptime(start, DFMT_IN)
t_end = datetime.strptime(end, DFMT_IN)
n_days = t_end - t_start + timedelta(days=1)
print('# STATION: {}'.format(ID['station']))
print('# CONFIG:  {}'.format(ring))
print('# STREAM:  {}'.format(streamc))
print('# NCOMP:   {}'.format(len(ID['comp'])))
print('# COMP:    {}'.format(ID['comp']))
print('# FROM:    {}'.format(t_start.date()))
print('# TO:      {}'.format(t_end.date()))
print('# NDAYS:   {}'.format(n_days.days))
if n_days < timedelta(days=1):
    raise RuntimeError('Start date must precede end date!')
if not os.path.isdir(indir):
    raise RuntimeError('No input dir: {}'.format(indir))
DFMT_FIN = DFMT_OUT.format(*[ID[k] for k in PFMT_OUT])
kwargs = dict(encoding=11)  # STEIM2


def convms_sep(t_start, ID, t_end, indir, destination, DFMT_IN, DFMT_FIN,
               streamc, kwargs):
    """Get each comp separately, then merge and write."""
    # start previous day, it often has first few samples of next day
    DAY = 86400
    t_cur = t_start - timedelta(days=1)
    i = 0
    D = {}
    for k in ID['comp']:
        D[k] = Stream()
    while t_cur <= t_end:
        i += 1
        print('# {}'.format(t_cur.date()))
        cdir = os.path.join(indir, t_cur.strftime(DFMT_IN))
        odir = os.path.join(destination, t_cur.strftime(DFMT_FIN))
        ustart = UTCDateTime(t_cur)
        uend = ustart + DAY - 0.001  # avoid having sample at 00.000 twice
        for cc in ID['comp']:  # get each component separately
            print('# get {} ... '.format(cc), end='')
            time_bc1 = timer()
            tst = read(cdir + '/{}.{}.*'.format(streamc, cc))
            time_bc2 = timer()
            tst.merge(-1)
            st_info = u_obs.get_stream_info(tst)
            print('took {:6.3f} s, got {:d} samples, start {}, end {}'.format(
                    time_bc2 - time_bc1, st_info['nsamp'],
                    st_info['start'], st_info['end']))
            if st_info['ntrac'] == 0:
                continue
            D[cc] += tst
            D[cc].merge(-1)  # cannot merge renamed yet, but same ID fragments
            for tr in D[cc]:
                tr.stats.network = ID['network']
                tr.stats.station = ID['station']
            if i == 1:
                continue
            D[cc].merge(-1)  # merge all after renaming
            n = len(D[cc])
            ulast = D[cc][n-1].stats.endtime
            xx = D[cc].slice(
                starttime=ustart, endtime=uend, nearest_sample=False)
            u_obs.save_mseed(odir, xx, lbl="", force=False, **kwargs)
            D[cc].trim(starttime=uend, endtime=ulast)
        t_cur = t_cur + timedelta(days=1)
    return None


def allsame(it):
    """Test if all elements of iterable are same."""
    return it.count(it[0]) == len(it)


def construct_pattern(li):
    """
    Get UNIX glob pattern for list of equal length strings.

    Parameters
    ----------
    li : list or set
        List of equal length strings. Example: ['HHE', 'HHN', 'HHZ'].

    Raises
    ------
    RuntimeError
        All strings in given list must be same length due to glob match rules.

    Returns
    -------
    str
        Glob pattern string to match all given strings. Example: 'HH[ZNE]'.
    """
    nl = len(li)
    if nl == 1:  # only one string in list
        return li[0]
    nm = [len(k) for k in li]
    if not allsame(nm):
        raise RuntimeError('Strings in list must all have same length!')
    x = []
    for i in range(nm[0]):
        s = ''.join(set([v[i] for v in li]))
        if len(s) > 1:
            x.append('[{}]'.format(s))
        else:
            x.append(s)
    return ''.join(x)


def convms(t_start, ID, t_end, indir, destination, DFMT_IN, DFMT_FIN,
           streamc, kwargs):
    """Get all components together, merge, then separately write."""
    # little bit faster than reading components separately...
    # start previous day, it often has first few samples of next day
    DAY = 86400
    t_cur = t_start - timedelta(days=1)
    i = 0
    cglob = construct_pattern(ID['comp'])
    D = Stream()
    while t_cur <= t_end:
        i += 1
        print('# {}'.format(t_cur.date()))
        cdir = os.path.join(indir, t_cur.strftime(DFMT_IN))
        odir = os.path.join(destination, t_cur.strftime(DFMT_FIN))
        ustart = UTCDateTime(t_cur)
        t_cur = t_cur + timedelta(days=1)  # update for next iter
        uend = ustart + DAY - 0.001  # avoid having sample at 00.000 twice
        print('# get {} ... '.format(cglob), end='')
        # read mseed files from current daily dir
        time_bc1 = timer()
        tst = read(cdir + '/{}.{}.*'.format(streamc, cglob))
        time_bc2 = timer()
        tst.merge(-1)
        st_info = u_obs.get_stream_info(tst)
        print('took {:6.3f} s, got {:d} samples, start {}, end {}'.format(
                time_bc2 - time_bc1, st_info['nsamp'],
                st_info['start'], st_info['end']))
        if st_info['ntrac'] == 0:
            continue
        D += tst
        D.merge(-1)  # cannot merge renamed yet, but same ID fragments
        for tr in D:
            # assign real network and station names instead of whatever came
            # from digitizer
            tr.stats.network = ID['network']
            tr.stats.station = ID['station']
        if i == 1:
            # collect data without file saving before first day only for first
            # few samples of actual first day
            continue
        D.merge(-1)  # merge all after renaming
        # extract exactly one day, all components, to save to mseed file
        xx = D.slice(starttime=ustart, endtime=uend, nearest_sample=False)
        u_obs.save_mseed(odir, xx, lbl="", force=False, **kwargs)
        # cut off stream to contain only next day
        if uend < st_info['end']:
            # have samples for next day, trim off previous day
            D.trim(starttime=uend, endtime=st_info['end'])
        else:
            # no samples for next day yet, continue with empty stream
            D = Stream()
    return None


time_run_start = timer()
convms(t_start, ID, t_end, indir, destination, DFMT_IN, DFMT_FIN,
       streamc, kwargs)
print('# DONE')
time_run_end = timer()
print('# Elapsed time: {} s'.format(time_run_end - time_run_start))
