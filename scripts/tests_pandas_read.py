#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 16:05:26 2020

@author: ts

Test: 
    While date/time columns get combined and moved to index as expected for
    non-empty file, do date/time columns remain when reading empty file?
Answer: Yes
"""
from io import StringIO


def read_my_file(file):
    import pandas as pd
    columns = ['id', 'date', 'time', 'foo', 'bar']
    dtypes = ['Int64', 'object', 'object', 'Int64', 'object']
    col_dtype = dict(zip(columns, dtypes))
    return pd.read_csv(
                file1,  sep='\s+',  header=None,
                names=columns, dtype=col_dtype, infer_datetime_format=True,
                parse_dates={"date_time": ['date', 'time']},
                index_col="date_time")


file1 = StringIO("""
                 1 20090101 123456.123  100  A
                 2 20090101 123612.987   90  C
                 """)
print('# File 1:')
print(read_my_file(file1))

file2 = StringIO("""
                 """)

print('')
print('# File 2:')
print(read_my_file(file2))
