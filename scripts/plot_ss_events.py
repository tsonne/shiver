#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  1 16:22:48 2020

@author: ts

Collect waveforms for one station and many events,
do basic processing,
create trace and spectra plots.
"""
import os
import numpy as np
from timeit import default_timer as timer
from obspy import UTCDateTime

import u_io
import u_SIL
import u_obs


time_run_start = timer()

## CONFIGURATION
# GET INPUT SETTINGS:
conf = u_io.parse_yaml_conf('../config/MJO_sse_conf.yml')
# STATION
station = conf['stations']['codes']
# BC PREPROC INPUT
path_BC = conf['paths']['BC_in']
code_dct = {'network': conf['stations']['network']}
# INPUT/OUTPUT DIRS
inv_dir = conf['paths']['inventory']
etc_dir = conf['paths']['sil_etc']
out_dir = conf['paths']['outdir']
# FLAGS
PLOT_TRACES = conf['action']['plot_traces']
RESP_XML = conf['response_xml']


# GET SIL CATALOG AND SELECT SOME EVENTS OF INTEREST,
# USE ZIPPED PICKLED SIL CAT FILE:
X = u_SIL.silcat_by_conf(conf['catalog'])

# PRINT INFO ABOUT LARGEST EVENTS AND PICKED POLARITIES:
u_SIL.print_some_info_SILCAT(X, verbose=True)


# get epicentral/hypocentral distances (one station, many events):
src_crd = X[['latitude', 'longitude']].values.tolist()
sta_df = u_SIL.get_SIL_station_table_df(etc_dir)
sta_df = sta_df.loc[station]
sta_LL = sta_df[['lat', 'lon']].values.tolist()
XX = X.assign(repi=u_obs.get_distances(sta_LL, src_crd))
XX = XX.assign(rhyp=np.sqrt(XX['repi']**2 + XX['depth']**2))
XX = XX.assign(azi=u_obs.get_azim(sta_LL, src_crd))

# DETERMINE TIME WINDOWS OF INTEREST FOR EACH EVENT:
dt_pre = 50  # int, seconds before origin time for window start
dt_post = 90  # int, seconds after  origin time for window end
XX = XX.assign(t_start=(XX.index - np.timedelta64(dt_pre, 's')),
               t_end=(XX.index + np.timedelta64(dt_post, 's')))

# SET ZOOM PLOT
path_zooms = os.path.join(out_dir, 'zooms')
os.makedirs(path_zooms, exist_ok=True)
path_flt = os.path.join(out_dir, 'flt')
os.makedirs(path_flt, exist_ok=True)
path_spc = os.path.join(out_dir, 'spc')
os.makedirs(path_spc, exist_ok=True)
zoom_pre = 1.  # float, seconds before origin time for zoom start
zoom_post = 20.  # float, seconds after  origin time for zoom end

# LOOP THROUGH EVENTS:
#   GET WAVEFORMS, CORRECT, FILTER AND PLOT SIGNAL TO FIGURE FILES
SDT_FMT = '%Y%m%d%H%M%S'
fbb1 = 1.
fbb2 = 35.
SPC_DCT = {'estop': -1, 'tspre': 0.5, 'tpost': 5.5, 'tnoise': 10.}
cnt = 0
for Y in XX.itertuples():
    cnt += 1
    if cnt > 199:  # debug
        break
    time_bc1 = timer()
    t_origin = UTCDateTime(Y.Index)

    # TRY TO GET WAVEFORMS
    st0 = u_SIL.read_bc_by_time(Y.t_start, Y.t_end, station,
                                codes=code_dct, path_BC=path_BC)
    time_bc2 = timer()
    if not st0:
        print('# No BC data for {}: {}'.format(cnt, Y.Index))
        continue
    print('# ID:{}, Got BC waveforms, took {:6.3f} s'.format(
        cnt, time_bc2 - time_bc1))

    # for now merge gaps and proceed with all channels:
    st = st0.copy()
    st.merge()
    st.sort()

    # REMOVE RESPONSE,
    # prefilter, return velocity in m/s
    print('# Remove response...')
    pre_filt = [0.07, 0.15, 35, 45]
    if RESP_XML:
        # remove response using seiscomp station XML
        stcor, inv = u_obs.remove_resp_xml(st, pre_filt, inv_dir)
    else:
        # remove response using SIL etc files
        stcor = u_SIL.remove_resp_sil(st, pre_filt, etc_dir)
    print('# Removed response.')

    # BANDPASS FILTER corrected data
    stflt = stcor.copy()
    stflt.taper(0.01)
    stflt.filter('bandpass', freqmin=fbb1, freqmax=fbb2)

    # HIGHER F FILTER
    stfl2 = stcor.copy()
    stfl2.taper(0.01)
    stfl2.filter('bandpass', freqmin=2.0, freqmax=30.)

    # AUTO P/S PICK
    dfPick = u_obs.stream_pick(stfl2, flo=2., fhi=30.)

    if PLOT_TRACES:
        print('# Create trace figures...')

        fn = Y.Index.strftime(SDT_FMT) + '_raw.png'
        figpath = os.path.join(path_zooms, fn)
        stz = st.slice(t_origin - zoom_pre,
                       t_origin + zoom_post)
        stz.plot(equal_scale=True, size=(1200, 600), dpi=110, outfile=figpath)

        re = Y.repi
        rh = Y.rhyp
        h = Y.depth
        m = Y.MLW
        fn = Y.Index.strftime(SDT_FMT) + '_flt.png'
        figpath = os.path.join(path_flt, fn)
        stz = stflt.slice(t_origin - zoom_pre,
                          t_origin + zoom_post)

        ftitle = ('O: {}, $R_e$ = {:5.2f} km, $R_h$ = {:5.2f} km, h = '
                  '{:5.2f} km, azi = {:3.0f}°, M = {:4.1f}, BP({:5.2f}, '
                  '{:5.2f}) Hz').format(
                      Y.Index, re, rh, h, Y.azi, m, fbb1, fbb2)
        u_obs.save_bw_traceplot(figpath, stz, title=ftitle,
                                figsize=(12., 6.), dpi=200)

        fn = Y.Index.strftime(SDT_FMT)
        FFS, FFN = u_obs.noise_signal_spectra(stcor, dfPick, outdir=path_spc,
                                              fn_prefix=fn, pdict=SPC_DCT)
