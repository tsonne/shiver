#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug  1 15:40:22 2020

@author: ts

Parse entire SIL event database from source (/mnt/sk2/),
save as parquet file.
"""
from timeit import default_timer as timer
from shiver import u_SIL

# catalog parameters
CAT = {'SIL_CAT_top': '/mnt/sk2',
       'start': '1990-01-01 00:00:00',
       'end': None,  #
       'fread': 'events.fps',
       'rcolumns': 'all',
       'logging_level': None}
# new file parameters:
OUT = {'IO_dir': '../dat/sil/catalog/',
       'ext': '.prq.zst',
       'cmpr': 'zstd'}
# update to new dataframe, save to file, name includes start and end dates
t1 = timer()
X = u_SIL.silcat_by_conf(CAT)
t2 = timer()
print(t2 - t1)
print('# Got entire catalog')
u_SIL.print_some_info_SILCAT(X)
if not X.empty:
    filepath = u_SIL.write_silcat_file(X, OUT)
