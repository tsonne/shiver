#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 20 19:17:14 2020

@author: ts

Call this script with a config file setting what data, methods and processing
parameters to use for seismic array data analysis.
"""


def parse_cli_args():
    """Define and parse command line arguments to this script."""
    import argparse
    parser = argparse.ArgumentParser(description='Array proc script')
    parser.add_argument('first', type=int, nargs='?',
                        help='first event number to process')
    parser.add_argument('last', type=int, nargs='?',
                        help='last event number to process')
    parser.add_argument('config', type=str, nargs='?',
                        default='array_proc.yml',
                        help='config file path')
    return parser.parse_args()


def main():
    """Run entire array processing line."""
    import os
    import copy
    import u_ICE1
    import u_obs
    import u_array
    from u_io import parse_yaml_conf

    # get input settings
    args = parse_cli_args()
    conf = parse_yaml_conf(args.config)
    conf['cli'] = args

    # read station table file
    stats = u_ICE1.get_station_table()

    # get subdirectories and loop through them
    indirs = u_ICE1.list_subdir_selection(
            conf['paths']['top_in'], args.first, args.last)
    ecnt = 0
    indirs = indirs[1555:1557]  # DEBUG: try two late events
    for indir in indirs:
        ecnt += 1
        print('### INPUT DATA DIR, NUMBER:')
        print('{}, {}'.format(indir, ecnt))
        cfg = copy.deepcopy(conf)
        # Load vertical components
        st = u_ICE1.get_stream(indir, 'Z')
        #
        # get relative onset time
        ons, bn = u_ICE1.get_onset(conf['paths']['f_ons'], indir)
        if ons < 0.:
            print('WARNING: no P onset picked for event: {}'.format(bn))
            continue
        else:
            print('Found picked P onset for event {} at : {} s'.
                  format(bn, ons))
        cfg['cur'] = {}
        cfg['cur']['ons'] = ons
        fbase = bn.replace('.', '')
        cfg['paths']['fbase'] = fbase
        #
        # assign coordinates to loaded traces using station table dict:
        # (assumes that the loaded traces have matching station codes)
        u_ICE1.assign_station_coords(stats, st)
        #
        # remove bad signals:
        if len(st.select(station='IS608')) > 0:
            for tr in st.select(station="IS688"):
                st.remove(tr)
        #
        # check if min number of channels, else exit
        nchan = len(st)
        if nchan < conf['fk']['min_nchan']:
            print(('WARNING: Not enough traces: '
                   'event {} has {}, min is {}').format(
                    bn, nchan, conf['fk']['min_nchan']))
            continue
        #
        # get start time and trace length,
        # however this does not account for non-sync traces!
        # TODO: synchronize traces in stream, get latest start, earliest end.
        time0 = st[0].stats.starttime
        timeZ = st[0].stats.endtime
        smpdel = st[0].stats.delta
        trlen = smpdel * st[0].stats.npts
        dur = conf['fk']['presig']+conf['fk']['postsig']
        if trlen < dur:
            print(('WARNING: Not enough samples: '
                   'event {} has {} s, min is {} s').format(bn, trlen, dur))
            continue
        cfg['cur']['time0'] = time0
        cfg['cur']['timeZ'] = timeZ
        cfg['cur']['smpdel'] = smpdel
        #
        # make output dir:
        outdir = conf['paths']['top_out'] + '/' + fbase
        pbase = outdir + '/' + fbase
        cfg['paths']['pbase'] = pbase
        os.makedirs(outdir, exist_ok=True)
        print('INFO: created/found output dir: {}'.format(outdir))
        #
        # NO INSTRUMENT CORRECTION HERE! ACCELEROMETERS ASSUMED FLAT RESPONSE!
        #
        # remove mean:
        st.detrend('constant')
        #
        # print out a PNG image file of the raw traces:
        fpng = pbase + '_raw.png'
        u_obs.save_bw_traceplot(fpng, st)
        #
        # get array transfer function image
        fpng = pbase + '_ATF_k.png'
        u_array.save_array_transfer_k_plot(st, fpng)
        # slowness limits and step, min/max frequency, freq sample distance
        #
        #
        # Create spectrogram and save figure
        u_obs.save_multi_spectrogram(st, cfg)
        #
        # LOOP OVER FILTERBANDS
        nbands = len(conf['fk']['fbands'])
        for iFB in range(nbands):
            u_array.fk_analysis(st, cfg, iFB)


if __name__ == "__main__":
    main()
