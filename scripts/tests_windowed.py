#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 15:23:19 2020

@author: ts

Testing windowed analysis using obspy methods
- using simple calculations and indexing data directly is 43 times faster
"""
import os
from datetime import datetime
from shiver import u_obs
from shiver import u_SIL
from obspy import UTCDateTime
import numpy as np
from timeit import default_timer as timer

# Input values
dir_mseed = '/mnt/M2_1/DATA/VI/project/adv_msme/20150102173906/mseed'
pre_filt = [0.025, 0.05, 35., 45.]  # response removal prefilter corners
fb = [0.05, 15.]  # filter band corners
etc_dir = '../dat/sil/etc/'

# Get mseed data with SIL BC headers
st0 = u_SIL.read_mseed_bc(pattern=(dir_mseed + '/*msd'))
if not st0: raise RuntimeError('No data for given specs!')
st = st0.copy()
st.merge()
st.sort()

# Remove gappy stations
dSI = u_obs.get_stream_info(st)
gappy_station_list = u_obs.get_gappy_stations(st0, dSI['start'], dSI['end'])
if gappy_station_list:
    for station in gappy_station_list:
        for tr in st.select(station=station):
            st.remove(tr)
used_ev_sta_codes = u_obs.station_stream_list(st)

# Remove response
stcor = u_SIL.remove_resp_sil(st, pre_filt, etc_dir)

# Filter
stflt = stcor.copy()
stflt.taper(0.01)
stflt.filter('bandpass', freqmin=fb[0], freqmax=fb[1])


## TEST THESE METHODS

def get_windowed_maxabs_naive(
        st, window_length, step, offset=0,
        include_partial_windows=False, nearest_sample=True):
    """
    Get the lowest max amplitude of a series of time windows for each trace.
    
    Return DataFrame, one row per station, four columns E, N, Z, V3.
    Go over each trace in sliding time windows, keep the max abs amplitude,
    return the lowest per trace, i.e. possible noise floor.
    V3 is the vector length of all 3 components.

    Parameters
    ----------
    st : obspy.stream
        DESCRIPTION.
    window_length : TYPE
        DESCRIPTION.
    step : TYPE
        DESCRIPTION.
    offset : TYPE, optional
        DESCRIPTION. The default is 0.
    include_partial_windows : TYPE, optional
        DESCRIPTION. The default is False.
    nearest_sample : TYPE, optional
        DESCRIPTION. The default is True.

    Returns
    -------
    df : pandas.DataFrame
        DataFrame, one row per station, four columns E, N, Z, V3.

    """
    from obspy.core.util.misc import get_window_times
    import pandas as pd
    import numpy as np
    windows = get_window_times(
            starttime=st[0].stats.starttime,
            endtime=st[0].stats.endtime,
            window_length=window_length,
            step=step,
            offset=offset,
            include_partial_windows=include_partial_windows)
    Nw = len(windows)
    df = pd.DataFrame(index=u_obs.station_stream_list(st),
                      columns=['E','N','Z'],
                      dtype=np.float)
    for tr in st:
        x = np.empty(Nw)
        i = 0
        for w in tr.slide(
                window_length=window_length,
                step=step,
                offset=offset,
                include_partial_windows=include_partial_windows,
                nearest_sample=nearest_sample):
            x[i] = np.max(np.abs(w.data))
            i += 1
        df.at[tr.stats.station, tr.id[-1]] = np.min(x)
    df = df.assign(V3=np.sqrt(df.E**2. + df.N**2. + df.Z**2.))
    return df


def get_windowed_maxabs2(st, window_length, step, offset=0):
    """
    Get the lowest max amplitude of a series of time windows for each trace.
    
    Return DataFrame, one row per station, four columns E, N, Z, V3.
    Go over each trace in sliding time windows, keep the max abs amplitude,
    return the lowest per trace, i.e. possible noise floor.
    V3 is the vector length of all 3 components.

    Parameters
    ----------
    st : obspy.stream
        DESCRIPTION.
    window_length : TYPE
        DESCRIPTION.
    step : TYPE
        DESCRIPTION.
    offset : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    df : pandas.DataFrame
        DataFrame, one row per station, four columns E, N, Z, V3.
    """
    import pandas as pd
    import numpy as np
    df = pd.DataFrame(index=u_obs.station_stream_list(st),
                      columns=['E','N','Z'],
                      dtype=np.float)
    for tr in st:
        npts = tr.stats.npts
        noff = int(offset*tr.stats.sampling_rate)
        nstep = int(step*tr.stats.sampling_rate)
        nwin = int(window_length*tr.stats.sampling_rate)
        nw = int((npts-noff-nwin)/nstep) + 1
        x = np.inf
        i1 = noff - 1
        i2 = noff - 1 + nwin
        for i in range(nw):
            x = np.min([x, np.max(np.abs(tr.data[i1+(i*nstep):i2+(i*nstep)]))])
        df.at[tr.stats.station, tr.id[-1]] = x
    df = df.assign(V3=np.sqrt(df.E**2. + df.N**2. + df.Z**2.))
    return df


def get_windowed_maxabs3(st, window_length, step, offset=0):
    """
    Get the lowest max amplitude of a series of time windows for each trace.
    
    Return DataFrame, one row per station, four columns E, N, Z, V3.
    Go over each trace in sliding time windows, keep the max abs amplitude,
    return the lowest per trace, i.e. possible noise floor.
    V3 is the vector length of all 3 components.

    Parameters
    ----------
    st : obspy.stream
        DESCRIPTION.
    window_length : TYPE
        DESCRIPTION.
    step : TYPE
        DESCRIPTION.
    offset : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    df : pandas.DataFrame
        DataFrame, one row per station, four columns E, N, Z, V3.
    """
    import pandas as pd
    import numpy as np
    df = pd.DataFrame(index=u_obs.station_stream_list(st),
                      columns=['E','N','Z'],
                      dtype=np.float)
    for tr in st:
        npts = tr.stats.npts
        noff = int(offset*tr.stats.sampling_rate)
        nstep = int(step*tr.stats.sampling_rate)
        nwin = int(window_length*tr.stats.sampling_rate)
        nw = int((npts-noff-nwin)/nstep) + 1
        i1 = noff - 1
        i2 = noff - 1 + nwin
        x = [np.max(np.abs(
                tr.data[i1+(i*nstep):i2+(i*nstep)])) for i in range(nw)]
        df.at[tr.stats.station, tr.id[-1]] = np.min(x)
    df = df.assign(V3=np.sqrt(df.E**2. + df.N**2. + df.Z**2.))
    return df


#%%
dfZ = get_windowed_maxabs3(
            stflt, window_length=20., step=10., offset=5.)
"""
# Windowed analysis 1
tt = []
for i in range(5):
    time1 = timer()
    dfX = get_windowed_maxabs_naive(
            stflt, window_length=20., step=10., offset=5.)
    time2 = timer()
    tt.append(time2 - time1)
print('# Took {:6.3f} s (average of 5)'.format(np.mean(tt)))
# takes 1.490 s on average
# Profile: get_window_times and amax are fast, slide takes 50 times as much t

# Windowed analysis 2
tt = []
for i in range(5):
    time1 = timer()
    dfY = get_windowed_maxabs2(
            stflt, window_length=20., step=10., offset=5.)
    time2 = timer()
    tt.append(time2 - time1)
maxreldif = np.max(np.abs((dfX.V3/dfY.V3))-1.)
print('# Took {:6.3f} s (average of 5)'.format(np.mean(tt)))
print('# Max relative difference: {}'.format(maxreldif))
# takes 0.063 s on average (23.6 times faster than method 1)
# no longer use slide/slice/trim, but direct address of data arrays

# Windowed analysis 3
tt = []
for i in range(5):
    time1 = timer()
    dfZ = get_windowed_maxabs3(
            stflt, window_length=20., step=10., offset=5.)
    time2 = timer()
    tt.append(time2 - time1)
maxreldif = np.max(np.abs((dfX.V3/dfZ.V3))-1.)
print('# Took {:6.3f} s (average of 5)'.format(np.mean(tt)))
print('# Max relative difference: {}'.format(maxreldif))
# takes 0.034 s on average (43.8 times faster than method 1)
# same as method 2, but use list comprehension instead of for loop
"""
