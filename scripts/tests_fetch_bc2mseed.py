#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 14:47:30 2020

@author: ts

Extract BC waveform data for given time/stations and save as mseed.
Also save BC header lines in extra file.
"""
import os
from datetime import datetime
from shiver import u_obs
from shiver import u_SIL

start = '2020-07-19 23:30:42'
end = '2020-07-19 23:45:42'
stations = ['kri', 'lfe', 'vog']
code_dct = {'network': 'VI'}
path_BC = '/mnt/sil/bc'

# Set output directory name:
#mseed_dir = '/mnt/M2_1/DATA/VI/project/' + station.upper() + '/mseed'
dt_start = datetime.strptime(start, '%Y-%m-%d %H:%M:%S')
s_start = dt_start.strftime('%Y%m%d%H%M%S')
mseed_dir = '/mnt/M2_1/DATA/VI/project/' + s_start + '/mseed'

# Get, convert, save the data:
st = u_SIL.read_bc_by_time(start, end, stations,
                           codes=code_dct, path_BC=path_BC)
os.makedirs(mseed_dir, exist_ok=True)
u_obs.save_mseed(mseed_dir, st, lbl="", force=True, encoding='STEIM2')
u_SIL.save_bc_headers(st, mseed_dir + '/bc_headers.csv')
