#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  5 14:54:13 2020

@author: ts

Find points in polygon.
Methods:
    matplotlib.path.Path(polygon).contains_points(points)
        time: 2.2e-05 s, 8.0e-05 s, 6e-04 s, 6e-03 s
    shapely.geometry / [pt.within(shp_polygon) for pt in shp_points]
        time: 1.2e-03 s, 1.1e-02 s, 0.1 s, 1.0 s
    geopandas (uses shapely) / gpd.sjoin(gd_pts, gpx, op='within')
        time: 1.2e-02 s, 1.5e-02 s, 4e-02 s, 0.34 s
Times are for 100, 1000, 10000 and 1e5 points versus 6 point polygon.
The methods of shapely/geopandas have a lot of overhead in creation or
setup, which is not counted here, but it is significant.
The matplotlib method is by far the fastest, it scales well and has a fast
initialization. It assumes Cartesian data only.
"""

from timeit import default_timer as timer
import matplotlib.path as mpath
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np

PLOT_IT = False

# make matplotlib.path with numpy array of polygon points,
# will automatically close path when testing for points,
# such that first and last points get connected
polygon = np.array([[2.5, 1.2],
                    [4.0, 3.0],
                    [4.0, 4.5],
                    [2.8, 4.7],
                    [2.0, 3.5],
                    [1.5, 1.8]])
path = mpath.Path(polygon)

# test if one specific point inside polygon
point = (3.0, 3.0)
b = path.contains_point(point)
print('{} is in polygon: {}'.format(point, b))

# now do it for many points:
# make random points within a box
n = 100000  # number of points to make
xxyy = [1., 4.5, 1., 5.]  # xmin, xmax, ymin, ymax
points = np.random.uniform(size=(n, 2))
points[:, 0] = points[:, 0]*(xxyy[1]-xxyy[0]) + xxyy[0]
points[:, 1] = points[:, 1]*(xxyy[3]-xxyy[2]) + xxyy[2]
# test whether inside:
t1 = timer()
b = path.contains_points(points)
t2 = timer()
print(t2 - t1)

# plot everything
if PLOT_IT:
    fig, ax = plt.subplots(figsize=(8, 6), dpi=200)
    patch = mpatches.PathPatch(path, facecolor='b', alpha=0.4)
    ax.add_patch(patch)
    x, y = zip(*path.vertices)
    ax.plot(x, y, 'go')
    # plot points in two colors depending on if in or out:
    ax.plot(points[b, 0], points[b, 1], 'ro')
    ax.plot(points[~b, 0], points[~b, 1], 'ko')
    ax.grid()
    ax.axis('equal')
    outfile = '../out/polygon_test.png'
    fig.savefig(outfile, bbox_inches="tight")
    plt.close()

from shapely.geometry import Point, Polygon, MultiPoint
shp_points = MultiPoint([Point(i, j) for i, j in points])
shp_polygon = Polygon(polygon)
t1 = timer()
bb = [pt.within(shp_polygon) for pt in shp_points]
t2 = timer()
print(t2 - t1)


#from geopandas.geoseries import Point as gPoint
#from geopandas.geoseries import Polygon as gPolygon
#from geopandas.geoseries import GeoSeries
import pandas as pd
import geopandas as gpd
df = pd.DataFrame(points, columns=['longitude', 'latitude'])
gd_pts = gpd.GeoDataFrame(df,
                          geometry=gpd.points_from_xy(
                              df.longitude, df.latitude))
gd_pts.sindex  # init spatial index, required to make sjoin() fast,
# however if done for polygon it will be slower...
gpx = gpd.GeoDataFrame()
gpx['geometry'] = None
gpx.loc[0, 'geometry'] = shp_polygon
# gd_pts.crs = {'init': 'epsg:4674'}
# gpx.crs = {'init': 'epsg:4674'}
t1 = timer()
# output not a bool array, but a dataframe with the inside points:
within_points = gpd.sjoin(gd_pts, gpx, op='within')
t2 = timer()
print(t2 - t1)
