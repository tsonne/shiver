#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 12:06:28 2020

@author: ts

Process multiple stations for multiple events

* get event parameters from SIL catalog (event.fps files or saved dataframe)
* set fixed time window around event origin for waveforms
* get waveforms from BC files
    * detail: assumes path: {path_BC}/%Y/%b/%d/{station}/{BC_files}
    * detail: only tries to find certain list of stations!
* correct waveforms using BC headers and SIL resp files
    * option: can also use station XML response info
    * detail: pre-filter set at [0.07, 0.15, 35., 45.] for all stations
* bandpass filter copy of stream [1., 35.]
* create and save PGN image of stream (raw, corrected, filtered)
* check for clipped traces
* get PGV of entire time window (both filter streams)
* get distance, Mpgv using event, station locations and PGV values
* create and save plots of PGV, Mpgv vs Repi
* create and save plots of tPGV vs Repi
* write pgv, r, Mpgv to text file
"""
import os
import matplotlib
import subprocess
import numpy as np
from timeit import default_timer as timer
from obspy import UTCDateTime

from shiver import u_io
from shiver import u_SIL
from shiver import u_obs

matplotlib.use("ps")  # 'ps' might be fastest backend for static plots
time_run_start = timer()

# CONFIGURATION
#
# GET INPUT SETTINGS:
conf = u_io.parse_yaml_conf('../config/SIL_msme_conf_3.yml')
# INPUT/OUTPUT DIRS
inv_dir = conf['paths']['inventory']
etc_dir = conf['paths']['sil_etc']
out_dir = conf['paths']['outdir']
# STATIONS
stations = conf['stations']['codes']  # and handle string and '_all' cases...
stations = u_SIL.get_station_codes(stations, use_etc=True, path=etc_dir)
sta_tab = u_SIL.get_SIL_station_table_df(path_etc=etc_dir)
sta_tab = sta_tab.loc[stations]  # sta, lat, lon, elev
# BC PREPROC INPUT
path_BC = conf['paths']['BC_in']
code_dct = {'network': conf['stations']['network']}
# FLAGS
PLOT_TRACES = conf['action']['plot_traces']
RESP_XML = conf['response_xml']

# Use alternative method to save obspy stream plots (through PIL),
# here setup to save 1bit BW files, compress level 1 (fast, but ugly):
USE_OBSPY_PIL = True

# IF USING SIL CAT ARCHIVE FILE, UPDATE IT FIRST BEFORE GETTING EVENTS
if conf['catalog']['SIL_CAT_file']:
    u_SIL.update_default_silcat_file()

# GET SIL CATALOG AND SELECT SOME EVENTS OF INTEREST,
# USE ZIPPED PICKLED SIL CAT FILE:
X = u_SIL.silcat_by_conf(conf['catalog'])

# PRINT INFO ABOUT LARGEST EVENTS AND PICKED POLARITIES:
u_SIL.print_some_info_SILCAT(X, verbose=True)

# DETERMINE TIME WINDOWS OF INTEREST FOR EACH EVENT:
dt_pre = 20  # int, seconds before origin time for window start
dt_post = 150  # int, seconds after  origin time for window end
XX = X.assign(t_start=(X.index - np.timedelta64(dt_pre, 's')),
              t_end=(X.index + np.timedelta64(dt_post, 's')))

# LOOP THROUGH EVENTS, PROCESS ALL AVAILABLE STATIONS
COMP = 'V3'  # PGV convention: V3 = vector length of all 3 comp.
SDT_FMT = '%Y%m%d%H%M%S'
SDT_FMT2 = '%Y-%m-%d %H:%M:%S'
TITL_FMT = r'{} BP({}, {}) $\mathregular{{M_{{pgv}}}}$ = {:4.2f}'
fbb = [1., 35.]
SPC_DCT = {'estop': -1, 'tspre': 0.5, 'tpost': 5.5, 'tnoise': 10.}
pre_filt = [0.07, 0.15, 35., 45.]
mark_stations = ['ada']
cnt = 0
for Ev in XX.itertuples():
    cnt += 1
    if cnt < 0:  # debug
        continue
    if cnt > 2:  # debug
        break
    print('# {:03d}: {}'.format(cnt, Ev.Index))

    t_origin = UTCDateTime(Ev.Index)

    # GET WAVEFORMS
    time_bc1 = timer()
    st0 = u_SIL.read_bc_by_time(Ev.t_start, Ev.t_end, stations,
                                codes=code_dct, path_BC=path_BC)
    time_bc2 = timer()
    if not st0:
        print('# Found no data to process!!! SKIP')
        continue
    sta_codes = u_obs.station_stream_list(st0)
    print('# Got {:.0f} stations, took {:6.3f} s'.format(
        len(sta_codes), time_bc2 - time_bc1))

    # MERGE GAPS, PROCEED WITH ALL:
    st = st0.copy()
    st.merge()
    st.sort()

    # REMOVE RESPONSE,
    # prefilter, return velocity in m/s
    print('# Remove response...')
    if RESP_XML:
        # remove response using seiscomp station XML
        stcor, inv = u_obs.remove_resp_xml(st, pre_filt, inv_dir)
    else:
        # remove response using SIL etc files
        stcor = u_SIL.remove_resp_sil(st, pre_filt, etc_dir)
    print('# Removed response.')

    # BANDPASS FILTER corrected data
    stflt = stcor.copy()
    stflt.taper(0.01)
    stflt.filter('bandpass', freqmin=fbb[0], freqmax=fbb[1])

    # HIGHER F FILTER
    stfl2 = stcor.copy()
    stfl2.taper(0.01)
    stfl2.filter('bandpass', freqmin=2.0, freqmax=30.)

    # AUTO P/S PICK
    # dfPick = u_obs.stream_pick(stfl2, flo=2., fhi=30.)

    # CREATE EVENT SUBDIR
    ev_ots = Ev.Index.strftime(SDT_FMT)  # event id str = origin time
    ev_dir = os.path.join(out_dir, ev_ots)  # subdir path for this event
    os.makedirs(ev_dir, exist_ok=True)  # create dir if not exist

    # PLOT ALL TRACES, SAVE COMBINED FILE
    if PLOT_TRACES:
        print('# Create trace figures...')
        if USE_OBSPY_PIL:
            figpath = os.path.join(ev_dir, 'raw.png')
            u_obs.save_obspy_PIL(fpng=figpath, st=st)
            figpath = os.path.join(ev_dir, 'cor.png')
            u_obs.save_obspy_PIL(fpng=figpath, st=stcor)
            figpath = os.path.join(ev_dir, 'flt.png')
            u_obs.save_obspy_PIL(fpng=figpath, st=stflt)
            cmd1 = """
            cd {}
            convert raw.png cor.png flt.png +append all_rawcorflt.png
            /bin/rm raw.png cor.png flt.png
            """.format(ev_dir)
            """
            cd {}
            convert raw.png cor.png flt.png +append x_rawcorflt.png
            convert -flatten +dither x_rawcorflt.png \
                -depth 2 -colors 8 -type palette all_rawcorflt.png
            /bin/rm raw.png cor.png flt.png x_rawcorflt.png
            """
        else:
            figpath = os.path.join(ev_dir, 'raw.png')
            st.plot(equal_scale=False, outfile=figpath)
            figpath = os.path.join(ev_dir, 'cor.png')
            stcor.plot(equal_scale=False, outfile=figpath)
            figpath = os.path.join(ev_dir, 'flt.png')
            stflt.plot(equal_scale=False, outfile=figpath)
            cmd1 = """
            cd {}
            for f in raw.png cor.png flt.png; do
            convert -flatten +dither $f -depth 2 -colors 8 -type palette x$f
            done
            convert xraw.png xcor.png xflt.png +append all_rawcorflt.png
            /bin/rm raw.png cor.png flt.png xraw.png xcor.png xflt.png
            """.format(ev_dir)
        subprocess.run(cmd1, shell=True)
        print('# Saved trace figures.')

    # IDENTIFY CLIPPED STATIONS:
    clip_id_list = u_obs.check_clipped(st, THR_ABS=3e6)
    clip_sta = u_obs.sorted_unique(u_obs.station_from_id(clip_id_list))

    print('# Get Mpgv, save plots...')
    # get horizontal peak ground velocity
    dfPGVTcor = u_obs.get_pgv_time(stcor, comp=COMP)
    dfPGVTflt = u_obs.get_pgv_time(stflt, comp=COMP)

    # GET EPICENTRAL DISTANCES:
    src_crd = [Ev.latitude, Ev.longitude]
    sta_df = sta_tab.loc[sta_codes]
    LL = sta_df[['lat', 'lon']].values.tolist()
    DD = u_obs.get_distances(src_crd, LL)

    # store lat, lon, Repi, unfiltered PGV and tPGV, isclipped
    dfFULL = dfPGVTflt.rename(columns={COMP: 'PGVflt',
                                       'pgvtime': 'tPGVflt'})
    dfFULL = dfFULL.assign(lat=sta_df['lat'], lon=sta_df['lon'],
                           repi=DD, PGVnof=dfPGVTcor[COMP],
                           tPGVnof=dfPGVTcor['pgvtime'],
                           clip=[x in clip_sta for x in dfFULL.index])

    # get Mpgv:
    Mflt = [u_obs.m_by_pgv_r_C(x, y) for x, y in zip(dfFULL['PGVflt'],
                                                     dfFULL['repi'])]
    Mnof = [u_obs.m_by_pgv_r_C(x, y) for x, y in zip(dfFULL['PGVnof'],
                                                     dfFULL['repi'])]
    dfFULL = dfFULL.assign(Mflt=Mflt)  # filtered vel
    dfFULL = dfFULL.assign(Mnof=Mnof)  # unfiltered (only response corrected)

    # create and save plots of PGV, Mpgv vs Repi,
    # using median Mpgv of stations less than 200 km and not clipped,
    # mark apparent outliers and any clipped stations in plot:
    s_origin = Ev.Index.strftime(SDT_FMT2)
    df_M200 = dfFULL.query("repi < 200 and not clip")
    Mmed = df_M200['Mnof'].median()
    figpath = os.path.join(ev_dir, 'PGVnoflt_R.png')
    title = TITL_FMT.format(s_origin, *pre_filt[1:3], Mmed)
    u_obs.plot_pgv_r(dfFULL.assign(pgv=dfFULL['PGVnof']), outfile=figpath,
                     M=Mmed, title=title, label_outliers=True,
                     label_clipped=True, label_index=mark_stations)
    # same for higher filterband:
    Mmed = df_M200['Mflt'].median()
    figpath = os.path.join(ev_dir, 'PGVflt_R.png')
    title = TITL_FMT.format(s_origin, *fbb, Mmed)
    u_obs.plot_pgv_r(dfFULL.assign(pgv=dfFULL['PGVflt']), outfile=figpath,
                     M=Mmed, title=title, label_outliers=True,
                     label_clipped=True, label_index=mark_stations)

    # plot distance, PGVtraveltime
    figpath = os.path.join(ev_dir, ev_ots + '_r_tpgvnof.png')
    u_obs.plot_var_reltime(dfFULL['repi'], dfFULL['tPGVnof'],
                           originTime=t_origin,
                           outfile=figpath)

    # write pgv, r, Mpgv to text file:
    w2_path = os.path.join(ev_dir, ev_ots + '_parameter.dat')
    with open(w2_path, 'w') as f:
        f.write(dfFULL.to_string(columns=['lat', 'lon', 'repi', 'clip',
                                          'PGVflt', 'tPGVflt', 'Mflt',
                                          'PGVnof', 'tPGVnof', 'Mnof'],
                                 float_format='{:.4e}'.format))

# Print elapsed runtime:
print('# Done.')
time_run_end = timer()
print('# Elapsed time: {} s'.format(time_run_end - time_run_start))
