#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 16 16:07:49 2020

@author: ts

# just testing a impulsive signals,
# strong response correction artefacts
#
# very impulsive signals have a flat spectrum,
# the high spectral amplitude at very low frequencies gets magnified
# too much through response correction (deconvolution filter),
# the only way to remove strong filter artefacts is to highpass filter
# at the instrument corner or even one octave higher, at the cost of
# the signal content.
"""

import os
from datetime import datetime
from shiver import u_obs
from shiver import u_SIL
from obspy import UTCDateTime
import matplotlib as mpl
mpl.use('Qt5Agg')  # make plots interactive

# Input values
dir_ev = '20200719233042'
stations =  ['kri', 'lfe', 'vog']
dir_mseed = '/mnt/M2_1/DATA/VI/project/' + dir_ev + '/mseed'
#pre_filt = [0.025, 0.05, 35., 45.]
#pre_filt = [0.25, 0.5, 35., 45.]
#pre_filt = [0.1, 0.2, 35., 45.]
pre_filt = [0.1, 1.0, 35., 45.]
etc_dir = '../dat/sil/etc/'

# Get mseed data with SIL BC headers
st0 = u_SIL.read_mseed_bc(stations=stations, pattern=(dir_mseed + '/*msd'))
if not st0: raise RuntimeError('No data for given specs!')
st = st0.copy()

# show me
st.plot()

# Remove response
stcor = u_SIL.remove_resp_sil(st, pre_filt, etc_dir)
stcor.plot()

# filter
stcor.taper(0.01)
stcor.filter('bandpass', freqmin=0.4, freqmax=15.)
stcor.plot()

# Cut stream to 30 sec before P-arrival, 2 min after P
# P at 23:36:15
s_tP = '20200719233615'
t_pre = 5.
t_post = 35.
u_tP = UTCDateTime(s_tP)
st_cut = st.slice(starttime=u_tP-t_pre, endtime=u_tP+t_post)

# plot spectra
fig = u_obs.plot_fft(st_cut)

# Remove response
stc2 = u_SIL.remove_resp_sil(st_cut, pre_filt, etc_dir)
stc2.plot()

# filter
stc2.taper(0.01)
stc2.filter('bandpass', freqmin=0.5, freqmax=15.)
stc2.plot()
