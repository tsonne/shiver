#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 14:49:25 2020

@author: ts

PURPOSE: Get waveform data by FDSN client, plot

Details: Event in Greenland 
"""

%matplotlib auto
from obspy.clients.fdsn import Client
from obspy import UTCDateTime

# Initialize client, get specific piece of data and plot
client = Client('IRIS')
t_orig = UTCDateTime('20201003132247')
(t_pre, t_post) = (0, 240)
t_start = t_orig + t_pre
t_end = t_orig + t_post
network = 'DK'
station = 'SCO'
st = client.get_waveforms(network, station, '*', 'BHZ', t_start, t_end,
                          attach_response=True)
st.plot()

# get station metadata
inventory = client.get_stations(network=network, station=station,
                                starttime=t_start,
                                endtime=t_end,
                                level="response")

# plot response for one station, all channels
# (needs this extra code to change figure size)
# (basic plot is too small from inventory.plot_response())
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(8,6), dpi=200)
ax1 = fig.add_subplot(211)
ax2 = fig.add_subplot(212, sharex=ax1)
fig = inventory.plot_response(min_freq=0.01, station=station,
                        axes=(ax1,ax2))
#outfile=InventoryPath+Name+"_GEC2_RESP.png"
from obspy.core.inventory.response import _adjust_bode_plot_figure
_adjust_bode_plot_figure(fig, plot_degrees=False, show=False)
#fig.savefig(outfile, bbox_inches = "tight")
#plt.close()

# Get some data based on event search by catalog
t_start = UTCDateTime('20201001')
t_end = UTCDateTime('20201004')
cat = client.get_events(starttime=t_start, endtime=t_end,
                        minlatitude=-90, maxlatitude=90,
                        minlongitude=-180, maxlongitude=180,
                        minmagnitude=6.0, orderby='magnitude')
elon = cat[0].origins[0].longitude  # deg
elat = cat[0].origins[0].latitude  # deg
edep = cat[0].origins[0].depth  # meters
t_or = cat[0].origins[0].time  # UTCDateTime


# Get some data based on event search by catalog
"""
t_start = UTCDateTime('20201001')
t_end = UTCDateTime('20201004')
client2 = Client('ISC')
cat = client2.get_events(starttime=t_start, endtime=t_end,
                         minlatitude=-90, maxlatitude=90,
                         minlongitude=-180, maxlongitude=180,
                         minmagnitude=6.0, orderby='magnitude')
"""
