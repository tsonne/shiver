#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  5 18:13:59 2020

@author: ts

Read mseed files and bc header list file combined,
get stream with bc header info.
"""
from shiver import u_SIL

mseed_dir = '/mnt/M2_1/DATA/VI/project/adv_msme/20200105043250/mseed'
kw = {'start': None,
      'end': None,
      'stations': None,
      'pattern': (mseed_dir + '/*msd')}

st = u_SIL.read_mseed_bc(**kw)