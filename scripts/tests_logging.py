#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 13:17:04 2020

@author: ts
"""

import logging

logging.basicConfig()
module_logger = logging.getLogger(__name__)

module_logger.setLevel(logging.DEBUG)
module_logger.debug('logger initialized')
print(module_logger.hasHandlers())
print(module_logger.getEffectiveLevel())
print(module_logger.isEnabledFor(logging.DEBUG))
print(module_logger.isEnabledFor(logging.INFO))
print(module_logger.isEnabledFor(logging.WARNING))

module_logger.debug('debug msg')
module_logger.info('info msg')
module_logger.warning('warning msg')
module_logger.error('error msg')
