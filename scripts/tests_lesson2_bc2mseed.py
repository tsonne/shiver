#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 08:49:44 2020

@author: ts

Get BC waveforms, save as MSEED files
"""

import os
from shiver import u_SIL
from datetime import datetime
from shiver import u_obs

start = '2020-06-21 19:05:52'
end = '2020-06-21 19:25:52'
stations = ['ada' , 'bjk', 'mjo']
code_dct = {'network': 'VI'}
path_BC = '../dat/sil/bc'

# get the BC data:
st = u_SIL.read_bc_by_time(start, end, stations,
                           codes=code_dct, path_BC=path_BC)

# make output directory for mseed files
dt_start = datetime.strptime(start, '%Y-%m-%d %H:%M:%S')
s_start = dt_start.strftime('%Y%m%d%H%M%S')
mseed_dir = '../dat/sil/mseed/' + s_start
os.makedirs(mseed_dir, exist_ok=True)

# save stream as mseed, also save BC headers as text file
u_obs.save_mseed(mseed_dir, st, lbl="", force=True, encoding='STEIM2')
u_SIL.save_bc_headers(st, mseed_dir + '/bc_headers.csv')
