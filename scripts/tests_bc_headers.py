#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 13:33:42 2020

@author: ts

Write list of all SIL stations and their instruments based on last bc header.
"""
import os
import glob
import pandas as pd
from datetime import datetime, timedelta
import u_SIL
d_siletc = '../dat/sil/etc/'
path_BC = '/mnt/sil/bc'
Y = u_SIL.get_SIL_station_table_df(path_etc=d_siletc)
Y['sensor'] = None
Y['digitizer'] = None
Y['last_sample'] = None
day = datetime.now()  # start date
stopdate = datetime.strptime("2015-01-01", "%Y-%m-%d")  # do not go further
b = pd.isnull(Y['sensor'])  # bool array of stations without sensor info
while any(b) and day > stopdate:  # as long as sensor info is missing
    if day.day == 1:  # print date on every first day of month
        print('# {}'.format(day.date()))
    missing = Y[b].index.values.tolist()  # list of missing stations
    path = day.strftime('%Y/%b/%d/').lower()  # get date-based path part
    daydir = os.path.join(path_BC, path)  # get full path to bc day dir
    dlistfull = glob.glob(daydir + '*')  # list of station dirs
    dlist = [os.path.basename(i) for i in dlistfull]
    for sta in missing:
        if sta in dlist:  # if missing station found in current day dir
            flist = glob.glob(daydir + sta + '/*')
            if not flist:  # dir can be empty, skip
                continue
            lastf = max(flist, key=os.path.getctime)  # most recent BC file
            st0 = u_SIL.get_bc_stream(lastf)  # stream to get last sample time
            hdr = st0[0].stats.bc_header
            Y.at[sta, ['sensor', 'digitizer']] = hdr[0].split()
            Y.at[sta, 'last_sample'] = st0[0].stats.endtime
            print('# Found station {}: {}, {}, {}'.format(
                sta, *Y.loc[sta][['sensor', 'digitizer', 'last_sample']].
                tolist()))
    b = pd.isnull(Y['sensor'])
    day -= timedelta(days=1)  # go back one day
day += timedelta(days=1)
print('#')
print('# Last checked date: {}'.format(day.date()))
if any(b):
    missing = Y[b].index.values.tolist()
    print('# Missing stations: {}'.format(missing))
else:
    print('# No missing stations.')
fn = '../out/station_table.txt'
Y.sort_index(inplace=True)
Y.to_string(fn, index_names=False, header=['lat', 'lon', 'elev',
                                           'sensor', 'adc', 'last_sample'])
print('# Done.')
