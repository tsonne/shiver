#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 10:55:21 2020

@author: ts

Try out storing and reading parquet files from/to pandas.DataFrame
using different compression types.
TYPE SIZE TIME
PARQUET SNAPPY        |  58.4M  |   2.3 s
PARQUET GZIP          |  34.4M  |  10.4 s
PARQUET ZSTD          |  34.8M  |   2.7 s
PARQUET LZO           |  56.8M  |   2.5 s
PARQUET LZ4           |  65.0M  |   2.2 s
PARQUET UNCOMPRESSED  | 161.6M  |   1.9 s
PARQUET BROTLI        |  26.8M  | 329.0 s
PICKLE UNCOMPRESSED   | 124.6M  |   1.0 s
PICKLE GZIP           |  33.0M  |  45.5 s
PICKLE BZ2            |  26.8M  |  17.5 s
PICKLE XZ             |  24.0M  |  65.7 s

For the example data (pandas dataframe with ~0.5M rows, 32 cols),
All PICKLE COMPRESSIONS are much slower than PARQUET COMPRESSIONS, except
for PRQ BROTLI, which is far too slow, so maybe bugged...?
FASTEST is PICKLE UNCOMPRESSED (also second largest file),
BEST COMPRESSION is PICKLE XZ (also second slowest process),
FAST AND SMALL seems to be PARQUET ZSTD.
--> use PARQUET ZSTD as best compromise.
"""

from timeit import default_timer as timer
import pandas as pd
import os

def print_df_info(X):
    print("Size of dataframe: {}".format(X.shape))
    print("Total number of elements: {}".format(X.size))
    print("Number of earthquakes: {}".format(X.shape[0]))
    print("Number of columns: {}".format(X.shape[1]))
    #print("Column names: \n{}".format(list(X.columns.values)))
    print("First entry: \n{}".format(X.index[0]))
    print("Last entry: \n{}".format(X.index[-1]))
    return None

def run_all(IO_dir, X, L):
    for C in L:
        t1 = timer()
        out_file = os.path.join(IO_dir, 'events_19900101_20200225' + C[0])
        # WRITE, READ:
        if C[3] == 'PRQ':
            X.to_parquet(out_file, compression=C[1])
            Y = pd.read_parquet(out_file)
        elif C[3] == 'PKL':
            X.to_pickle(out_file, compression=C[1])
            Y = pd.read_pickle(out_file)
        else:
            print('#### UNKOWN TYPE!!!!!!! #####')
            continue
        t2 = timer()
        print('########################### {} ###'.format(C[2]))
        print(t2 - t1)
        print_df_info(Y)
    return None


IO_dir = '/home/ts/Documents/IMO/PROJECT/Seismology/out/sil_cat'
SIL_CAT_file = os.path.join(IO_dir, 'events_19900101_20200225.pkl.gz')
X = pd.read_pickle(SIL_CAT_file)
# PRINT SOME PANDAS DATAFRAME INFO:
# print_df_info(X)

L = [
     ['.prq.snp',      'snappy',       'PRQ SNP', 'PRQ'],
     ['.prq.gz',       'gzip',         'PRQ GZP', 'PRQ'],
     ['.prq.zst',      'zstd',         'PRQ zst', 'PRQ'],
     ['.prq.lzo',      'lzo',       'PRQ LZO', 'PRQ'],
     ['.prq.lz4',      'lz4',       'PRQ LZ4', 'PRQ']
     ['.prq',          'uncompressed', 'PRQ UNC', 'PRQ'],
     ['_test.pkl',     None,           'PKL UNC', 'PKL'],
     ['_test.pkl.gz',  'gzip',         'PKL GZP', 'PKL'],
     ['_test.pkl.bz2', 'bz2',          'PKL BZ2', 'PKL'],
     ['_test.pkl.xz',  'xz',           'PKL XZ',  'PKL']]
run_all(IO_dir, X, L)

"""
# WRITE TO PARQUET FILE: BROTLI (EXTREMELY SLOW, BUGGED?!)
t1 = timer()
out_file = os.path.join(IO_dir, 'events_19900101_20200225.prq.br')
X.to_parquet(out_file, compression='BROTLI')
# READ FROM PARQUET FILE
X_5 = pd.read_parquet(out_file)
t2 = timer()
print('########################### PRQ BRT ###')
print(t2 - t1)
print_df_info(X_5)
"""
