#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 11:50:58 2020

@author: ts
"""

from geo_dataread.sil_read import find_alert

event_id = '/2020/jun/26/13:50:00/55:14:897'
X = find_alert(event_id,
               sil_path="/mnt/sk2",
               pre_path="/tmp",
               eps=30, logger=__name__)
print(X)
print(X.MW_pgv.median())

import csv
infile = '/tmp/2020/jun/26/089.event'
f = open(infile, 'r')
reader = csv.DictReader(f)
f.close() 