#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 11:13:01 2020

@author: ts

Test plot function speed:
    obspy vs naive implementation
    use PIL vs not use PIL
    use default backend 'Qt5Agg' vs use 'ps'

Result:
    matplotlib without decimation is very slow,
    matplotlib with decimation is a bit faster than obspy or same,
    however not handling gaps or different start/end times yet,
    Fastest backend 'ps', almost twice as fast as 'Qt5Agg',
    obspy vs mpl/decim have larger time diff if 'Qt5Agg' instead of 'ps',
    obspy AND saving through PIL: quality/handling advantage AND good time,
    saving PNG through PIL is significantly faster than matplotlib,
    but strongly depends on settings,
    PNG compression=1 is very fast,
    high compression=9 is slower,
    compression=0 (none at all) creates 12 times bigger files than compr=1,
    compression=0 takes a bit longer than c=1 (probably file writing process),
    saving more colors is slower (PIL modes 'RGB', 'L', 'P'),
    1bit PNG size of compression 9 is 68% of compression 1,
    using dither on 1bit makes traces look worse,
    dither increases file size by 20% - 50% (high to low compression),
    dithering increases runtime,
    fastest is 1bit image (PIL mode='1') at compression=1 and no dither
"""
import os
from shiver import u_SIL
from shiver import u_obs
from timeit import default_timer as timer
import matplotlib
matplotlib.use("ps")  # 'ps' is fastest backend for static plots?!
matplotlib.rcParams['interactive'] = False


def my_decim(data, dec_factor):
    """
    Decimate array, return min and max of each decimated sample.

    Do not fill anything after last decimated sample,
    e.g. if n=24, dec_factor=5, then get 4 min,max samples and ignore samples
    21 to 24. The returned values are two arrays giving the minimum and maximum
    values of each decimation interval, instead of returning just every k-th
    value.

    Parameters
    ----------
    data : numpy.array
        1D array of any length.
    dec_factor : int
        Decimation factor.

    Returns
    -------
    tuple of two numpy.array
        First is array of min values for each decimation sample, second is
        array of max values for each decimation sample.

    """
    import numpy as np
    k = (len(data)//dec_factor)*dec_factor
    return (np.nanmin(data[:k].reshape(-1, dec_factor), axis=1),
            np.nanmax(data[:k].reshape(-1, dec_factor), axis=1))


def my_streamplot(st, fpng):
    """
    Make stream plot, exact pixel size for each trace, decimating.

    It is a bit faster than obspy plot, but this method does not account for
    gaps or different time ranges in the data.
    The traces are expected to all cover the same range without any gaps!
    Also missing: title, absolute time reference in x-axis tick label.
    Note: Grid lines cost a lot of time (like 0.8 s for 9 traces),
    use fill_between instead of lines to show extrema of downsampled traces,
    the intelligence behind the obspy plot method is quite advanced for
    handling gaps and different time ranges as well as giving good performance.
    Matplotlib auto axis handling probably costs a lot of time.
    Using sharex=True in subplots() is slower than setting x-axis limits and
    xaxis_date() for each axis.
    Plotting the actual traces (fill_between) takes about 20% of the whole
    operation.
    Saving to file, fig.savefig(fpng), takes about 80% of the process!!

    Fastest backend is 'ps', if it's 100% time, then the default 'Qt5Agg'
    takes 133%, 'cairo' takes 117%.

    Saving through buffer and PIL is faster than using pil_kwargs.

    Parameters
    ----------
    st : TYPE
        DESCRIPTION.
    fpng : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    import numpy as np
    #import matplotlib
    #if matplotlib.rcParams['backend'] == 'ps':
    #    dpi = 72
    #else:
    #    dpi = 100
    import matplotlib.pyplot as plt
    from matplotlib.dates import date2num
    n = len(st)
    dpi = 100  # dots per inch (figure resolution setting)
    dpph = 250  # pixels per traceplot height
    dppw = 705  # pixels per traceplot width
    pleft = 80  # pixels on left figure margin
    pright = 20  # pixels on right figure margin
    pbottom = 50  # pixels on bottom figure margin
    ptop = 70  # pixels on top figure margin
    pixh = n*dpph + pbottom + ptop  # total figure height in pixels
    pixw = dppw + pleft + pright  # total figure width in pixels
    height = pixh/dpi
    width = pixw/dpi
    fsize = (width, height)
    left = pleft/pixw
    right = 1. - pright/pixw
    bottom = pbottom/pixh
    top = 1. - ptop/pixh

    st.sort()
    t_start = st[0].stats.starttime
    t_end = st[0].stats.endtime
    t_diff = t_end - t_start
    new_samp_rate = dppw / t_diff
    t_start_mpl = date2num(t_start.datetime)
    t_end_mpl = date2num(t_end.datetime)
    times_mpl = (t_start_mpl + np.arange(dppw) / new_samp_rate / 86400.0)
    for tr in st:
        if tr.stats.starttime < t_start:
            t_start = tr.stats.starttime
        if tr.stats.endtime > t_end:
            t_end = tr.stats.endtime

    fig, axes = plt.subplots(
                n, 1, sharex=False, sharey=False, squeeze=False,
                figsize=fsize, dpi=dpi,
                gridspec_kw={'hspace': 0., 'left': left, 'right': right,
                             'top': top, 'bottom': bottom})
    # found that using mpl backend 'ps' changes dpi after fig creation
    # (figure gets smaller), so must set it again explicitly:
    fig.set_dpi(dpi)  # this costs a bit of time though
    # However, resetting width and height is not necessary
    #fig.set_figwidth(width)  # costs time again
    #fig.set_figheight(height)  # costs time again

    for i in range(0, n):
        (dmin, dmax) = my_decim(st[i].data,
                                int(st[i].stats.sampling_rate / new_samp_rate))
        axes[i][0].xaxis_date()
        axes[i][0].set_xlim([t_start_mpl, t_end_mpl])
        #axes[i][0].plot(times_mpl, dmin, 'k-', lw=.5)
        #axes[i][0].plot(times_mpl, dmax, 'k-', lw=.5)
        axes[i][0].fill_between(times_mpl, dmin, dmax, color='k', zorder=2)
        axes[i][0].text(0.01, 0.95, st[i].id,
                        horizontalalignment='left', verticalalignment='top',
                        transform=axes[i][0].transAxes)
        #axes[i][0].grid(b=True, axis='x', linestyle='--', color=(.8, .8, .8))
        #axes[i][0].grid(b=True, axis='y', linestyle='--', color=(.8, .8, .8))
    axes[n-1][0].set_xlabel('Time [s]')
    #axes[n-1][0].xaxis_date()
    #axes[n-1][0].set_xlim([t_start_mpl, t_end_mpl])
    """
    fig.savefig(fpng, format='png',
                pil_kwargs={
                    'optimize': False,
                    'compress_level': 1})
    """
    #fig.savefig(fpng, format='png')

    save_by_PIL(fig, pixw, pixh, fpng)
    plt.close()
    return None


def save_obspy_PIL(fpng, st, title=''):
    """Create obspy stream plot and save as file using pillow (PIL)."""
    #from PIL import Image
    #from io import BytesIO
    import matplotlib.pyplot as plt
    from shiver import u_obs

    n = len(st)
    dpi = 100  # dots per inch (figure resolution setting)
    dpph = 250  # pixels per traceplot height
    dppw = 1000  # pixels per traceplot width
    pleft = 80  # pixels on left figure margin, fixed in obspy to 80
    pright = 40  # pixels on right figure margin, fixed in obspy to 40
    pbottom = 40  # pixels on bottom figure margin, fixed in obspy to 40
    ptop = 60  # pixels on top figure margin, fixed in obspy to 60
    pixh = n*dpph + pbottom + ptop  # total figure height in pixels
    pixw = dppw + pleft + pright  # total figure width in pixels
    height = pixh/dpi
    width = pixw/dpi

    cfig = plt.figure(figsize=(width, height), dpi=dpi)
    cfig.set_dpi(dpi)  # need to set it again if using 'ps' backend
    st.plot(fig=cfig, equal_scale=False, size=(pixw, pixh))
    si = u_obs.get_stream_info(st)
    suptitle = '{}  -  {}'.format(str(si['start']).rstrip("Z0").rstrip("."),
                                  str(si['end']).rstrip("Z0").rstrip("."))
    cfig.suptitle(suptitle, y=((pixh-15.)/pixh), fontsize='small',
                  horizontalalignment='center')

    #cfig.axes[0].set_title(title)
    #plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, 
    #                    right=0.995, hspace=0.0, wspace=0.0)
    #plt.subplots_adjust(top=0.95, bottom=0.05, left=0.05, 
    #                    right=0.995, hspace=0.0, wspace=0.0)

    # works with qt5agg, but not ps:
    #canvas = plt.get_current_fig_manager().canvas
    #canvas.draw()
    #pil_im = Image.frombytes('RGB', canvas.get_width_height(),
    #                         canvas.tostring_rgb())

    # works correctly (if mode 'RGBA' for 'ps' and 'RGB' for else):
    """
    buf = BytesIO()
    plt.savefig(buf, format='raw')
    buf.seek(0)
    pil_im = Image.frombytes('RGBA', size=(pixw, pixh),
                             data=buf.getvalue())
    """

    # works, but no speed advantage over anything else:
    #buf = BytesIO()
    #cfig.savefig(buf)
    #buf.seek(0)
    #pil_im = Image.open(buf)

    #pil_im.convert('L').save(
    #    fpng, format='PNG', optimize=False, compress_level=1)

    # outsourced the PIL stuff to next function:
    save_by_PIL(cfig, pixw, pixh, fpng)
    plt.close()


def save_by_PIL(fig, pixw, pixh, fpng):
    """
    Save figure as PNG file using pillow (PIL) directly.

    Parameters
    ----------
    fig : matplotlib.figure.Figure
        Figure handle.
    pixw : int
        Width of figure in pixels.
    pixh : int
        Height of figure in pixels.
    fpng : str
        Output filepath.

    Returns
    -------
    None.
    """
    from PIL import Image
    from io import BytesIO
    import matplotlib.pyplot as plt
    buf = BytesIO()
    plt.savefig(buf, format='raw')
    buf.seek(0)
    pil_im = Image.frombytes('RGBA', size=(pixw, pixh),
                             data=buf.getvalue())
    #pil_im.convert('L').save(
    #    fpng, format='PNG', optimize=False, compress_level=1)
    #pil_im.convert(mode='P', palette=Image.ADAPTIVE, colors=8, dither=Image.NONE).save(
    #    fpng, format='PNG', optimize=False, compress_level=1)
    #pil_im.convert('L').save(
    #    fpng, format='PNG', optimize=True, compress_level=9)
    #pil_im.convert(mode='P', palette=Image.ADAPTIVE, colors=8, dither=Image.NONE).save(
    #    fpng, format='PNG', optimize=True, compress_level=9)
    #pil_im.convert(mode='P', palette=Image.ADAPTIVE, colors=4, dither=Image.NONE).save(
    #    fpng, format='PNG', optimize=True, compress_level=9)
    pil_im.convert(mode='1', dither=Image.NONE).save(
            fpng, format='PNG', optimize=False, compress_level=1)
    #pil_im.convert(mode='1', dither=Image.NONE).save(
    #        fpng, format='PNG', optimize=True, compress_level=9)
    #pil_im.convert(mode='1').save(
    #        fpng, format='PNG', optimize=False, compress_level=1)
    #pil_im.convert(mode='1').save(
    #        fpng, format='PNG', optimize=True, compress_level=9)
    return None


SWITCH_MATPL = False
SWITCH_DECIM = False
SWITCH_OBS_PIL = True
SWITCH_OBSPY = False

t_start = '2020-04-20 03:53:47'
t_end = '2020-04-20 03:59:47'
stations = ['dyn', 'kis', 'jok', 'sig', 'skr',
            'rju', 'ren', 'rah', 'nyl', 'lfe']
path_BC = '/mnt/sil/bc/'

out_dir = '../out/plotspeed'

st0 = u_SIL.read_bc_by_time(t_start, t_end, stations, path_BC=path_BC)
os.makedirs(out_dir, exist_ok=True)

nReplot = 1

if SWITCH_MATPL:
    figpath = os.path.join(out_dir, 'traces_simple.png')
    time_bc1 = timer()
    for i in range(0, nReplot):
        u_obs.simple_streamplot(st0, None, figpath)
    time_bc2 = timer()
    print('# Did {:.0f} naive mpl plots, took {:6.3f} s'.format(
            nReplot, time_bc2 - time_bc1))

if SWITCH_DECIM:
    figpath = os.path.join(out_dir, 'traces_decim.png')
    time_bc1 = timer()
    for i in range(0, nReplot):
        my_streamplot(st0, figpath)
    time_bc2 = timer()
    print('# Did {:.0f} decim plots, took {:6.3f} s'.format(
            nReplot, time_bc2 - time_bc1))

if SWITCH_OBS_PIL:
    figpath = os.path.join(out_dir, 'traces_obspil.png')
    time_bc1 = timer()
    for i in range(0, nReplot):
        save_obspy_PIL(figpath, st0)
    time_bc2 = timer()
    print('# Did {:.0f} obs_PIL plots, took {:6.3f} s'.format(
            nReplot, time_bc2 - time_bc1))

if SWITCH_OBSPY:
    figpath = os.path.join(out_dir, 'traces_obspy.png')
    time_bc1 = timer()
    for i in range(0, nReplot):
        st0.plot(equal_scale=False, outfile=figpath)
    time_bc2 = timer()
    print('# Did {:.0f} obspy plots, took {:6.3f} s'.format(
            nReplot, time_bc2 - time_bc1))
