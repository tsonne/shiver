#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 08:25:12 2020

@author: ts
"""
%matplotlib auto
from datetime import datetime
from shiver import u_obs
from shiver import u_SIL

start = '2020-07-19 23:30:42'
end = '2020-07-19 23:45:42'
stations = ['kri']  #, 'lfe', 'vog']
code_dct = {'network': 'VI'}
path_BC = '/mnt/sil/bc'

# Get the BC data:
st0 = u_SIL.read_bc_by_time(start, end, stations,
                            codes=code_dct, path_BC=path_BC)
st = st0.copy()

# show me
st.plot()

# Remove response
pre_filt = [0.1, 1.0, 35., 45.]
etc_dir = '../dat/sil/etc/'
stcor = u_SIL.remove_resp_sil(st, pre_filt, etc_dir)
stcor.plot()

# filter
stcor.taper(0.01)
stcor.filter('bandpass', freqmin=0.4, freqmax=15.)
stcor.plot()

# Cut stream to 30 sec before P-arrival, 2 min after P
# P at 23:36:15
s_tP = '20200719233615'
t_pre = 5.
t_post = 35.
u_tP = UTCDateTime(s_tP)
st_cut = st.slice(starttime=u_tP-t_pre, endtime=u_tP+t_post)

# plot spectra
u_obs.plot_fft(st_cut)

# Remove response
stc2 = u_SIL.remove_resp_sil(st_cut, pre_filt, etc_dir)
stc2.plot()

# filter
stc2.taper(0.01)
stc2.filter('bandpass', freqmin=5., freqmax=15.)
stc2.plot()
