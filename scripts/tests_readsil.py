#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 12:06:51 2020

@author: ts

Test geo_dataread.sil_read.read_sil_data()
"""
import u_SIL
from geo_dataread.sil_read import read_sil_data


X = read_sil_data(
        start="2020-06-01", end=None,
        frfile=False, base_path="/mnt/sk2",
        fread="events.fps", fname=None, rcolumns=[],
        aut=False, aut_per=30, repl_mlw=False, dna=False, duplicate=False,
        cum_moment=False, logging_level='error')
u_SIL.print_some_info_SILCAT(X, verbose=True)

X = read_sil_data(
        start="2020-06-01", end=None,
        frfile=False, base_path="/mnt/sk2",
        fread="events.fps", fname=None, rcolumns='all',
        aut=False, aut_per=30, repl_mlw=False, dna=False, duplicate=False,
        cum_moment=False)
u_SIL.print_some_info_SILCAT(X, verbose=True)

infile = ('/home/ts/Documents/IMO/PROJECT/Seismology/code/shiver/dat/'
          'sil/catalog/sil_events.prq.zst')
X = read_sil_data(
        start=None, end=None,
        frfile=True, base_path="/mnt/sk2",
        fread="events.fps", fname=infile, rcolumns='all',
        aut=False, aut_per=30, repl_mlw=False, dna=False, duplicate=False,
        cum_moment=False)
u_SIL.print_some_info_SILCAT(X, verbose=True)
