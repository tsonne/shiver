#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 20:01:22 2020

Process multiple stations for multiple events

* get event parameters from SIL catalog (event.fps files or saved dataframe)
* set fixed time window around event origin for waveforms
* get waveforms from BC files
    * detail: assumes path: {path_BC}/%Y/%b/%d/{station}/{BC_files}
    * detail: only tries to find certain list of stations!
* correct waveforms using BC headers and SIL resp files
    * option: can also use station XML response info
    * detail: pre-filter set at [0.07, 0.15, 35., 45.] for all stations
* bandpass filter copy of stream [1., 35.]
* create and save PGN image of stream (raw, corrected, filtered)
* check for clipped traces
* get PGV of entire time window (both filter streams)
* get distance, Mpgv using event, station locations and PGV values
* create and save plots of PGV, Mpgv vs Repi
* create and save plots of tPGV vs Repi
* write pgv, r, Mpgv to text file

Advanced options enabled

* read waveforms from mseed and use textfile with station response info
* save mseed files when read from BC files

"""
import os
import matplotlib
import subprocess
import numpy as np
import pandas as pd
from timeit import default_timer as timer
from obspy import UTCDateTime

from shiver import u_io
from shiver import u_SIL
from shiver import u_obs

matplotlib.use("ps")  # 'ps' might be fastest backend for static plots
time_run_start = timer()

# CONFIGURATION
#
# GET INPUT SETTINGS:
conf = u_io.parse_yaml_conf('../config/SIL_adv_msme_conf_snr.yml')
# INPUT/OUTPUT DIRS
inv_dir = conf['paths']['inventory']
etc_dir = conf['paths']['sil_etc']
out_dir = conf['paths']['outdir']
# STATIONS
stations = conf['stations']['codes']  # and handle string and '_all' cases...
stations = u_SIL.get_station_codes(stations, use_etc=True, path=etc_dir)
full_station_table = u_SIL.get_SIL_station_table_df(path_etc=etc_dir)
full_station_table = full_station_table.loc[stations]  # sta, lat, lon, elev
# BC PREPROC INPUT
path_BC = conf['paths']['BC_in']
code_dct = {'network': conf['stations']['network']}
# FLAGS
UPDATE_CAT = conf['catalog']['SIL_CAT_file_UPDATE']
READ_BC = conf['waveform']['read_BC']
READ_MSEED = conf['waveform']['read_mseed']
WRITE_MSEED = conf['waveform']['write_mseed']
FORCE_WRITE_MSEED = conf['waveform']['force_write_mseed']
RESP_XML = conf['response_xml']
PLOT_TRACES = conf['action']['plot_traces']
PLOT_R_PGV = conf['action']['plot_r_pgv']
PLOT_R_TPGV = conf['action']['plot_r_tpgv']
PLOT_R_SNR = conf['action']['plot_r_snr']
SAVE_PGV_TAB = conf['action']['save_pgv_table']

# IF USING SIL CAT ARCHIVE FILE, UPDATE IT FIRST BEFORE GETTING EVENTS
if conf['catalog']['SIL_CAT_file'] and UPDATE_CAT:
    u_SIL.update_default_silcat_file()

# GET SIL CATALOG AND SELECT SOME EVENTS OF INTEREST,
# USE ZIPPED PICKLED SIL CAT FILE:
X = u_SIL.silcat_by_conf(conf['catalog'])

# PRINT INFO ABOUT LARGEST EVENTS AND PICKED POLARITIES:
u_SIL.print_some_info_SILCAT(X, verbose=True)

# DETERMINE TIME WINDOWS OF INTEREST FOR EACH EVENT:
dt_pre = conf['waveform']['dt_pre']  # sec before origin time for window start
dt_post = conf['waveform']['dt_post']  # sec after  origin time for window end
XX = X.assign(t_start=(X.index - np.timedelta64(dt_pre, 's')),
              t_end=(X.index + np.timedelta64(dt_post, 's')))

# PREPARE FOR LOOP...
COMP = 'V3'  # PGV convention: V3 = vector length of all 3 comp.
SDT_FMT = '%Y%m%d%H%M%S'
SDT_FMT2 = '%Y-%m-%d %H:%M:%S'
TITL_FMT = r'{} BP({}, {}) $\mathregular{{M_{{pgv}}}}$ = {:4.2f}'

# FILTERBANDS (FB):
fbb = [[0.05, 15.],
       [0.25, 15.],
       [1.0, 15.],
       [0.05, 0.5],
       [0.25, 2.5],
       [1.0, 10.],
       [0.5, 5.],
       [2.0, 15.]]
pre_filt = [0.025, 0.05, 35., 45.]
SNR_THRH = 3.  # SNR threshold for acceptance of PGV/Mpgv estimates
mark_stations = None  #['ada']
Vp = 6.0  # first arrival P phase velocity (km/s)
Vs = 3.3  # S-wave velocity (km/s)
tP_pre = 3.  # start signal window this many seconds before P
tS_post = 90.  # stop signal window this many seconds after S
cnt = 0
#
# PREPARE DATAFRAMES FOR ENTIRE DATASET
L10SNR_THRH = np.log10(SNR_THRH)
dfY = pd.DataFrame()
dfEV = XX.copy()
dfEV = dfEV.assign(CNT=None)
#
# METADATA OF FB FOR LOOPS, STORAGE
Nfb = len(fbb)
lFB = []  # list of dicts for filterband metadata, labels
for i in range(Nfb):
    s = str(i)
    lFB.append({
                'FB': s,  # label of FB i
                'fb': fbb[i],  # FB corner values
                'Mm': 'Mm' + s,  # col event mean magnitude
                'Ms': 'Ms' + s,  # col event stdev magnitude
                'Mn': 'Mn' + s,  # col event number of magnitude est.
                'PGV': 'PGV' + s,  # col station PGV
                'tPGV': 'tPGV' + s,  # col station PGV time
                'noise': 'Noise' + s,  # col noise level
                'SNR': 'SNR' + s,  # col SNR = PGV/noise
                'L10SNR': 'L10SNR' + s,  # col log10(SNR)
                'Mpgv': 'Mpgv' + s,  # col M_PGV
                'lbl': 'BP {}-{} HZ'.format(*fbb[i]),  # plot label
                })
    dfEV = dfEV.assign(**{lFB[i]['Mm']: None,
                          lFB[i]['Ms']: None,
                          lFB[i]['Mn']: None})
# Number of R,PGV figures (two if possible, else one)
NfigPGV = 2 if Nfb > 1 else 1
#
# LOOP THROUGH EVENTS, PROCESS ALL AVAILABLE STATIONS
XX = XX[33:35]  # DEBUG
for Ev in XX.itertuples():
    cnt += 1
    print('# {:03d}: {}'.format(cnt, Ev.Index))
    dfEV.at[Ev.Index, 'CNT'] = cnt

    # EVENT DIR PATH
    t_origin = UTCDateTime(Ev.Index)
    ev_ots = t_origin.strftime(SDT_FMT)  # event id str = origin time
    ev_dir = os.path.join(out_dir, ev_ots)  # subdir path for this event

    # GET WAVEFORMS
    time_bc1 = timer()
    FOUND_MSEED = False
    st0 = None
    if READ_MSEED:
        try:
            st0 = u_SIL.read_mseed_bc(start=Ev.t_start, end=Ev.t_end,
                                      pattern=(ev_dir + '/mseed/*msd'))
            FOUND_MSEED = True
        except Exception as msd_exc:
            print('# Could not get mseed files, get:')
            print(msd_exc)
    if READ_BC and not FOUND_MSEED:
        if READ_MSEED:
            print('# Try to get BC waveforms instead:')
        st0 = u_SIL.read_bc_by_time(Ev.t_start, Ev.t_end, stations,
                                    codes=code_dct, path_BC=path_BC)
    time_bc2 = timer()
    if not st0:
        print('# Found no data to process!!! SKIP')
        continue

    # CREATE EVENT DIR
    os.makedirs(ev_dir, exist_ok=True)
    
    st0.sort()

    full_ev_sta_codes = u_obs.station_stream_list(st0)
    print('# Got {:.0f} stations, took {:6.3f} s'.format(
        len(full_ev_sta_codes), time_bc2 - time_bc1))

    # WRITE RAW MSEED, SAVE BC HEADERS
    if WRITE_MSEED and (not FOUND_MSEED or FORCE_WRITE_MSEED):
        mseed_dir = os.path.join(ev_dir, 'mseed')
        os.makedirs(mseed_dir, exist_ok=True)
        u_obs.save_mseed(mseed_dir, st0, lbl="", force=True, encoding='STEIM2')
        u_SIL.save_bc_headers(st0, mseed_dir + '/bc_headers.csv')
        print('# Have written mseed and bc header file to disk')

    # MERGE GAPS BEFORE PLOT, PROCEED WITH ALL:
    st = st0.copy()
    gappy_station_list = u_obs.get_gappy_stations(st0, Ev.t_start, Ev.t_end)
    st.merge()
    st.sort()

    # REMOVE RESPONSE,
    # prefilter, return velocity in m/s
    print('# Remove response...')
    if RESP_XML:
        # remove response using seiscomp station XML
        stcor, inv = u_obs.remove_resp_xml(st, pre_filt, inv_dir)
    else:
        # remove response using SIL etc files
        stcor = u_SIL.remove_resp_sil(st, pre_filt, etc_dir)
    print('# Removed response.')

    # PLOT ALL TRACES (RAW, COR, FLT), SAVE COMBINED FILE
    if PLOT_TRACES:
        # BANDPASS FILTER FOR PLOT
        stflt = stcor.copy()
        stcor.taper(0.01)
        stcor.filter('bandpass', freqmin=fb_pl[0][0], freqmax=fb_pl[0][1])
        stflt.taper(0.01)
        stflt.filter('bandpass', freqmin=fb_pl[1][0], freqmax=fb_pl[1][1])
        # CREATE AND COMBINE IMAGES
        # TODO: do this in Python, not with imagemagick!
        print('# Create trace figures...')
        u_obs.save_obspy_PIL(fpng=(ev_dir + '/raw.png'), st=st)
        u_obs.save_obspy_PIL(fpng=(ev_dir + '/cor.png'), st=stcor)
        u_obs.save_obspy_PIL(fpng=(ev_dir + '/flt.png'), st=stflt)
        cmd1 = """
        cd {}
        convert raw.png cor.png flt.png +append all_rawcorflt.png
        /bin/rm raw.png cor.png flt.png
        """.format(ev_dir)
        subprocess.run(cmd1, shell=True)
        print('# Saved trace figures.')

    # REMOVE GAPPY STATIONS:
    # (they shall be plotted but not processed any further)
    if gappy_station_list:
        for station in gappy_station_list:
            for tr in st.select(station=station):
                st.remove(tr)
            for tr in stcor.select(station=station):
                stcor.remove(tr)
    used_ev_sta_codes = u_obs.station_stream_list(st)
    print('# Use {:.0f} stations (no gaps)'.format(len(used_ev_sta_codes)))

    # BANDPASS FILTER CORRECTED DATA
    lSt = []  # keep each filtered stream in order
    for i in range(Nfb):
        tmpSt = stcor.copy()
        tmpSt.taper(0.01)
        tmpSt.filter('bandpass', freqmin=fbb[i][0], freqmax=fbb[i][1])
        lSt.append(tmpSt.copy())

    # IDENTIFY CLIPPED STATIONS:
    clip_id_list = u_obs.check_clipped(st, THR_ABS=3e6)
    clip_sta = u_obs.sorted_unique(u_obs.station_from_id(clip_id_list))

    # GET EPICENTRAL DISTANCES:
    src_crd = [Ev.latitude, Ev.longitude]
    dfStaLoc = full_station_table.loc[used_ev_sta_codes]
    staLatLonList = dfStaLoc[['lat', 'lon']].values.tolist()
    repi = u_obs.get_distances(src_crd, staLatLonList)

    # GET APPROXIMATE SIGNAL START-END TIMES FOR EACH STATION
    u_otime = UTCDateTime(Ev.Index)  # origin time in obspy.UTCDateTime
    repi = np.asarray(repi)
    tt_start = repi/Vp - tP_pre  # rel start time signal windows
    tt_stop = repi/Vs + tS_post  # rel stop time signal windows
    u_wstart = [u_otime + x for x in tt_start]  # abs start times
    u_wstop = [u_otime + x for x in tt_stop]  # abs stop times

    # CREATE DATAFRAME FOR STATION VALUES:
    #    (ARRANGE AS: EVID, LAT, LON, ELEV, REPI, CLIP)
    dfFULL = dfStaLoc.assign(EvID=cnt,
                             repi=repi,
                             clip=[x in clip_sta for x in dfStaLoc.index])
    dfFULL = dfFULL[['EvID', 'lat', 'lon', 'elev', 'repi', 'clip']]

    # LOOP THROUGH FB:
    #   GET HORIZONTAL PEAK GROUND VELOCITY (PGV) AND ITS TIME
    #   (SEARCH FOR PGV WITHIN GIVEN SIGNAL WINDOW TIMES),
    #   GET NOISE LEVEL AND SNR ESTIMATE,
    #   GET M_PGV
    print('# Get PGV, noise, SNR, Mpgv ...')
    lDfPGV = []
    for i in range(Nfb):
        dfPGV = u_obs.get_pgv_time(
                    lSt[i], comp=COMP, t_start=u_wstart, t_stop=u_wstop)
        dfNoz = u_obs.get_windowed_maxabs(
                    lSt[i], window_length=20., step=10., offset=5.)
        dfFULL[lFB[i]['PGV']] = dfPGV[COMP]
        dfFULL[lFB[i]['tPGV']] = dfPGV['pgvtime']
        dfFULL[lFB[i]['noise']] = dfNoz['V3']
        dfFULL[lFB[i]['SNR']] = dfFULL[lFB[i]['PGV']] / dfNoz['V3']
        # convert object columns to numeric
        cols = [lFB[i]['PGV'], lFB[i]['noise'], lFB[i]['SNR']]
        dfFULL[cols] = dfFULL[cols].apply(pd.to_numeric)
        dfFULL[lFB[i]['L10SNR']] = np.log10(dfFULL[lFB[i]['SNR']])
        dfFULL[lFB[i]['Mpgv']] = [
            u_obs.m_by_pgv_r_C(x, y) for x, y in zip(dfFULL[lFB[i]['PGV']],
                                                     dfFULL['repi'])]
        # mean, stdev, number of magnitudes
        dfEV.at[Ev.Index, lFB[i]['Mm']] = dfFULL.query(
                "{} > {} and not clip".format(
                    lFB[i]['L10SNR'], L10SNR_THRH))[lFB[i]['Mpgv']].mean()
        dfEV.at[Ev.Index, lFB[i]['Ms']] = dfFULL.query(
                "{} > {} and not clip".format(
                    lFB[i]['L10SNR'], L10SNR_THRH))[lFB[i]['Mpgv']].std()
        dfEV.at[Ev.Index, lFB[i]['Mn']] = len(dfFULL.query(
                "{} > {} and not clip".format(
                    lFB[i]['L10SNR'], L10SNR_THRH))[lFB[i]['Mpgv']])

    # STORE THIS EVENT'S DATA BEYOND LOOP
    dfY = pd.concat([dfY, dfFULL])

    # PLOT (R, PGV), INDICATE MPGV
    # using median Mpgv of stations less than 200 km and not clipped,
    # mark apparent outliers and any clipped stations in plot:
    s_origin = Ev.Index.strftime(SDT_FMT2)
    if PLOT_R_PGV:
        for i in range(NfigPGV):
            Mmed = dfFULL.query(
                    "repi < 200 and not clip")[lFB[i]['Mpgv']].median()
            figpath = os.path.join(ev_dir, 'R_PGV_FB{}.png'.format(i))
            title = TITL_FMT.format(s_origin, *lFB[i]['fb'], Mmed)
            u_obs.plot_pgv_r(
                    dfFULL.assign(pgv=dfFULL[lFB[i]['PGV']]),
                    outfile=figpath, M=Mmed, title=title, label_outliers=True,
                    label_clipped=True, label_index=mark_stations)

    # PLOT (R, PGVTRAVELTIME)
    if PLOT_R_TPGV:
        for i in range(NfigPGV):
            figpath = os.path.join(ev_dir, 'R_tPGV_FB{}.png'.format(i))
            u_obs.plot_var_reltime(dfFULL['repi'],
                                   dfFULL[lFB[i]['tPGV']],
                                   originTime=t_origin,
                                   outfile=figpath)

    # PLOT (R, LOG10SNR)
    if PLOT_R_SNR:
        figpath = os.path.join(ev_dir, 'R_SNR.png')
        # can combine two in one, but need to check if we have >1 FB
        if Nfb > 1:
            u_obs.quick_plot_xy(figpath, dfFULL.repi, dfFULL[lFB[0]['L10SNR']],
                                x2=dfFULL.repi, y2=dfFULL[lFB[1]['L10SNR']],
                                xlabel='epicentral distance (km)',
                                ylabel='log10(SNR)',
                                title=(s_origin + ' SNR (PGV/noise)'),
                                labels=[lFB[0]['lbl'], lFB[1]['lbl']])
        else:
            u_obs.quick_plot_xy(figpath, dfFULL.repi, dfFULL[lFB[0]['L10SNR']],
                                xlabel='epicentral distance (km)',
                                ylabel='log10(SNR)',
                                title=(s_origin + ' SNR (PGV/noise)'),
                                labels=[lFB[0]['lbl']])

    # PLOT (R, MPGV) BY FB
    # Fancy scatterplot, combine all data to one plot and label FBs and SNR.
    figpath = os.path.join(ev_dir, 'R_M_FB.png')
    # combine columns of all FBs of same type (all Mpgv, all SNR, repi)
    x = np.concatenate([dfFULL.repi.values for i in range(Nfb)])
    y = np.concatenate([dfFULL[lFB[i]['Mpgv']].values for i in range(Nfb)])
    s = np.concatenate(
        [((3*dfFULL[lFB[i]['L10SNR']])**2).values for i in range(Nfb)])
    # each FB gets one color value
    c = np.concatenate([i*np.ones(dfFULL.shape[0]) for i in range(Nfb)])
    scdict = dict(x=x, y=y, s=s, c=c, cmap='jet')
    # legend settings (note resizing in kw2 func for marker size values)
    labels = [lFB[i]['lbl'] for i in range(Nfb)]
    kw1 = dict(prop='colors', labels=labels)
    kw2 = dict(prop='sizes', color='black', fmt='{x:.2f}', num=8,
               func=lambda s: np.sqrt(s)/3, title='log10SNR')
    u_obs.fancy_scatter_plot(
            figpath, scdict,
            xlabel='Distance (km)', ylabel='Mpgv',
            title='Distance-Magnitude for all frequency bands',
            legend=True, legend_out=True,
            fancy_legend=True, legend_kw1=kw1, legend_kw2=kw2)
    

    # write pgv, r, Mpgv to text file:
    if SAVE_PGV_TAB:
        w2_path = os.path.join(ev_dir, ev_ots + '_parameter.dat')
        with open(w2_path, 'w') as f:
            f.write(dfFULL.to_string(float_format='{:.4e}'.format))

    print('#')

# HAVE DATA OF ALL EVENTS
#
# get 95 percentile value of noise level estimates of all FB:
ND = len(dfY[lFB[0]['noise']])
k95 = int(ND/100*95)
noise95 = []
print('#')
for i in range(Nfb):
    noise95.append(dfY[lFB[i]['noise']].sort_values().iloc[k95])
    print('# {}: 95-percentile noise level: {:.3e}'.format(
            lFB[i]['lbl'], noise95[i]))
print('#')

# get distance-magnitude plot of where PGV = noise level based on PV09
n = 50
y = noise95[0]*np.ones(50)
m = np.linspace(1., 5.0, n)
r1 = u_obs.PV09C_r_by_pgv_m(y, m)
r2 = u_obs.PV09C_r_by_pgv_m(y*3., m)
fn='../out/pgv_noise.png'
u_obs.quick_plot_xy(
    fn, np.log10(r1), m,
    x2=np.log10(r2), y2=m,
    marker='-',
    xlabel='log10(epicentral distance [km])',
    ylabel='Mpgv',
    title=('Distance-Magnitude at which PGV/noise = k,\n'
           'PGV by PV09C, noise level = {:.3e} m/s'.format(noise95[0])),
    labels=['k = 1','k = 3'])

# Noise histograms for first two FBs
fn='../out/noise_hist.png'
x2 = None
l2 = None
if Nfb > 1:
    x2 = np.log10(dfY[lFB[1]['noise']])
    l2 = lFB[1]['lbl']
u_obs.quick_hist1(
    fn, np.log10(dfY[lFB[0]['noise']]), x2=x2,
    xlabel='log10(Noise[m/s])',
    title='Noise estimates, N = {}'.format(ND),
    labels=[lFB[0]['lbl'], l2])

# Scatterplot of R-M for each FB, only if > SNR_THRH
for i in range(Nfb):
    fn = '../out/R_M_FB{}.png'.format(i)
    b = dfY[lFB[i]['L10SNR']] >= L10SNR_THRH
    u_obs.quick_scatter(
        fn, dfY['repi'][b], dfY[lFB[i]['Mpgv']][b], c=dfY[lFB[i]['L10SNR']][b],
        xlabel='Distance (km)', ylabel='Mpgv',
        title='Distance-Magnitude for ' + lFB[i]['lbl'],
        vmin=L10SNR_THRH, vmax=2., colorbar=True,
        cmap='cool', cbarlabel='log10SNR')

# Clipped stations overview plot
b = dfY['clip']
if np.sum(b) > 0:
    dfCL = dfY[b]
    clipped_stations = dfCL.index.unique().to_list()
    fn = '../out/R_M_Clipped.png'
    dct = {}
    for station in clipped_stations:
        ecnt = dfCL.loc[station, 'EvID']
        repi = dfCL.loc[station, 'repi']
        if isinstance(ecnt, np.int64):
            mlw = dfEV.loc[dfEV['CNT']==ecnt]['MLW'].values
        else:
            repi = repi.to_list()
            mlw = dfEV.loc[dfEV['CNT'].isin(ecnt)]['MLW'].to_list()
        dct[station] = [repi, mlw]
    u_obs.quick_plot_dict(fn, dct,
                          xlabel='Distance (km)', ylabel='MLW',
                          title='Distance-Magnitude for clipped stations')
else:
    print('#')
    print('# No clipping detected at all!')
    print('#')

# Print elapsed runtime:
print('# Done.')
time_run_end = timer()
print('# Elapsed time: {} s'.format(time_run_end - time_run_start))
