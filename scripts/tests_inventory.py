#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 10:23:24 2020

@author: ts

Read SeisComP StationXML file from Gempa,
select one station ...
"""

from obspy import read_inventory

# Downloaded most recent SC3ML for sil, version 0.9
fpath_XML = ('../dat/sil/inventory/20201120_sil_vi_edit1_NL.xml')
inv = read_inventory(fpath_XML, format='SC3ML')

# get metadata for only one station (all channels and times):
net = 'VI'
sta = 'ada'
sel_inv = inv.select(network=net, station=sta)

# print some top level info about that station:
sel_inv.get_channel_metadata('VI.ada..HHZ')
#sel_inv.get_coordinates('')
#DI = sel_inv.get_contents()  # just get simple dict with strings
startdate = sel_inv.networks[0].stations[0].start_date
enddate = sel_inv.networks[0].stations[0].end_date

# get response
resp = inv.get_response('.'.join([net,sta,'','HHZ']),
                        startdate)

#
# Get response from NRL (same as ADA...?)
from obspy.clients.nrl import NRL

nrl = NRL()
response = nrl.get_response( 
    sensor_keys=['Guralp', 'CMG-3ESP', '60 s - 50 Hz', '2000'],
    datalogger_keys=[
        'Guralp', 'CMG-DM24', 'Mk3', 'Fixed', '1-10', '6', '100'])

print(nrl.dataloggers
      ['Guralp']['CMG-DM24']['Mk3']['Fixed']['1-10']['6']['100'])
print(nrl.sensors
      ['Guralp']['CMG-3ESP']['60 s - 50 Hz']['2000'])
