#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 25 15:23:15 2021

@author: ts

NOTES:
    * 1 hour data (5 channels) takes about 1 min
"""
%matplotlib auto

import glob
from obspy.core.util import AttribDict
from obspy import read, read_inventory #, UTCDateTime
from obspy.signal.array_analysis import (array_processing,
                                         get_spoint,
                                         get_geometry)
import shiver.u_array as u_array
import numpy as np
# for plots:
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from obspy.imaging.cm import viridis_white_r

# Settings:
taper_perc = 0.03
fbb1 = 2.
fbb2 = 8.
ffk1 = 2.
ffk2 = 8.
stride = 0.1
slim = 0.25
sstep = 0.025
FIXED_WLEN = False
set_wlen = 0.4
min_wlen = 0.35

# input data path
in_dir = '/home/ts/git/shiver/dat/arrays/NO'

# find and read inventory
xml_list = glob.glob(in_dir + '/*.xml')
xml_file = xml_list[0]
inv = read_inventory(xml_file)

# load wav. data
st = read(in_dir + '/*.msd')

# attach station coordiantes
for tr in st:
    meta = inv.get_channel_metadata(tr.id)
    tr.stats.coordinates = AttribDict({
            'latitude' : meta['latitude'],
            'longitude' : meta['longitude'],
            'elevation' : meta['elevation']})

# accept only first hour of data
ut_start = st[0].stats.starttime
st = st.trim(endtime=ut_start+3600)
st_f = st.copy()

# set time to be analyzed
stime = ut_start
etime = ut_start+3600

# detrend/demean
st_f.detrend(type='demean')
# taper
st_f.taper(max_percentage=taper_perc)
# filter
st_f.filter('bandpass',
            freqmin=fbb1, freqmax=fbb2, corners=3, zerophase=True)
# normalize
st_f.normalize()

# gather only vertical traces
st_zf = st_f.select(component='Z')
ns = len(st_zf)

# moving fk
smpdel = st_zf[0].stats.delta
if FIXED_WLEN:
    wlen = set_wlen
else:
    wlen = (0.1333 * ffk1 + 1.8667) / ffk1
    if wlen < min_wlen:
        wlen = min_wlen
wlen = float(round(wlen/smpdel))*smpdel
wfrac = stride/wlen + 0.000001
kwargs = dict(
    # slowness grid: X min, X max, Y min, Y max, Slow Step
    sll_x=-slim, slm_x=slim, sll_y=-slim, slm_y=slim, sl_s=sstep,
    # sliding window properties
    win_len=wlen, win_frac=wfrac,
    # frequency properties
    frqlow=ffk1, frqhigh=ffk2, prewhiten=0,
    # restrict output
    semb_thres=-1e9, vel_thres=-1e9, timestamp='mlabday',
    # start and end times
    stime=stime, etime=etime
)
fkdat = array_processing(st_zf, **kwargs)

# SNR
# get max SNR of corresponding beam within fk window
snrzeta = 5. # LTA update recursive diminution parameter (5 or 6 is fine)
snrlenfact = 2. # STA length is this many periods of low corner freq.
snrdelayfact = 2.5 # LTA delayed to STA by this many STA lengths
snrlen = 1. / ffk1 * snrlenfact # STA length (s)
snrdly = snrlen * snrdelayfact # LTA delay to STA (s)

fs = st_zf[0].stats.sampling_rate
nsamp_fk = int(wlen * fs)  # fk win length (samples)
nstep = int(nsamp * wfrac)  # fk win stride (samples)
nsamp_sta = int(snrlen * fs) # STA length (samples)
nsamp_snrdel = int(snrdly * fs) # LTA delay to STA (samples)
(nw, dum) = fkdat.shape
# start time index
wdel_snr = 20.  # SNR window length before start of fk window (s)
nwdel_snr = int(wdel_snr * fs)
nsamp_snr = int(wdel_snr * fs + nsamp_fk)
spoint = int((stime - st_zf[0].stats.starttime - wdel_snr) * fs + .5)
snrdat = np.nan(nw, dtype=np.float64)
dat = np.empty((ns, nsamp_snr), dtype=np.float64)
# get geometry once instead of every beam
geometry = get_geometry(st_zf, coordsys='lonlat', return_center=True)
npts = st_zf[0].stats.npts
for a in range(nw):
    offset = a*nstep
    i1snr = spoint + offset  # first SNR window index
    if i1snr < 0 or i1snr + nsamp_snr > npts:
        snrdat[a] = np.nan
        continue
    # create beam (stack)
    baz, slw = fkdat[a, 3:5]
    ndelays = u_array.get_delays_from_geo(geometry, smpdel, baz, slw)['ndelay']
    for b, tr in enumerate(st_zf):
        dat[b, :] = np.roll(tr.data[i1snr : i1snr + nsamp_snr], -ndelays[b])
    beam = dat.sum(axis=0) / ns
    # should taper beam before this!
    # but then need more signal after fk window to ensure it's not tapered
    # too much!
    c_snr = u_array.nor_sta_lta(
        beam, nsamp_sta, nsamp_snrdel, snrzeta)
    # get max amp from within fk window
    snrdat[a] = np.max(np.abs(c_snr[nwdel_snr : nwdel_snr + nsamp_fk]))



# PLOT output interactively
#
ffk = [ffk1, ffk2]
title_id = st_zf[0].id
FSizeI = [8.,4.] # conf['graphics']['fsize_fk']
myDPI = 200 # conf['graphics']['dpi']
#slim = conf['fk']['slim']
#
labels = ['rel.power', 'abs.power', 'baz', 'slow', 'snr']
nplt = len(labels)
# make output human readable, adjust backazimuth to values between 0
# and 360
# t, rel_power, abs_power, baz, slow = fkdat.T
# baz[baz < 0.0] += 360.
fkdat[fkdat[:, 3] < 0.0, 3] += 360.
#
xlocator = mdates.AutoDateLocator()
fig = plt.figure(figsize=FSizeI, dpi=myDPI)
for i, lab in enumerate(labels):
    ax = fig.add_subplot(nplt, 1, i + 1)
    if i == 4:  # snr
        ax.scatter(fkdat[:, 0], snrdat, c=fkdat[:, 1], s=1,
                   alpha=0.6, edgecolors='none', cmap=viridis_white_r)
    else:
        ax.scatter(fkdat[:, 0], fkdat[:, i + 1], c=fkdat[:, 1], s=1,
                   alpha=0.6, edgecolors='none', cmap=viridis_white_r)
    ax.set_xlim(fkdat[0, 0], fkdat[-1, 0])
    ax.set_ylabel(lab)
    if i == 2:  # baz
        ax.set_ylim(0., 360.)
        ax.set_yticks([0, 90, 180, 270, 360])
    elif i == 3:  # slow
        ax.set_ylim(0., slim*1.42)
    elif i == 4:  # snr
        pass
        #ax.set_ylim(0., snrdat.max()*1.05)  # has nan!
    else:
        ax.set_ylim(fkdat[:, i + 1].min(), fkdat[:, i + 1].max())
    ax.xaxis.set_major_locator(xlocator)
    ax.xaxis.set_major_formatter(mdates.AutoDateFormatter(xlocator))
    if i<nplt-1:  # hide xticklabels except for bottom subplot
        ax.xaxis.set_ticklabels([])
    ax.grid(b=True, linestyle='--', color=(.8, .8, .8))
    if i == nplt-1:
        ax.set_xlabel('Time (d HH:MM)')
fig.suptitle('{} FK ({:5.2f}, {:5.2f}) {}'.format(
        title_id, ffk[0], ffk[1], stime.strftime('%Y-%m-%d %H:%M:%S.%f')))

plt.close()



