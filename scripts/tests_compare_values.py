#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 14:29:41 2020

@author: ts

Compare PGV and PGV time values:
    AlertMap versus shiver.msme tables
"""
import os
import re
import pytz
import pandas as pd
import numpy as np
from obspy import UTCDateTime
from glob import glob
from shiver import u_obs
from shiver import u_SIL
import matplotlib.pyplot as plt


def get_combined_csv(file_pardat, file_alert):
    """
    Get merged DataFrame of two csv files.

    Indices expected to be station names, expect a parameter file by msmse.py
    and an alertMap based csv file as input. Convert alertMap PGV to m/s.
    Calculate time differences between alert_PGV_time and parameter.dat times,
    PGVflt (PGV of filtered traces) and PGVnof (PGV of unfiltered traces).

    Parameters
    ----------
    file_pardat : str
        Path to shiver.msme-produced *parameter.dat file.
    file_alert : str
        Path to alertMap-based PGV_*.csv file.

    Returns
    -------
    pandas.DataFrame
        DESCRIPTION.
    """
    import pandas as pd
    # cannot compare datetimes with and without timezones, must read both
    # files such that timezones are explicitly added
    dfP = pd.read_csv(file_pardat, sep=r'\s+', index_col=0, parse_dates=[5, 8],
                      date_parser=lambda col: pd.to_datetime(col, utc=True))
    dfA = pd.read_csv(file_alert, parse_dates=[1], index_col=0,
                      date_parser=lambda col: pd.to_datetime(col, utc=True))
    # join by indices of both (station names)
    dfX = dfP.join(dfA)
    # fix scientific notation PGV strings to float
    dfX['PGVflt'] = dfX['PGVflt'].astype(float)
    dfX['PGVnof'] = dfX['PGVnof'].astype(float)
    # convert alertMap PGV amplitude from microm/s to m/s to be same as others
    dfX['pgv'] = dfX['pgv']/1e6
    # compare times where both exist
    return dfX.assign(
        dt_flt=(
            dfX['time'] - dfX['tPGVflt']).apply(lambda x: x.total_seconds()),
        dt_nof=(
            dfX['time'] - dfX['tPGVnof']).apply(lambda x: x.total_seconds()))


def save_BoxFig(df, ptyp='ResPGV', dtyp='nof',
                xlabel='station', ylabel='PGV residual'):
    """
    Create and save boxplot of residuals given dataframe.

    Parameters
    ----------
    df : pandas.DataFrame
        Data matrix, station names are column labels, indices are events.
    ptyp : str
        Plot type, time or PGV residuals. Title will change if time residuals
        indicated as 'ResT', 'ResTT' or 'dt' (ign.case).
    dtyp : str
        Data type ('flt' or 'nof', based on filtered or not filtered traces).
    xlabel : str, optional
        x-axis label. The default is 'station'.
    ylabel : str, optional
        y-axis label. The default is 'PGV residual'.

    Returns
    -------
    None.
    """
    import numpy as np
    import os

    data_vec_list = df.values
    mask = ~np.isnan(data_vec_list)
    data_vec_list = [d[m] for d, m in zip(data_vec_list.T, mask.T)]

    ds_Ne = df.notna().sum()
    NminS = ds_Ne.min()
    NmaxS = ds_Ne.max()
    TITL_DPGV_STAT_FMT = (r'$\mathregular{{log_{{10}}}}(y_{{{}}}/y_{{{}}})$ '
                          r'per station (N from {} to {})')
    if ptyp.lower() in ['rest', 'dt', 'restt']:
        TITL_DPGV_STAT_FMT = (r'$\Delta t = t_{{{}PGV}} - t_{{{}PGV}}$ '
                              r'per station (N from {} to {})')
    title = TITL_DPGV_STAT_FMT.format(dtyp, 'alert', NminS, NmaxS)
    outfile = os.path.join(dir_out,
                           'AllStations_Boxplot_{}_{}PGValert.png'.format(
                               ptyp, dtyp))

    fig, ax = plt.subplots(figsize=(20, 12), dpi=100)
    bplot = ax.boxplot(data_vec_list,
                       vert=True, patch_artist=True,
                       medianprops={'color': 'red'},
                       labels=df.columns.values.tolist())
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.grid(b=True, which='major')
    for patch in bplot['boxes']:
        patch.set_facecolor('lightblue')
    YLim = ax.get_ylim()
    yr = YLim[1] - YLim[0]
    yp = YLim[0] + yr*0.01
    txd = {'horizontalalignment': 'center'}  # put number of events/station
    [ax.text(i+1., yp, str(ds_Ne[i]), **txd) for i in range(df.shape[1])]
    ax.tick_params(axis='x', labelrotation=45)
    fig.savefig(outfile, bbox_inches='tight')
    plt.close()


DO_EVENT_PLOTS = False
DO_SUMMARY_PLOTS = True
dir_out = '../out/alert_cmp/'
dir_alert = '../dat/alert/PGV data from event_table/'
dir_pardat = '../out/msme_group/'

all_pardat_files = glob(dir_pardat + '*/*_parameter.dat')
all_pardat_files.sort()
all_alert_files = glob(dir_alert + 'PGV_*csv')
all_alert_files.sort()

df_pardat = pd.DataFrame(all_pardat_files, columns=['pardat_file'])
torigin_pardat = []
ev_id = []
for filepath in all_pardat_files:
    f_name = os.path.splitext(os.path.basename(filepath))[0]
    s_time = re.findall(r'^[0-9]+', f_name)[0]
    torigin_pardat.append(UTCDateTime(s_time))
    ev_id.append(s_time)
df_pardat = df_pardat.assign(otime_p=torigin_pardat, ev_id=ev_id)
df_pardat['alert_file'] = None
df_pardat['otime_a'] = None

df_alert = pd.DataFrame(all_alert_files, columns=['file'])
torigin_alert = []
for filepath in all_alert_files:
    f_name = os.path.splitext(os.path.basename(filepath))[0]
    s_time = f_name[4:]
    torigin_alert.append(UTCDateTime(s_time))
df_alert = df_alert.assign(otime=torigin_alert)

TIME_THR = 90.  # tolerance in seconds between matching two times
for i in range(len(df_pardat)):
    b = np.abs(df_alert['otime'] - df_pardat['otime_p'].iloc[i]) < TIME_THR
    if any(b):
        df_pardat.iloc[i, 3] = df_alert.loc[b]['file'].values[0]
        df_pardat.iloc[i, 4] = df_alert.loc[b]['otime'].values[0]

Ne = df_pardat['otime_a'].notna().sum()
print('# Have {} matches for {} parameter files.'.format(Ne, len(df_pardat)))

REPI_THR = 200.  # epicentral distance threshold for magnitude
TITL_FMT = r'{}, {}, $\mathregular{{M_{{pgv}}}}$ = {:4.2f}'
TITL_DPGV_FMT = (r'{}, $\mathregular{{log_{{10}}}}(y_{{{}}}/y_{{{}}})$, '
                 r'$\sigma = {:4.2f}$')
TITL_DPGV_STAT_FMT = (r'$\mathregular{{log_{{10}}}}(y_{{{}}}/y_{{{}}})$ '
                      r'per station (N from {} to {})')
TITL_DT_STAT_FMT = (r'$\Delta t = t_{{{}PGV}} - t_{{{}PGV}}$ '
                    r'per station (N from {} to {})')
l_Malert = []
l_Mflt = []
l_Mnof = []
l_sigma_flt = []
l_sigma_nof = []
# preallocate two dataframes for all stations (index) and events (cols),
# to collect all PGV residuals and Repi for statistics later:
df_all_stations = u_SIL.get_SIL_station_table_df(path_etc='../dat/sil/etc')
Ns = len(df_all_stations)
l_matched_ev_ids = df_pardat[
                        df_pardat['otime_a'].notna()].ev_id.values.tolist()
df_ResPGVflt = pd.DataFrame(np.full([Ns, Ne], np.nan),
                            index=df_all_stations.index,
                            columns=l_matched_ev_ids)
df_ResPGVnof = pd.DataFrame(np.full([Ns, Ne], np.nan),
                            index=df_all_stations.index,
                            columns=l_matched_ev_ids)
df_ResTflt = pd.DataFrame(np.full([Ns, Ne], np.nan),
                          index=df_all_stations.index,
                          columns=l_matched_ev_ids)
df_ResTnof = pd.DataFrame(np.full([Ns, Ne], np.nan),
                          index=df_all_stations.index,
                          columns=l_matched_ev_ids)
df_Repi = pd.DataFrame(np.full([Ns, Ne], np.nan),
                       index=df_all_stations.index,
                       columns=l_matched_ev_ids)

for index, row in df_pardat.iterrows():
    if row['alert_file']:
        evID = row['ev_id']
        otime = pytz.utc.localize(row['otime_p'].datetime)
        dfX = get_combined_csv(row['pardat_file'], row['alert_file'])
        # times of PGV
        df_ResTflt[evID] = dfX['dt_flt']
        df_ResTnof[evID] = dfX['dt_nof']
        if DO_EVENT_PLOTS:
            u_obs.plot_var_reltime(
                dfX['repi'], dfX['dt_flt'],
                outfile=os.path.join(dir_out, evID + '_r_dt_fltPGValert.png'),
                ylabel='travel time difference (s)',
                title=r'$\Delta t$(fltPGV, alertPGV)')
            u_obs.plot_var_reltime(
                dfX['repi'], dfX['dt_nof'],
                outfile=os.path.join(dir_out, evID + '_r_dt_nofPGValert.png'),
                ylabel='travel time difference (s)',
                title=r'$\Delta t$(nofPGV, alertPGV)')
            u_obs.plot_var_reltime(
                dfX['repi'], dfX['tPGVnof'], originTime=otime,
                outfile=os.path.join(dir_out, evID + '_r_t_nofPGV.png'),
                ylabel='travel time (s)',
                title='tt PGV of unfiltered signals')
            u_obs.plot_var_reltime(
                dfX['repi'], dfX['tPGVflt'], originTime=otime,
                outfile=os.path.join(dir_out, evID + '_r_t_fltPGV.png'),
                ylabel='travel time (s)',
                title='tt PGV of filtered signals')
            u_obs.plot_var_reltime(
                dfX['repi'], dfX['time'], originTime=otime,
                outfile=os.path.join(dir_out, evID + '_r_t_alertPGV.png'),
                ylabel='travel time (s)',
                title='tt PGV of alertMap')

        # PGV
        # just each PGV estimate (alert, flt, nof)
        df_Bool = (dfX['pgv'].notna()) & (dfX['repi'] < REPI_THR)
        Mpgv = np.median([u_obs.m_by_pgv_r_C(x, y) for x, y in zip(
                    dfX[df_Bool]['pgv'],
                    dfX[df_Bool]['repi'])])
        if DO_EVENT_PLOTS:
            outfile = os.path.join(dir_out, evID + '_r_pgv_alertPGV.png')
            title = TITL_FMT.format(otime, 'AlertPGV', Mpgv)
            u_obs.plot_pgv_r(dfX, outfile=outfile, M=Mpgv, title=title)

        df_Bool = (dfX['PGVflt'].notna()) & (dfX['repi'] < REPI_THR)
        Mpgv = np.median([u_obs.m_by_pgv_r_C(x, y) for x, y in zip(
                    dfX[df_Bool]['PGVflt'],
                    dfX[df_Bool]['repi'])])
        if DO_EVENT_PLOTS:
            outfile = os.path.join(dir_out, evID + '_r_pgv_fltPGV.png')
            title = TITL_FMT.format(otime, 'fltPGV', Mpgv)
            u_obs.plot_pgv_r(dfX.assign(pgv=dfX['PGVflt']),
                             outfile=outfile, M=Mpgv, title=title)

        df_Bool = (dfX['PGVnof'].notna()) & (dfX['repi'] < REPI_THR)
        Mpgv = np.median([u_obs.m_by_pgv_r_C(x, y) for x, y in zip(
                    dfX[df_Bool]['PGVnof'],
                    dfX[df_Bool]['repi'])])
        if DO_EVENT_PLOTS:
            outfile = os.path.join(dir_out, evID + '_r_pgv_nofPGV.png')
            title = TITL_FMT.format(otime, 'nofPGV', Mpgv)
            u_obs.plot_pgv_r(dfX.assign(pgv=dfX['PGVnof']),
                             outfile=outfile, M=Mpgv, title=title)

        # PGV residuals between alert and flt, nof
        dtyp = 'nof'
        key = 'PGV' + dtyp
        df_Bool = (dfX[key].notna()) & (dfX['pgv'].notna())
        rr = dfX[df_Bool]['repi'].values.tolist()
        yy = (dfX[df_Bool][key].apply(np.log10)
              - dfX[df_Bool]['pgv'].apply(np.log10))
        sigma = yy.std()
        df_Repi[evID] = dfX[df_Bool]['repi']
        df_ResPGVnof[evID] = yy
        if DO_EVENT_PLOTS:
            outfile = os.path.join(
                        dir_out, evID + '_r_dpgv_' + dtyp + 'PGValert.png')
            title = TITL_DPGV_FMT.format(otime, dtyp, 'alert', sigma)
            u_obs.plot_var_reltime(rr, yy, outfile=outfile,
                                   ylabel='PGV residual', title=title)

        dtyp = 'flt'
        key = 'PGV' + dtyp
        df_Bool = (dfX[key].notna()) & (dfX['pgv'].notna())
        rr = dfX[df_Bool]['repi'].values.tolist()
        yy = (dfX[df_Bool][key].apply(np.log10)
              - dfX[df_Bool]['pgv'].apply(np.log10))
        sigma = yy.std()
        df_ResPGVflt[evID] = yy
        if DO_EVENT_PLOTS:
            outfile = os.path.join(
                        dir_out, evID + '_r_dpgv_' + dtyp + 'PGValert.png')
            title = TITL_DPGV_FMT.format(otime, dtyp, 'alert', sigma)
            u_obs.plot_var_reltime(rr, yy, outfile=outfile,
                                   ylabel='PGV residual', title=title)

AvgBias = df_ResPGVnof.mean().mean()
AvgStd = df_ResPGVnof.std().mean()
Nd = df_ResPGVnof.notna().sum().sum()  # number of data points
df_Bool = df_Repi.notna().sum(axis=1) > 0  # mask for matched station
Nsmatched = df_Bool.sum()
AvgTbias = df_ResTnof.mean().mean()
AvgTstd = df_ResTnof.std().mean()
print('# Based on {} events with {} records total: '.format(Ne, Nd))
print('# {} different stations matched.'.format(Nsmatched))
print(('# Average log10 bias per event: '
       '{:5.2f} (factor {:4.1f})').format(AvgBias, 10**(AvgBias)))
print(('# Average log10 standard deviation per event: '
       '{:5.2f} (factor {:4.1f})').format(AvgStd, 10**(AvgStd)))
print(('# Average time bias per event: '
       '{:5.2f} sec').format(AvgTbias))
print(('# Average time standard deviation per event: '
       '{:5.2f} sec').format(AvgTstd))
if DO_SUMMARY_PLOTS:
    save_BoxFig(df_ResPGVnof[df_Bool].T, ptyp='ResPGV', dtyp='nof',
                xlabel='station', ylabel='PGV residual')
    save_BoxFig(df_ResTnof[df_Bool].T, ptyp='ResTT', dtyp='nof',
                xlabel='station', ylabel='travel time difference (s)')
    save_BoxFig(df_ResPGVflt[df_Bool].T, ptyp='ResPGV', dtyp='flt',
                xlabel='station', ylabel='PGV residual')
    save_BoxFig(df_ResTflt[df_Bool].T, ptyp='ResTT', dtyp='flt',
                xlabel='station', ylabel='travel time difference (s)')
print('# DONE')
