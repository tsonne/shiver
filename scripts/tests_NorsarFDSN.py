#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 23 18:17:18 2020

@author: ts

Get 4 hours of MSEED waveform files from 5 stations of Norsar Spitsbergen
array, all 3 components each station.
Station SPB2 is not streamed by Norsar, the others only have HHZ.

searched over events on https://www.jordskjelv.no/,
chose 2019-12-01 07:00:00 - 11:00:00,
two events 100 km towards SE at 07:22:15 M2.1, 07:25:45 M2.3, both h=15km
"""
import os
from shiver import u_obs
from obspy.clients.fdsn import Client
from obspy import UTCDateTime

# output path
out_dir = '/home/ts/git/shiver/dat/arrays/NO'

# Initialize client, get MSEED data, plot, write to files
client = Client('IRIS')
t_orig = UTCDateTime('2019-12-01T07:00:00')
(t_pre, t_post) = (0, 3600*4)
t_start = t_orig + t_pre
t_end = t_orig + t_post
network = 'NO'
station = ['SPA0', 'SPB1', 'SPB3', 'SPB4', 'SPB5']
s_station = ','.join(station)
st = client.get_waveforms(network, s_station, '*', 'HH*',
                          t_start, t_end, attach_response=True)
st.plot()
u_obs.save_mseed(out_dir, st, lbl="", force=True, encoding='STEIM2')

# get station metadata and write to XMl file
inventory = client.get_stations(network=network, station=s_station,
                                starttime=t_start, endtime=t_end,
                                level='response')
inventory.write(os.path.join(out_dir, 'inventory_SPITS.xml'),
                format='STATIONXML')

# try to get some event info for that time and region
client_isc = Client('ISC')
try:
    cat = client_isc.get_events(starttime=t_start, endtime=t_end,
                                minlatitude=64.,
                                maxlatitude=84.,
                                minlongitude=-25.,
                                maxlongitude=58.,
                                minmagnitude=0)#, catalog="ISC")
except Exception as e:
    print('ERROR:')
    print(e)
