#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Command Line Input script to get and convert BC to MSEED files.

The script can find BC waveform files in a SIL directory structure by station
names and start/end times. It converts them to MSEED and saves those in a
given output directory.

Example:
    bc2mseed.py 20190122 20190202_120000 asm bja kri path/to/outdir -v
    ---
    This will get the data of the three stations (asm,bja,kri) from between
    2019-01-22T00:00:00 to 2019-02-02T12:00:00 and save MSEED to the given
    directory "path/to/outdir". (option: verbose)

Created on Thu Dec 10 16:56:51 2020
@author: ts
"""
import os
import argparse
import logging as log
from obspy import UTCDateTime
from shiver import u_SIL, u_obs

#if __name__ == '__main__':
parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-v', action='store_true',
                    help='verbose')
parser.add_argument('s', action='store',
                    help='start date_time, formats: yyyymmdd[HHMMSS], ISO8601')
parser.add_argument('e', action='store',
                    help='end date_time')
parser.add_argument('c', nargs='+',
                    help='station code(s), space-separated if multiple')
parser.add_argument('o', action='store',
                    help='output directory')
parser.add_argument('-p', action='store', default='/sil/bc',
                    help='path to SIL BC top directory (default: %(default)s)')
args = parser.parse_args()

if args.v:
    log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
else:
    log.basicConfig(format="%(levelname)s: %(message)s")


start = UTCDateTime(args.s)
end = UTCDateTime(args.e)
out_dir = args.o
stations = args.c
path_BC = args.p

log.info('Start:    {}'.format(start))
log.info('End:      {}'.format(end))
log.info('Stations: {}'.format(' '.join(stations)))
log.info('Output:   {}'.format(out_dir))

dt_start = start.datetime
dt_end = end.datetime

if not os.path.isdir(out_dir):
    log.error('Output directory not found: {}'.format(out_dir))
    exit()
log.info('---------')

#exit()

# make output directory for mseed files
#out_dir = '../dat/sil/mseed/' + dt_start.strftime('%Y%m%d%H%M%S')
#os.makedirs(out_dir, exist_ok=True)

# get the BC data:
code_dct = {'network': 'VI', 'location': ''}
st = u_SIL.read_bc_by_time(dt_start, dt_end, stations,
                           codes=code_dct, path_BC=path_BC)

# save stream as mseed, also save BC headers as text file
u_obs.save_mseed(out_dir, st, lbl="", force=True, encoding='STEIM2')
u_SIL.save_bc_headers(st, os.path.join(out_dir, 'bc_headers.csv'))
