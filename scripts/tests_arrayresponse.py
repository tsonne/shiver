#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  8 11:53:20 2020

@author: ts
"""


def parse_cli_args():
    """Define and parse command line arguments to this script."""
    import argparse
    parser = argparse.ArgumentParser(description='Array proc script')
    parser.add_argument('config', type=str, nargs='?',
                        default='../config/array_resps_conf.yml',
                        help='config file path')
    return parser.parse_args()

if True:
#def main():
    """Run entire array processing line."""
    import os
    import copy
    import numpy as np
    from shiver import u_ICE1
    from shiver import u_obs
    from shiver import u_array
    from shiver.u_io import parse_yaml_conf

    # get input settings
    args = parse_cli_args()
    conf = parse_yaml_conf(args.config)
    conf['cli'] = args

    # read station table file
    stats = u_array.read_station_table(conf['paths']['stationtable'])
    coords = stats[['lat', 'lon', 'elev']].values
    klim = 40.
    kstep = 0.4
    fig = u_array.array_transfer_k_plot(
        coords, klim=klim, kstep=kstep, plottype='contourf')


#if __name__ == "__main__":
#    main()
